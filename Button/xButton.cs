﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;

#region Enums
public enum ObjectAlignment
{
    Left,
    Right,
    Center
}
public enum SplitLine
{
    None,
    Max,
    Space
}
public enum ObjectDisplayOrder
{
    ImageFirst,
    TextFirst,
    ImageFront,
    TextFront,
}
public enum ImageSize
{
    FontHeight,
    Original,
    Custom,
}
public enum BorderStyle
{
    None,
    Solid,
    Dot,
    Dash,
    DashDot,
    DashDotDot,
}
public enum ControlState
{
    None,           // State none
    Base,           // Default state
    Hover,          // Control is hovered / MouseEnter
    Active,         // Control is clicked / MouseDown
    Disabled,       // Control is disabled
    Temporary       // Temporary changes
}
public enum ControlFlashDirection
{
    Forward,       
    Backward
}
public enum UpdateDetector
{
    None,
    State, 
    Image,
    ImageColor,
    ImageSize,
    ForeColor,
    FillColor,
    BorderColor,
}
#endregion

public struct ImageCache
{
    public Image Image { get; set; }
    public Color Color { get; set; }

    public ImageCache(Image image, Color color)
    {
        this.Image = image;
        Color = color;
    }
}

public class xButton : Control, IButtonControl, IDisposable
{
    #region IDisposable - implementation
    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            this._label?.Dispose();
            this._pictureBox?.Dispose();
        }

        base.Dispose(disposing);
    }
    #endregion

    #region IButtonControl - implementation
    private DialogResult _dialogResult = DialogResult.None;
    private bool _isDefault;

    [DefaultValue(typeof(DialogResult), "None")]
    public DialogResult DialogResult
    {
        get => _dialogResult;
        set
        {
            if (Enum.IsDefined(typeof(DialogResult), value))
                _dialogResult = value;
        }
    }
    public void NotifyDefault(bool value)
    {
        if (_isDefault != value)
            _isDefault = value;

        base.Invalidate();
    }
    public void PerformClick()
    {
        if (this.CanSelect)
            base.OnClick(EventArgs.Empty);
    }
    #endregion

    public xButton() : base()
    {
        base.SetStyle(ControlStyles.UserPaint, true);
        base.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        base.SetStyle(ControlStyles.ResizeRedraw, true);
        base.SetStyle(ControlStyles.DoubleBuffer, true);
        base.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
        UpdateStyles();

        // This must be set in order to be transparent at edges
        this.BackColor = Color.Transparent;
        this.Size = new Size(200, 25);
        //this.Font = new Font("Segoe UI Semibold", 9.75F, FontStyle.Bold);

        this._label = new NonDisabledLabel();
        this._label.Text = "Przykładowy tekst";
        this._label.AutoSize = true;
        this._label.BackColor = Color.Transparent;
        this._label.Font = this.Font;
        this._label.Enabled = false;

        this.Controls.Add(_label);
        this.DoubleBuffered = true;
    }

    // Flash
    private Color _flashColor = Color.Empty;
    private Color _flashStartColor = Color.Empty;
    private int _flashCount = 0;
    private bool _flash = false;

    // Controls
    private NonDisabledLabel _label;
    private PictureBox _pictureBox;

    #region State manager
    private ControlState _controlState = ControlState.Base;
    private ControlState _updateState = ControlState.Base;
    private UpdateDetector _updateDetector = UpdateDetector.None;

    private void IndicateUpdate(UpdateDetector update, ControlState state, bool sameStateRequired = false, bool invalidate = true)
    {
        if (sameStateRequired && _controlState != state)
            return;

        _controlState = state;

        if (update != _updateDetector || state != _updateState)
        {
            _updateDetector = update;
            _updateState = state;
        }

        if (invalidate)
            base.Invalidate();
    }
    #endregion

    #region Appearance properties assignment
    // Cache
    private Color _cacheForeColor = Color.Empty;
    private Color _cacheFillColor = Color.Empty;
    private Color _cacheBorderColor = Color.Empty;

    // Fore colors
    private Color _foreColor = Color.Black;
    private Color _foreHoverColor = Color.Black;
    private Color _foreActiveColor = Color.Black;
    private Color _foreDisabledColor = Color.Gray;

    // Border colors
    private Color _borderColor = Color.Gainsboro;
    private Color _borderHoverColor = Color.LightSteelBlue;
    private Color _borderActiveColor = Color.CornflowerBlue;
    private Color _borderDisabledColor = Color.Gray;

    // Fill colors
    private Color _fillColor = Color.White;
    private Color _fillHoverColor = Color.WhiteSmoke;
    private Color _fillActiveColor = Color.White;
    private Color _fillDisabledColor = Color.Gray;

    // Border
    private int _borderThickness = 1;
    private int _borderRadius = 5;
    private bool _borderShadows = true;
    private BorderStyle _borderStyle = BorderStyle.Solid;

    [Category("Appearance"), Description("Default control background, have to be Color.Transparent in order to support rounded border and custom fill color.")]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public override Color BackColor
    {
        get => base.BackColor;
        set => base.BackColor = Color.Transparent;
    }



    [Category("Appearance"), Description("Color that is applied to font.")]
    [DefaultValue(typeof(Color), "Black")]
    public override Color ForeColor
    {
        get => _foreColor;
        set
        {
            if (value == ForeColor)
                return;

            _foreColor = value;
            IndicateUpdate(UpdateDetector.ForeColor, ControlState.Base, true);
        }
    }
    [Category("Appearance"), Description("Color that is applied to font when mouse is over control.")]
    [DefaultValue(typeof(Color), "Black")]
    public Color ForeHoverColor
    {
        get => _foreHoverColor;
        set
        {
            if (value == ForeHoverColor)
                return;

            _foreHoverColor = value;
            IndicateUpdate(UpdateDetector.ForeColor, ControlState.Hover, true);
        }
    }
    [Category("Appearance"), Description("Color that is applied to font when control is active (focused etc.).")]
    [DefaultValue(typeof(Color), "Black")]
    public Color ForeActiveColor
    {
        get => _foreActiveColor;
        set
        {
            if (value == ForeActiveColor)
                return;

            _foreActiveColor = value;
            IndicateUpdate(UpdateDetector.ForeColor, ControlState.Active, true);
        }
    }
    [Category("Appearance"), Description("Color that is applied to font when control is disabled.")]
    [DefaultValue(typeof(Color), "Gray")]
    public Color ForeDisabledColor
    {
        get => _foreDisabledColor;
        set
        {
            if (value == ForeDisabledColor)
                return;

            _foreDisabledColor = value;
            IndicateUpdate(UpdateDetector.ForeColor, ControlState.Disabled, true);
        }
    }



    [Category("Appearance"), Description("Color that is applied to fill area inside border.")]
    [DefaultValue(typeof(Color), "White")]
    public Color FillColor
    {
        get => _fillColor;
        set
        {
            if (value == FillColor)
                return;

            _fillColor = value;
            IndicateUpdate(UpdateDetector.FillColor, ControlState.Base, true);
        }
    }
    [Category("Appearance"), Description("Color that is applied to fill area inside border when mouse is over control.")]
    [DefaultValue(typeof(Color), "WhiteSmoke")]
    public Color FillHoverColor
    {
        get => _fillHoverColor;
        set
        {
            if (value == FillHoverColor)
                return;

            _fillHoverColor = value;
            IndicateUpdate(UpdateDetector.FillColor, ControlState.Hover, true);
        }
    }
    [Category("Appearance"), Description("Color that is applied to fill area inside border when control is active (focused etc.).")]
    [DefaultValue(typeof(Color), "White")]
    public Color FillActiveColor
    {
        get => _fillActiveColor;
        set
        {
            if (value == FillActiveColor)
                return;

            _fillActiveColor = value;
            IndicateUpdate(UpdateDetector.FillColor, ControlState.Active, true);
        }
    }
    [Category("Appearance"), Description("Color that is applied to fill area inside border when control is disabled.")]
    [DefaultValue(typeof(Color), "Gray")]
    public Color FillDisabledColor
    {
        get => _fillDisabledColor;
        set
        {
            if (value == FillDisabledColor)
                return;

            _fillDisabledColor = value;
            IndicateUpdate(UpdateDetector.FillColor, ControlState.Disabled, true);
        }
    }



    [Category("Border"), Description("Color that is applied to border.")]
    [DefaultValue(typeof(Color), "Gainsboro")]
    public Color BorderColor
    {
        get => _borderColor;
        set
        {
            if (value == BorderColor)
                return;

            _borderColor = value;
            IndicateUpdate(UpdateDetector.BorderColor, ControlState.Base, true);
        }
    }
    [Category("Border"), Description("Color that is applied to border when mouse is over control.")]
    [DefaultValue(typeof(Color), "LightSteelBlue")]
    public Color BorderHoverColor
    {
        get => _borderHoverColor;
        set
        {
            if (value == BorderHoverColor)
                return;

            _borderHoverColor = value;
            IndicateUpdate(UpdateDetector.BorderColor, ControlState.Hover, true);
        }
    }
    [Category("Border"), Description("Color that is applied to border when control is active (focused etc.).")]
    [DefaultValue(typeof(Color), "CornflowerBlue")]
    public Color BorderActiveColor
    {
        get => _borderActiveColor;
        set
        {
            if (value == BorderActiveColor)
                return;

            _borderActiveColor = value;
            IndicateUpdate(UpdateDetector.BorderColor, ControlState.Active, true);
        }
    }
    [Category("Border"), Description("Color that is applied to border when control is disabled.")]
    [DefaultValue(typeof(Color), "Gray")]
    public Color BorderDisabledColor
    {
        get => _borderDisabledColor;
        set
        {
            if (value == BorderDisabledColor)
                return;

            _borderDisabledColor = value;
            IndicateUpdate(UpdateDetector.BorderColor, ControlState.Disabled, true);
        }
    }




    [Category("Border"), Description("Thickness of the border, if set to 0 then there is no border at all.")]
    [DefaultValue(1)]
    public int BorderThickness
    {
        get => _borderThickness;
        set
        {
            if (value == BorderThickness)
                return;

            _borderThickness = value;
            base.Invalidate();
        }
    }
    [Category("Border"), Description("Radius of the border, if set to 0 then is not rouneded at all.")]
    [DefaultValue(5)]
    public int BorderRadius
    {
        get => _borderRadius;
        set
        {
            if (value == BorderRadius)
                return;

            _borderRadius = value;
            base.Invalidate();
        }
    }
    [Category("Border"), Description("Draw additional border shadows")]
    [DefaultValue(true)]
    public bool BorderShadows
    {
        get => _borderShadows;
        set
        {
            if (value == BorderShadows)
                return;

            _borderShadows = value;
            base.Invalidate();
        }
    }

    [DefaultValue(typeof(BorderStyle), "Solid")]
    public BorderStyle BorderStyle
    {
        get => _borderStyle;
        set 
        {
            if (value == BorderStyle)
                return;

            _borderStyle = value;
            base.Invalidate();
        }
    }
    #endregion

    #region Behaviour properties
    // Behaviuor
    private bool _readOnly = false;
    private bool _enabled = true;

    // Text location
    private ObjectAlignment _textAlignment = ObjectAlignment.Center;

    [Category("Behaviour"), Description("Alignment of text")]
    [DefaultValue(typeof(ObjectAlignment), "Center")]
    public ObjectAlignment TextAlignment
    {
        get => _textAlignment;
        set
        {
            if (value == TextAlignment)
                return;

            _textAlignment = value;
            base.Invalidate();
        }
    }
    [Category("Behaviour"), Description("Basic readonly, if set to true you can only view, select and copy value. Functions does not work when true, otherwise it works.")]
    [DefaultValue(false)]
    public bool ReadOnly
    {
        get => _readOnly;
        set
        {
            if (value == ReadOnly)
                return;

            _readOnly = value;
            base.Invalidate();
        }
    }
    #endregion

    #region Image properties
    // Properties
    private Image _image = null;
    private Image _imageHover = null;
    private Image _imageActive = null;
    private Image _imageDisabled = null;
    private Color _imageColor = Color.Empty;
    private Color _imageHoverColor = Color.Empty;
    private Color _imageActiveColor = Color.Empty;
    private Color _imageDisabledColor = Color.Empty;
    private ObjectAlignment _imageAlignment = ObjectAlignment.Center;
    private ObjectDisplayOrder _displayOrder = ObjectDisplayOrder.ImageFirst;
    private int _spaceBetween = 4;
    private ImageSize _imageSize = ImageSize.FontHeight;
    private Size _imageCustomSize = new Size(16, 16);

    // Cache
    private Image _cacheImage = null;
    private Dictionary<ControlState, ImageCache> _cacheImages = null;
    private Color _cacheImageColor = Color.Empty;

    // Controller
    private Color _imageColorIndicator = Color.Empty;

    [DefaultValue(null)]
    public Image Image
    {
        get => _image;
        set
        {
            if (value == Image)
                return;

            _image = value;

            RemoveImageFromCache(ControlState.Base);
            IndicateUpdate(UpdateDetector.Image, ControlState.Base, true);
        }
    }
    [DefaultValue(null)]
    public Image ImageHover
    {
        get => _imageHover;
        set
        {
            if (value == ImageHover)
                return;

            _imageHover = value;

            RemoveImageFromCache(ControlState.Hover);
            IndicateUpdate(UpdateDetector.Image, ControlState.Hover, true);
        }
    }
    [DefaultValue(null)]
    public Image ImageActive
    {
        get => _imageActive;
        set
        {
            if (value == ImageActive)
                return;

            _imageActive = value;

            RemoveImageFromCache(ControlState.Active);
            IndicateUpdate(UpdateDetector.Image, ControlState.Active, true);
        }
    }
    [DefaultValue(null)]
    public Image ImageDisabled
    {
        get => _imageDisabled;
        set
        {
            if (value == ImageDisabled)
                return;

            _imageDisabled = value;

            RemoveImageFromCache(ControlState.Disabled);
            IndicateUpdate(UpdateDetector.Image, ControlState.Disabled, true);
        }
    }

    [Category("Image"), Description("Color that is applied to Image.")]
    [DefaultValue(typeof(Color), "")]
    public Color ImageColor
    {
        get => _imageColor;
        set
        {
            if (value == ImageColor)
                return;

            _imageColor = value;
            IndicateUpdate(UpdateDetector.ImageColor, ControlState.Base, true);
        }
    }
    [Category("Image"), Description("Color that is applied to Image when mouse is over control.")]
    [DefaultValue(typeof(Color), "")]
    public Color ImageHoverColor
    {
        get => _imageHoverColor;
        set
        {
            if (value == ImageHoverColor)
                return;

            _imageHoverColor = value;
            IndicateUpdate(UpdateDetector.ImageColor, ControlState.Hover, true);
        }
    }
    [Category("Image"), Description("Color that is applied to Image when control is active (focused etc.).")]
    [DefaultValue(typeof(Color), "")]
    public Color ImageActiveColor
    {
        get => _imageActiveColor;
        set
        {
            if (value == ImageActiveColor)
                return;

            _imageActiveColor = value;
            IndicateUpdate(UpdateDetector.ImageColor, ControlState.Active, true);
        }
    }
    [Category("Image"), Description("Color that is applied to Image when control is disabled.")]
    [DefaultValue(typeof(Color), "")]
    public Color ImageDisabledColor
    {
        get => _imageDisabledColor;
        set
        {
            if (value == ImageDisabledColor)
                return;
                
            _imageDisabledColor = value;
            IndicateUpdate(UpdateDetector.ImageColor, ControlState.Disabled, true);
        }
    }
    [Category("Image"), Description("Align of Image")]
    [DefaultValue(typeof(ObjectAlignment), "Center")]
    public ObjectAlignment ImageAlignment
    {
        get => _imageAlignment;
        set
        {
            if (value == ImageAlignment)
                return;

            _imageAlignment = value;
            base.Invalidate();
        }
    }
    [Category("Image"), Description("Display order of Image and text")]
    [DefaultValue(typeof(ObjectDisplayOrder), "ImageFirst")]
    public ObjectDisplayOrder DisplayOrder
    {
        get => _displayOrder;
        set
        {
            if (value == DisplayOrder)
                return;

            _displayOrder = value;
            base.Invalidate();
        }
    }
    [Category("Image"), Description("Space between Image and text")]
    [DefaultValue(4)]
    public int SpaceBetween
    {
        get => _spaceBetween;
        set
        {
            if (value == SpaceBetween)
                return;

            _spaceBetween = value;
            base.Invalidate();
        }
    }
    [Category("Image"), Description("Image size")]
    [DefaultValue(typeof(ImageSize), "FontHeight")]
    public ImageSize ImageSize
    {
        get => _imageSize;
        set
        {
            if (value == ImageSize)
                return;

            if (value == ImageSize.Custom)
            {
                if (ImageCustomSize.Width == 0 || ImageCustomSize.Height == 0)
                    _imageCustomSize = new Size(Font.Height, Font.Height);
            }

            _imageSize = value;

            _cacheImages?.Clear();
            IndicateUpdate(UpdateDetector.ImageSize, ControlState.None);
        }
    }
    [Category("Image"), Description("Image custom size")]
    [DefaultValue(typeof(Size), "16, 16")]
    public Size ImageCustomSize
    {
        get => _imageCustomSize;
        set
        {
            if (value == ImageCustomSize)
                return;

            _imageCustomSize = value;
            _cacheImages?.Clear();
            IndicateUpdate(UpdateDetector.ImageSize, ControlState.None);
        }
    }
    #endregion

    #region Split properties
    //private SplitLine _splitLine = SplitLine.None;
    //public SplitLine SplitLine
    //{
    //    get => _splitLine;
    //    set
    //    {
    //        if (value == _splitLine)
    //            return;

    //        _splitLine = value;
    //        base.Invalidate();
    //    }
    //}
    #endregion

    #region Control events and methods
    protected override void OnPaint(PaintEventArgs e)
    {
        Log("\tdetected - OnPaint");

        UpdateColors();
        UpdateImage();
        UpdateControlLocations();

        this._label.ForeColor = _cacheForeColor;

        DrawControl(e);

        // Reset state
        _updateDetector = UpdateDetector.None;
        _updateState = ControlState.None;
    }

    protected override void OnEnter(EventArgs e)
    {
        if (DesignMode)
            return;

        base.OnEnter(e);

        StopFlash();
        IndicateUpdate(UpdateDetector.State, ControlState.Hover);
    }
    protected override void OnLeave(EventArgs e)
    {
        if (DesignMode)
            return;

        base.OnLeave(e);
        IndicateUpdate(UpdateDetector.State, ControlState.Base);
    }
    protected override void OnMouseEnter(EventArgs e)
    {
        if (DesignMode)
            return;

        base.OnMouseEnter(e);

        StopFlash();
        IndicateUpdate(UpdateDetector.State, ControlState.Hover);
    }
    protected override void OnMouseLeave(EventArgs e)
    {
        if (DesignMode)
            return;

        base.OnMouseLeave(e);
        IndicateUpdate(UpdateDetector.State, ControlState.Base);
    }
    protected override void OnMouseDown(MouseEventArgs e)
    {
        if (DesignMode)
            return;

        base.OnMouseDown(e);

        if (e.Button == MouseButtons.Left)
        {
            Focus();
            IndicateUpdate(UpdateDetector.State, ControlState.Active);
        }
    }
    protected override void OnMouseUp(MouseEventArgs e)
    {
        if (DesignMode)
            return;

        base.OnMouseUp(e);

        if (e.Button == MouseButtons.Left)
            IndicateUpdate(UpdateDetector.State, ControlState.Hover);
    }
    protected override void OnTextChanged(EventArgs e)
    {
        base.OnTextChanged(e);
        this._label.Text = this.Text;
        base.Invalidate();
    }
    protected override void OnFontChanged(EventArgs e)
    {
        base.OnFontChanged(e);
        this._label.Font = this.Font;
        base.Invalidate();
    }
    protected override void OnVisibleChanged(EventArgs e)
    {
        base.OnVisibleChanged(e);
        base.Invalidate();
    }
    protected override void OnEnabledChanged(EventArgs e)
    {
        base.OnEnabledChanged(e);

        _enabled = base.Enabled;
        IndicateUpdate(UpdateDetector.State, !_enabled ? ControlState.Disabled : ControlState.Base);
    }
    protected override void OnResize(EventArgs e)
    {
        base.OnResize(e);
        base.Invalidate();
    }
    protected override void OnSizeChanged(EventArgs e)
    {
        base.OnSizeChanged(e);
        base.Invalidate();
    }
    #endregion

    #region Private methods
    private void UpdateColors()
    {
        if (_updateState == ControlState.Temporary)
            return;

        if (_updateDetector == UpdateDetector.ForeColor || _updateDetector == UpdateDetector.State)
            SwapColor(ref _cacheForeColor, ForeColor, ForeHoverColor, ForeActiveColor, ForeDisabledColor, false);
        if (_updateDetector == UpdateDetector.FillColor || _updateDetector == UpdateDetector.State)
            SwapColor(ref _cacheFillColor, FillColor, FillHoverColor, FillActiveColor, FillDisabledColor, false);
        if (_updateDetector == UpdateDetector.BorderColor || _updateDetector == UpdateDetector.State)
            SwapColor(ref _cacheBorderColor, BorderColor, BorderHoverColor, BorderActiveColor, BorderDisabledColor);
        if (_updateDetector == UpdateDetector.ImageColor || _updateDetector == UpdateDetector.State)
            SwapColor(ref _cacheImageColor, ImageColor, ImageHoverColor, ImageActiveColor, ImageDisabledColor, false);

        if (_cacheForeColor == Color.Empty)
            _cacheForeColor = ForeColor;
        if (_cacheFillColor == Color.Empty)
            _cacheFillColor = FillColor;
        if (_cacheBorderColor == Color.Empty)
            _cacheBorderColor = BorderColor;
    }
    private void UpdateImage()
    {
        UpdateDetector[] imageUpdate = new UpdateDetector[] { UpdateDetector.State, UpdateDetector.Image, UpdateDetector.ImageColor, UpdateDetector.ImageSize };

        // Check for any changes
        if (!imageUpdate.Contains(_updateDetector))
            return;

        // Create new empty Image
        Image image = null;

        if (IsImagePropertySet())
        {
            // Retrieve Image from cache
            if (_updateDetector == UpdateDetector.State && _imageColorIndicator == _cacheImageColor)
                image = GetImageFromCache();

            // Create new image based on current state Image
            if (image == null && _cacheImage != null)
            {
                //Set Image size
                image = UpdateImageSize((Bitmap)_cacheImage);

                // Set Image color
                if (_cacheImageColor != Color.Empty)
                    image = UpdateImageColor((Bitmap)image);
                else
                    _imageColorIndicator = Color.Empty;

                // Cache Image
                CacheImage(_cacheImageColor, (Bitmap)image);
            }
        }

        if (this._pictureBox == null)
            InitalizePictureBox();

        this._pictureBox.Size = image != null ? image.Size : new Size(0, 0);
        this._pictureBox.Image = image != null ? image : null;
        this._pictureBox.Visible = this._pictureBox.Image != null;
    }
    private void UpdateControlLocations()
    {
        bool positionText = true;
        bool positionImage = true;

        if (TextAlignment == ObjectAlignment.Left)
            this._label.TextAlign = ContentAlignment.MiddleLeft;
        else if (TextAlignment == ObjectAlignment.Center)
            this._label.TextAlign = ContentAlignment.MiddleCenter;
        else if (TextAlignment == ObjectAlignment.Right)
            this._label.TextAlign = ContentAlignment.MiddleRight;

        // margin from left/right edge
        int margin = 4;

        // Border radius calculation
        if (BorderRadius > 2 && (this.Height - this._label.Height < (BorderRadius - 2) * 2))
            margin += (int)Math.Ceiling((BorderRadius - 2) / 3.14);

        if (this._pictureBox != null && this._pictureBox.Visible)
        {
            // Location of text and Image
            if (TextAlignment == ImageAlignment)
            {
                if (DisplayOrder == ObjectDisplayOrder.ImageFirst || DisplayOrder == ObjectDisplayOrder.TextFirst)
                {
                    Control[] controls = GetControlOrder();

                    if (TextAlignment == ObjectAlignment.Left)
                    {
                        controls[0].Location = new Point(margin, Height / 2 - controls[0].Height / 2);
                        controls[1].Location = new Point(margin + controls[0].Width + SpaceBetween, Height / 2 - controls[1].Height / 2);
                    }
                    else if (TextAlignment == ObjectAlignment.Right)
                    {
                        controls[0].Location = new Point(Width - margin - controls[0].Width, Height / 2 - controls[0].Height / 2);
                        controls[1].Location = new Point(Width - margin - controls[0].Width - SpaceBetween - controls[1].Width, Height / 2 - controls[1].Height / 2);
                    }
                    else if (TextAlignment == ObjectAlignment.Center)
                    {
                        // To be exact equal there has to be added SpaceBetween but for some reason then is not equal
                        int totalWidth = GetControlsTotalWidth();

                        controls[0].Location = new Point(Width / 2 - totalWidth / 2, Height / 2 - controls[0].Height / 2);
                        controls[1].Location = new Point(controls[0].Location.X + controls[0].Width + SpaceBetween, Height / 2 - controls[1].Height / 2);
                    }

                    positionText = false;
                    positionImage = false;
                }
            }
        }
        else
            positionImage = false;

        // Location of Image
        if (positionImage)
        {
            if (ImageAlignment == ObjectAlignment.Left)
                this._pictureBox.Location = new Point(margin, Height / 2 - this._pictureBox.Height / 2);
            else if (ImageAlignment == ObjectAlignment.Right)
                this._pictureBox.Location = new Point(Width - margin * 2 - this._pictureBox.Width, Height / 2 - this._pictureBox.Height / 2);
            else if (ImageAlignment == ObjectAlignment.Center)
                this._pictureBox.Location = new Point(Width / 2 - this._pictureBox.Width / 2, Height / 2 - this._pictureBox.Height / 2);
        }

        // Location of text
        if (positionText)
        {
            if (TextAlignment == ObjectAlignment.Left)
                this._label.Location = new Point(margin, Height / 2 - this._label.Height / 2);
            else if (TextAlignment == ObjectAlignment.Right)
                this._label.Location = new Point(Width - margin * 2 - this._label.Width, Height / 2 - this._label.Height / 2);
            else if (TextAlignment == ObjectAlignment.Center)
                this._label.Location = new Point(Width / 2 - this._label.Width / 2, Height / 2 - this._label.Height / 2);
        }

        if (DisplayOrder == ObjectDisplayOrder.ImageFront && this._pictureBox != null && this._pictureBox.Visible)
            this._pictureBox.BringToFront();
        else if (DisplayOrder == ObjectDisplayOrder.TextFront)
            this._label.BringToFront();
    }
    private void DrawControl(PaintEventArgs e)
    {
        Rectangle clientRectangle = this.ClientRectangle;
        clientRectangle.Width -= 1;
        clientRectangle.Height -= 1;

        Rectangle fillRectangle = clientRectangle;
        Rectangle borderRectangle = clientRectangle;
        Rectangle shadowRectangle = clientRectangle;

        if (BorderThickness > 0)
        {
            borderRectangle.Inflate(-BorderThickness / 2, -BorderThickness / 2);
            fillRectangle.Inflate(-BorderThickness / 2, -BorderThickness / 2);
        }

        //if (BorderShadows && _enabled)
        if (BorderShadows)
        {
            fillRectangle.Inflate(-1, -1);
            borderRectangle.Inflate(-1, -1);
        }

        using (GraphicsPath pathFill = GetFigurePath(fillRectangle, BorderRadius))
        using (SolidBrush brushFill = new SolidBrush(_cacheFillColor))
        using (GraphicsPath pathBorder = GetFigurePath(borderRectangle, BorderRadius))
        using (Pen penBorder = new Pen(_cacheBorderColor, BorderThickness))
        {
            // Antialiasing (AntiAlias, HighQuality): Both AntiAlias (4) and HighQuality (2) enable antialiasing
            // to smooth edges. HighQuality might incorporate further enhancements for overall rendering quality.
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;

            // Specifies high quality, low speed rendering.
            e.Graphics.CompositingQuality = CompositingQuality.HighQuality;

            // Specifies high-quality, bicubic interpolation. Prefiltering is performed to ensure high-quality shrinking.
            // This mode produces the highest quality transformed images.
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

            // Specifies high quality, low speed rendering.
            e.Graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

            // Fill
            e.Graphics.FillPath(brushFill, pathFill);

            // Border shadows
            if (BorderShadows)
            {
                int extraRadius = BorderRadius > 0 ? 2 : 0;

                using (GraphicsPath pathShadow = GetFigurePath(shadowRectangle, BorderRadius + extraRadius))
                {
                    FuzzyShadows(pathShadow, e.Graphics, _cacheBorderColor, 222, 3, 1);
                }
            }

            // Border
            if (BorderThickness > 0)
            {
                switch (BorderStyle)
                {
                    case BorderStyle.None:
                        penBorder.Width = 0;
                        break;
                    case BorderStyle.Dot:
                        penBorder.DashStyle = DashStyle.Dot;
                        break;
                    case BorderStyle.Dash:
                        penBorder.DashStyle = DashStyle.Dash;
                        break;
                    case BorderStyle.DashDot:
                        penBorder.DashStyle = DashStyle.DashDot;
                        break;
                    case BorderStyle.DashDotDot:
                        penBorder.DashStyle = DashStyle.DashDotDot;
                        break;
                    case BorderStyle.Solid:
                    default:
                        penBorder.DashStyle = DashStyle.Solid;
                        break;
                }

                e.Graphics.DrawPath(penBorder, pathBorder);
            }
            //// Split line
            //if (this._pictureBox != null && this._pictureBox.Image != null)
            //{
            //    // Skip if none or image centered
            //    if (SplitLine == SplitLine.None || ImageAlignment == ObjectAlignment.Center)
            //        return;

            //    // If incorrect order then skip
            //    if (ImageAlignment == ObjectAlignment.Left && DisplayOrder == ObjectDisplayOrder.TextFirst ||
            //        ImageAlignment == ObjectAlignment.Right && DisplayOrder == ObjectDisplayOrder.ImageFirst)
            //        return;

            //    int xPos = borderRectangle.X;
            //    int yPosStart = borderRectangle.Y;
            //    int yPosEnd = borderRectangle.Y;

            //    if (ImageAlignment == ObjectAlignment.Left)
            //        xPos += this._pictureBox.Location.X + this._pictureBox.Width + SpaceBetween / 2;
            //    else if (ImageAlignment == ObjectAlignment.Right)
            //        xPos += borderRectangle.Width - (borderRectangle.Width - this._pictureBox.Location.X - SpaceBetween / 2);


            //    if (SplitLine == SplitLine.Space)
            //    {
            //        yPosStart += (int)(borderRectangle.Height * 0.1);
            //        yPosEnd += (int)(borderRectangle.Height * 0.9);
            //    }
            //    else if (SplitLine == SplitLine.Max)
            //        yPosEnd += borderRectangle.Height;

            //    penBorder.Width = 1;
            //    e.Graphics.DrawLine(penBorder, new Point(xPos, yPosStart), new Point(xPos, yPosEnd));
            //}
        }
    }
    private Control[] GetControlOrder()
    {
        if (DisplayOrder == ObjectDisplayOrder.ImageFirst)
            return new Control[] { this._pictureBox, this._label };
        else if (DisplayOrder == ObjectDisplayOrder.TextFirst)
            return new Control[] { this._label, this._pictureBox };

        return null;
    }
    private int GetControlsTotalWidth()
    {
        if (this._pictureBox != null && Image != null)
            return this._pictureBox.Width + this._label.Width;
        else
            return this._label.Width;
    }
    private void InitalizePictureBox()
    {
        if (this._pictureBox == null)
        {
            this._pictureBox = new PictureBox();
            this._pictureBox.Enabled = false;

            if (this._pictureBox.Parent != this)
                this._pictureBox.Parent = this;

            if (!this.Controls.Contains(this._pictureBox))
                this.Controls.Add(this._pictureBox);
        }
    }
    private GraphicsPath GetFigurePath(Rectangle rect, int radius)
    {
        GraphicsPath path = new GraphicsPath();

        if (radius > 0)
        {
            float curveSize = radius * 2F;

            path.StartFigure();
            path.AddArc(rect.X, rect.Y, curveSize, curveSize, 180, 90);
            path.AddArc(rect.Right - curveSize, rect.Y, curveSize, curveSize, 270, 90);
            path.AddArc(rect.Right - curveSize, rect.Bottom - curveSize, curveSize, curveSize, 0, 90);
            path.AddArc(rect.X, rect.Bottom - curveSize, curveSize, curveSize, 90, 90);
            path.CloseFigure();
        }
        else
            path.AddRectangle(rect);

        return path;
    }
    private void FuzzyShadows(GraphicsPath path, Graphics gr, Color base_color, int max_opacity, int width, int opaque_width)
    {
        int num_steps = width - opaque_width + 1;
        float delta = (float)max_opacity / num_steps / num_steps;

        float alpha = delta;

        for (int thickness = width; thickness >= opaque_width; thickness--)
        {
            Color color = Color.FromArgb((int)alpha, base_color.R, base_color.G, base_color.B);
            using (Pen pen = new Pen(color, thickness))
            {
                pen.EndCap = LineCap.Round;
                pen.StartCap = LineCap.Round;
                gr.DrawPath(pen, path);
            }
            alpha += delta;
        }
    }
    private bool CacheImage(Color color, Bitmap Bitmap)
    {
        // Checks if Bitmap is null
        if (Bitmap == null)
            return false;

        // Checks if Dictionary is null and initialize if needed
        if (_cacheImages == null)
            _cacheImages = new Dictionary<ControlState, ImageCache>();

        // Checks if Dictionary contains any key or if it does not contain this Color 
        // then creates new Dictionary entry
        if (_cacheImages.Count == 0 || !_cacheImages.ContainsKey(_controlState))
        {
            _cacheImages[_controlState] = new ImageCache(Bitmap, color);
            return true;
        }

        return false;
    }
    private Image GetImageFromCache()
    {
        if (DesignMode)
            return null;

        // Check if cache Dictionary is initialized or does it contains any key
        if (_cacheImages == null || _cacheImages.Count == 0)
            return null;

        // Returns Image if found key
        if (_cacheImages.TryGetValue(_controlState, out ImageCache imageCache))
        {
            if (_cacheImageColor != imageCache.Color)
                return null;

            Log($"Found cached Image for state {_controlState}");
            return imageCache.Image;
        }

        return null;
    }
    private bool IsImagePropertySet()
    {
        if (DesignMode)
        {
            _cacheImage = Image;
            return _cacheImage != null;
        }

        switch (_controlState)
        {
            case ControlState.Base:
                _cacheImage = Image;
                break;
            case ControlState.Hover:
                _cacheImage = ImageHover;
                break;
            case ControlState.Active:
                _cacheImage = ImageActive;
                break;
            case ControlState.Disabled:
                _cacheImage = ImageDisabled;
                break;
        }

        if (_cacheImage != null)
        {
            Log($"Updated _cacheImage for state {_controlState}");
            if (Image == null)
                return false;

            return true;
        }
        else if (Image != null)
        {
            Log($"Did not update _cacheImage for state {_controlState}, set Image");
            _cacheImage = Image;
            return true;
        }

        return false;
    }
    private Bitmap UpdateImageSize(Bitmap Bitmap)
    {
        Size size = new Size(0, 0);

        // If original size then return Bitmap
        if (ImageSize == ImageSize.Original)
            return (Bitmap)Bitmap;
        else if (ImageSize == ImageSize.FontHeight)
            size = new Size(this.Font.Height, this.Font.Height);
        else if (ImageSize == ImageSize.Custom)
            size = _imageCustomSize;


        // If new size equals current size return Bitmap
        if (Bitmap.Width == size.Width && Bitmap.Height == size.Height)
            return (Bitmap)Bitmap;

        // Calculate width and height with new desired size
        float nPercentW = ((float)size.Width / (float)Bitmap.Width);
        float nPercentH = ((float)size.Height / (float)Bitmap.Height);

        // New Width and Height
        int targetWidth = (int)(Bitmap.Width * nPercentW);
        int targetHeight = (int)(Bitmap.Height * nPercentH);

        // Create new Bitmap
        Bitmap bitmap = new Bitmap(targetWidth, targetHeight);
        Graphics g = Graphics.FromImage(bitmap);

        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
        g.SmoothingMode = SmoothingMode.HighQuality;
        g.PixelOffsetMode = PixelOffsetMode.HighSpeed;
        g.CompositingQuality = CompositingQuality.HighQuality;

        // Draw Bitmap with new width and height
        g.DrawImage(Bitmap, 0, 0, targetWidth, targetHeight);
        g.Dispose();

        return bitmap;
    }
    private Bitmap UpdateImageColor(Bitmap Bitmap)
    {
        Bitmap bitmap = new Bitmap(Bitmap.Width, Bitmap.Height);

        for (int y = 0; y < Bitmap.Height; y++)
        {
            for (int x = 0; x < Bitmap.Width; x++)
            {
                Color pixel = Bitmap.GetPixel(x, y);

                int newRed = _cacheImageColor.R;
                int newGreen = _cacheImageColor.G;
                int newBlue = _cacheImageColor.B;

                bitmap.SetPixel(x, y, Color.FromArgb(pixel.A, newRed, newGreen, newBlue));
            }
        }

        _imageColorIndicator = _cacheImageColor;

        return bitmap;
    }
    private void SwapColor(ref Color cacheC, Color baseC, Color hoverC, Color activeC, Color disabledC, bool keepFocus = true)
    {
        switch (_controlState)
        {
            case ControlState.Base:
                cacheC = (base.Focused && keepFocus) ? hoverC : baseC;
                break;
            case ControlState.Hover:
                cacheC = hoverC;
                break;
            case ControlState.Active:
                cacheC = activeC;
                break;
            case ControlState.Disabled:
                cacheC = disabledC;
                break;
        }
    }
    private Color[] GetFlashColors(Color startColor, Color endColor, int steps)
    {
        Color[] colors = new Color[steps];

        // Calculate the step size for each color component
        float stepR = (endColor.R - startColor.R) / (float)(steps - 1);
        float stepG = (endColor.G - startColor.G) / (float)(steps - 1);
        float stepB = (endColor.B - startColor.B) / (float)(steps - 1);

        // Generate the transition colors
        for (int i = 0; i < steps; i++)
        {
            int r = (int)(startColor.R + stepR * i);
            int g = (int)(startColor.G + stepG * i);
            int b = (int)(startColor.B + stepB * i);

            colors[i] = Color.FromArgb(r, g, b);
        }

        return colors;
    }
    private void RemoveImageFromCache(ControlState state)
    {
        if (DesignMode)
            return;

        if (_cacheImages != null && _cacheImages.ContainsKey(state))
        {
            Log($"Removed {state} from Image cache.");
            _cacheImages.Remove(state);
        }
    }
    #endregion

    #region Public methods
    public void SetTemporaryForeColor(Color color)
    {
        if (!_enabled)
            return;

        StopFlash();
        _cacheForeColor = color;
        IndicateUpdate(UpdateDetector.ForeColor, ControlState.Temporary);
    }
    public void SetTemporaryFillColor(Color color)
    {
        if (!_enabled)
            return;

        StopFlash();
        _cacheFillColor = color;
        IndicateUpdate(UpdateDetector.FillColor, ControlState.Temporary);
    }
    public void SetTemporaryBorderColor(Color color)
    {
        if (!_enabled)
            return;

        StopFlash();
        _cacheBorderColor = color;
        IndicateUpdate(UpdateDetector.BorderColor, ControlState.Temporary);
    }
    public void SetTemporaryImageColor(Color color)
    {
        if (!_enabled)
            return;

        StopFlash();
        _cacheImageColor = color;
        IndicateUpdate(UpdateDetector.ImageColor, ControlState.Temporary);
    }
    public void StartFlash(Color targetColor, int durationMillis, int colorShiftCount = 5, int flashCount = -1)
    {
        if (targetColor == null || durationMillis < 100 || colorShiftCount < 1 || flashCount == 0)
            return;

        if (!_enabled)
            return;

        _flashStartColor = _cacheBorderColor;
        _flashColor = targetColor;
        _flash = true;

        Color[] colors = GetFlashColors(_cacheBorderColor, _flashColor, colorShiftCount);
        ControlFlashDirection flashDirection = ControlFlashDirection.Forward;
        int stepTime = durationMillis / colorShiftCount;
        int i = 0;

        Task.Run(async () =>
        {
            while (_flash)
            {
                // Forwards
                if (flashDirection == ControlFlashDirection.Forward && i > colors.Length - 1)
                {
                    i = colors.Length - 2;
                    flashDirection = ControlFlashDirection.Backward;
                }

                // Backwards
                else if (flashDirection == ControlFlashDirection.Backward && i < 0)
                {
                    i = 1;
                    flashDirection = ControlFlashDirection.Forward;
                    _flashCount++;

                    if (_flashCount == flashCount)
                    {
                        StopFlash();
                        break;
                    }
                }

                // Wait first half of step time
                await Task.Delay(stepTime / 2);

                if (!_flash)
                    break;

                _cacheBorderColor = colors[i];
                IndicateUpdate(UpdateDetector.BorderColor, ControlState.None);

                if (flashDirection == ControlFlashDirection.Forward)
                    i++;
                else if (flashDirection == ControlFlashDirection.Backward)
                    i--;

                // Wait second half of step time
                await Task.Delay(stepTime / 2);
            }
        });
    }
    public void StopFlash()
    {
        if (_flash)
        {
            _flash = false;
            _cacheBorderColor = _flashStartColor;

            _flashCount = 0;
            _flashColor = Color.Empty;
            _flashStartColor = Color.Empty;

            IndicateUpdate(UpdateDetector.BorderColor, ControlState.Temporary);
        }
    }
    #endregion




    int i = 1;
    bool logs = false;
    private void Log(string message, bool newLine = true)
    {
        if (!logs)
            return;

        if (newLine)
            Console.WriteLine($"[{i++}]\t {Name} {message}");
        else
            Console.Write($"[{i++}]\t {Name} {message}");
    }
}



[ToolboxItem(false)]
public class NonDisabledLabel : Label
{
    public NonDisabledLabel()
    {
        this.SetStyle(ControlStyles.UserPaint, true); //Call in constructor, Use UserPaint
    }

    protected override void OnPaint(PaintEventArgs e)
    {
        if (!Enabled)
        {
            SolidBrush drawBrush = new SolidBrush(ForeColor); //Choose colour

            e.Graphics.DrawString(Text, Font, drawBrush, 0f, 0f); //Dra whatever text was on the label
        }
        else
        {
            base.OnPaint(e); //Default Forecolours
        }
    }
}