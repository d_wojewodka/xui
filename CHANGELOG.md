﻿### Update 1.0.0.1
- xButton - added memory leak prevention IDisposable
- xButton - fixed Positioning of Label and Image

### Update 1.0.0.2
- xButton - fixed image update, if there is not color set (Color.Empty image will be displayed with default colors)

### Update 1.0.0.3
- xButton - fixed Dispose method (NullReference)
- xButton - fixed Enable state

### Update 1.0.0.4
- xButton - fixed active image color

### Update 1.0.0.6
- xButton - changed name form InputButton to xButton
- xButton - fixed image update

### Update 1.0.0.8
- xButton - added caching mechanism for image, optimized call to update image
- xButton - fixed displaying correct image color

### Update 1.0.0.9
- xButton - improved colors cache

### Update 1.0.0.11
- xButton - fixed problem that when control was created and image was set, then image was not shown
- xButton - fixed image sizing not changes

### Update 1.0.0.13
- xButton - fixed problem with setting base colors that they were not changing
- xButton - added public method StartFlash and StopFlash

### Update 1.0.0.14
- xButton - improved flash code
- xButton - added preview xButton features in Preview project

### Update 1.0.1.0
- xButton - fixed color changes
- xButton - added option to have multiple images

### Update 1.0.1.1
- xButton - fixed image assignment

### Update 1.0.1.3
- xButton - fixed right alignment when TextAlignment == ImageAlignment and DisplayOrder ImageFront or TextFront
- xInput - some missing updates

### Update 1.0.2.0
- xButton - fixed performance issues
- xButton - fixed conditional rendering
- xButton - improved control state flow
- xButton - commented code
- xButton - fixed image issues

### Update 1.0.2.2
- xButton - fixed problem with resources
- xButton - changed Bitmap to Image, Bitmap is only used for changing color and resize

### Update 1.0.2.3
- xButton - fixed animation color restore (was problem with await Task.Delay that after wasn't check if animation is running)

### Update 1.0.3.0
- xToolTip - added xToolTip

### Update 1.0.3.2
- xToolTip - added TitleShadowColor
- xToolTip - fixed BorderColor

### Update 1.0.4.0
- xButton - fixed problem with image from resources (Disposal problem)
- xButton - improved button state management
- xButton - improved image caching mechanism
- xButton - added IButtonControl implementation
- xButton - added border style

### Update 1.0.4.2
- xButton - improved designer serialization (default values are hidden in .designer)
- xButton - renamed borderWidth to borderThickness