﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

[ToolboxItem(false)]
public class InputBaseX : Control
{
    #region Designer properties
    // Colors
    private Color _backgroundColor = Color.White;

    private Color _fontColor = Color.DarkGray;
    private Color _foreDefaultColor = Color.DarkGray;
    private Color _foreHoverColor = Color.DarkGray;
    private Color _foreFocusColor = Color.DarkGray;

    private Color _borderColor = Color.Gray;
    private Color _borderDefaultColor = Color.Gray;
    private Color _borderHoverColor = Color.LightSkyBlue;
    private Color _borderFocusColor = Color.DodgerBlue;




    // Default color
    private Color _defaultForeColor = Color.Empty;
    private Color _defaultFillColor = Color.Empty;
    private Color _defaultBorderColor = Color.Empty;

    // Base colors
    private Color _baseForeColor = Color.Empty;
    private Color _baseFillColor = Color.Empty;
    private Color _baseBorderColor = Color.Empty;

    // Hover colors
    private Color _hoverForeColor = Color.Empty;
    private Color _hoverFillColor = Color.Empty;
    private Color _hoverBorderColor = Color.Empty;

    // Focus colors
    private Color _focusForeColor = Color.Empty;
    private Color _focusFillColor = Color.Empty;
    private Color _focusBorderColor = Color.Empty;


    // Behaviuor
    private bool _readOnly = false;

    // Border and filling
    private int _borderRadius = 5;
    private bool _borderShadows = true;
    private GraphicsPath _shape = null;
    private GraphicsPath _fill = null;
    #endregion

    #region Properties assignment
    [Category("Appearance"), Description("Default control background, should be Color.Transparent in order to properly use BackgroundColor fill.")]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public override Color BackColor
    {
        get => base.BackColor;
        set => base.BackColor = Color.Transparent;
    }

    [Category("Appearance"), Description("Default control background, should be Color.Transparent in order to properly use BackgroundColor fill.")]
    [DefaultValue(typeof(Color), "Black")]
    public override Color ForeColor
    {
        get => base.ForeColor;
        set
        {
            base.ForeColor = value;
            base.Invalidate();
        }
    }

    [Category("Appearance"), Description("Color that is filled inside border.")]
    [DefaultValue(typeof(Color), "White")]
    public Color BackgroundColor
    {
        get => _backgroundColor;
        set
        {
            this._backgroundColor = value;
            base.Invalidate();
        }
    }


    [Category("Border"), Description("Radius of the border, if set to 0 then is not rouneded at all.")]
    [DefaultValue(5)]
    public int BorderRadius
    {
        get => _borderRadius;
        set
        {
            _borderRadius = value;
            base.Invalidate();
        }
    }


    [Category("Border"), Description("Draw additional border shadows")]
    [DefaultValue(true)]
    public bool BorderShadows
    {
        get => _borderShadows;
        set
        {
            _borderShadows = value;
            base.Invalidate();
        }
    }

    [Category("Border"), Description("Visible border color, if you wanna use validation then assign color to this property. It will be overrided when text changes and goes back to normal.")]
    [DefaultValue(typeof(Color), "Gray")]
    public Color BorderColor
    {
        get => _borderColor;
        set
        {
            _borderColor = value;
            base.Invalidate();
        }
    }

    [Category("Border"), Description("Default color of border.")]
    [DefaultValue(typeof(Color), "Gray")]
    public Color BorderDefaultColor
    {
        get => _borderDefaultColor;
        set
        {
            _borderDefaultColor = value;
            BorderColor = value;
            base.Invalidate();
        }
    }

    [Category("Border"), Description("Color of border when user hovers over it.")]
    [DefaultValue(typeof(Color), "LightSkyBlue")]
    public Color BorderHoverColor
    {
        get => _borderHoverColor;
        set
        {
            _borderHoverColor = value;
            base.Invalidate();
        }
    }

    [Category("Border"), Description("Color of border when user clicked inside control.")]
    [DefaultValue(typeof(Color), "DodgerBlue")]
    public Color BorderFocusColor
    {
        get => _borderFocusColor;
        set
        {
            _borderFocusColor = value;
            base.Invalidate();
        }
    }


    [Category("Behaviour"), Description("Basic readonly, if set to true you can only view, select and copy value. Functions does not work when true, otherwise it works.")]
    [DefaultValue(false)]
    public bool ReadOnly
    {
        get => _readOnly;
        set
        {
            _readOnly = value;
            base.Invalidate();
        }
    }
    #endregion

    public InputBaseX() : base()
    {
        base.SetStyle(ControlStyles.UserPaint, true);
        base.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        base.SetStyle(ControlStyles.ResizeRedraw, true);
        base.SetStyle(ControlStyles.DoubleBuffer, true);
        base.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
        UpdateStyles();

        this.BackColor = Color.Transparent;
        this.ForeColor = Color.Black;
        this.Size = new Size(200, 25);
        this.DoubleBuffered = true;
    }


    #region Control events and methods
    protected override void OnPaint(PaintEventArgs e)
    {
        e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
        Bitmap bitmap = new Bitmap(base.Width, base.Height);

        using (Graphics g = Graphics.FromImage(bitmap))
        {
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            e.Graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.PixelOffsetMode = PixelOffsetMode.HighSpeed;
            e.Graphics.CompositingQuality = CompositingQuality.HighQuality;

            Color borderColor;
            bool drawBorderShadow = false;

            if (!ReadOnly)
                borderColor = BorderFocusColor;
            else if (!ReadOnly && (this.ClientRectangle.Contains(this.PointToClient(Cursor.Position))))
                borderColor = BorderHoverColor;
            else
                borderColor = BorderColor;


            if (borderColor != BorderDefaultColor)
                drawBorderShadow = true;

            // Shadows
            Matrix _Matrix = new Matrix();
            int borderShadows = BorderShadows ? 2 : 0;

            if (BorderShadows && drawBorderShadow)
            {
                GraphicsPath shadows = new RoundedRectangle((float)base.Width, (float)base.Height, (float)this.BorderRadius + borderShadows, 0.5f, 0.5f).Path;
                FuzzyShadows(shadows, e.Graphics, borderColor, 222, 2, 1);
            }

            // Border shape
            this._shape = new RoundedRectangle((float)base.Width - borderShadows, (float)base.Height - borderShadows, (float)this.BorderRadius, 0f, 0f).Path;
            if (BorderShadows)
            {
                _Matrix.Translate((float)borderShadows / 2, (float)borderShadows / 2);
                _shape.Transform(_Matrix);
            }

            e.Graphics.DrawPath(new Pen(borderColor), this._shape);

            using (SolidBrush brush = new SolidBrush(this._backgroundColor))
            {
                // Fill area
                float fillMargin = BorderRadius > 0 ? 0.5f : 1f;
                this._fill = new RoundedRectangle((float)(base.Width - fillMargin - borderShadows), (float)(base.Height - fillMargin - borderShadows), (float)this.BorderRadius, 0.5f, 0.5f).Path;
                _fill.Transform(_Matrix);

                e.Graphics.FillPath((Brush)brush, this._fill);
            }

            Transparency.MakeTransparent(this, e.Graphics);
        }

        base.OnPaint(e);
    }

    protected override void OnEnter(EventArgs e)
    {
        base.OnEnter(e);
        base.Invalidate();
    }
    protected override void OnLeave(EventArgs e)
    {
        base.OnLeave(e);
        base.Invalidate();
    }
    protected override void OnMouseEnter(EventArgs e)
    {
        base.OnMouseEnter(e);
        base.Invalidate();
    }
    protected override void OnMouseLeave(EventArgs e)
    {
        base.OnMouseLeave(e);
        base.Invalidate();
    }
    #endregion

    #region Methods
    private void FuzzyShadows(GraphicsPath path, Graphics gr, Color base_color, int max_opacity, int width, int opaque_width)
    {
        int num_steps = width - opaque_width + 1;
        float delta = (float)max_opacity / num_steps / num_steps;

        float alpha = delta;

        for (int thickness = width; thickness >= opaque_width; thickness--)
        {
            Color color = Color.FromArgb((int)alpha, base_color.R, base_color.G, base_color.B);
            using (Pen pen = new Pen(color, thickness))
            {
                pen.EndCap = LineCap.Round;
                pen.StartCap = LineCap.Round;
                gr.DrawPath(pen, path);
            }
            alpha += delta;
        }
    }
    #endregion
}
