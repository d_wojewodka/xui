﻿using System;
using System.Windows.Forms;
using static Win32Helpers;

public class NativeWindowFuzzyShadows : NativeWindow, IDisposable
{
    #region Constructor
    public NativeWindowFuzzyShadows(bool enable) { }
    #endregion

    #region Dispose
    private bool _isDisposed;

    public bool IsDisposed
    {
        get => _isDisposed;
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!_isDisposed)
        {
            if (disposing)
                // release unmanaged resources
                _isDisposed = true;

            this.ReleaseHandle();
        }
    }
    #endregion


    public void InitializeWindowShadows() { }
    public void SetEnable(bool enable) { }
    public void SetFocus(bool focus) { }
    public void SetOwner(IntPtr owner) { }



    protected override void WndProc(ref Message m) { }



    private void DestroyWindowShadows() { }
    private void RegisterEvents() { }
    private void UnregisterEvents() { }
    private void OnVisibleChanged(object sender, EventArgs e) { }
    private void OnMouseDown(object sender, FormShadowResizeArgs e) { }
    private int GetSizeMode() { return 0; }
    private void CloseShadows() { }
    private void ShowBorder() { }
    private void UpdateFocus() { }
    private void UpdateLocation() { }
    private void UpdateSizes() { }
    private void WindowPosChanged(WINDOWPOS location) { }
    private void Size(IntPtr wParam, IntPtr lParam) { }
}
