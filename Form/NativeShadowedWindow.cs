﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UIX.Form;
using static Win32Helpers;

internal enum ShadowDockPositon
{
    Left = 0,
    Top = 1,
    Right = 2,
    Bottom = 3
}

internal delegate void FormShadowResizeEventHandler(object sender, FormShadowResizeArgs args);
internal class FormShadowResizeArgs : EventArgs
{
    private readonly ShadowDockPositon _side;
    private readonly HitTest _mode;
    private readonly Point _point;

    public ShadowDockPositon Side
    {
        get { return _side; }
    }

    public Point ScreenPoint
    {
        get
        {
            return _point;
        }
    }

    public HitTest Mode
    {
        get { return _mode; }
    }

    internal FormShadowResizeArgs(ShadowDockPositon side, HitTest mode, Point point)
    {
        _side = side;
        _mode = mode;
        _point = point;
    }
}

internal class NativeShadowedWindow : NativeWindow
{
    internal readonly string CLASS_NAME = "PureForm";

    [StructLayout(LayoutKind.Sequential)]
    public struct ANIMATIONINFO
    {
        public uint cbSize;
        public int iMinAnimate;
    };

    public static uint SPIF_SENDCHANGE = 0x02;
    public static uint SPI_SETANIMATION = 0x0049;
    public static uint SPI_GETANIMATION = 0x0048;


    private IntPtr parentWindowHWnd => _parentWindow.Handle;
    private Form _parentWindow;

    private FormShadow _topFormShadow;
    private FormShadow _leftFormShadow;
    private FormShadow _bottomFormShadow;
    private FormShadow _rightFormShadow;

    private WINDOWPOS _lastLocation;

    private readonly List<FormShadow> _shadows = new List<FormShadow>();
    private Color _shadowColor = Color.Black;

    private bool _isEnabled;
    private bool _isFocused = false;
    private bool _isWindowMinimized = false;
    private bool _isAnimationDelayed = false;

    private bool _isInitialized = false;

    private Bitmap[] _cachedImages;

    internal Bitmap ActiveBitmapTemplate => _cachedImages?[1];
    internal Bitmap InactiveBitmapTemplate => _cachedImages?[2];

    internal bool Resizable => _parentWindow.FormBorderStyle == FormBorderStyle.SizableToolWindow || _parentWindow.FormBorderStyle == FormBorderStyle.Sizable;

    public Color ShadowColor
    {
        get => _shadowColor;
        set
        {
            _shadowColor = value;

            InitializeBitmapCache();

            if (!_isInitialized)
                return;

            foreach (var sideShadow in _shadows)
            {
                sideShadow.UpdateShadow();
            }
        }
    }

    private ShadowPatternType _shadowPattern = ShadowPatternType.None;
    private Bitmap _shadowPatternImage = null;


    [Browsable(true)]
    [Description("Shadow pattern type.")]
    public ShadowPatternType ShadowPattern
    {
        get => _shadowPattern;
        set
        {
            _shadowPattern = value;

            InitializeBitmapCache();

            if (!_isInitialized)
                return;

            foreach (var sideShadow in _shadows)
            {
                sideShadow.UpdateShadow();
            }
        }
    }
    [Category("Images")]
    [Browsable(true)]
    [Description("Custom shadow pattern image.")]
    public Bitmap ShadowPatternImage
    {
        get => _shadowPatternImage;
        set
        {
            _shadowPatternImage = value;

            InitializeBitmapCache();

            if (!_isInitialized)
                return;

            foreach (var sideShadow in _shadows)
            {
                sideShadow.UpdateShadow();
            }
        }
    }

    public bool IsInitialized => _isInitialized;
    public bool IsEnabled => _isEnabled;
    public void InitializeShadows()
    {
        _topFormShadow = new FormShadow(ShadowDockPositon.Top, parentWindowHWnd, this);
        _leftFormShadow = new FormShadow(ShadowDockPositon.Left, parentWindowHWnd, this);
        _bottomFormShadow = new FormShadow(ShadowDockPositon.Bottom, parentWindowHWnd, this);
        _rightFormShadow = new FormShadow(ShadowDockPositon.Right, parentWindowHWnd, this);

        _shadows.Add(_topFormShadow);
        _shadows.Add(_leftFormShadow);
        _shadows.Add(_bottomFormShadow);
        _shadows.Add(_rightFormShadow);

        User32.ShowWindow(_topFormShadow.Handle, ShowWindowStyles.SW_SHOWNOACTIVATE);
        User32.ShowWindow(_leftFormShadow.Handle, ShowWindowStyles.SW_SHOWNOACTIVATE);
        User32.ShowWindow(_bottomFormShadow.Handle, ShowWindowStyles.SW_SHOWNOACTIVATE);
        User32.ShowWindow(_rightFormShadow.Handle, ShowWindowStyles.SW_SHOWNOACTIVATE);

        _topFormShadow.ExternalResizeEnable = Resizable;
        _leftFormShadow.ExternalResizeEnable = Resizable;
        _bottomFormShadow.ExternalResizeEnable = Resizable;
        _rightFormShadow.ExternalResizeEnable = Resizable;

        _isInitialized = true;

        AssignHandle(parentWindowHWnd);
        AlignSideShadowToTopMost();

        ShadowColor = _shadowColor;
    }

    public void Enable(bool enable)
    {
        if (_isEnabled && !enable)
        {
            ShowBorder(false);
            UnregisterEvents();
        }
        else if (!_isEnabled && enable)
        {
            RegisterEvents();

            if (_parentWindow != null)
            {
                UpdateSizes(_parentWindow.Width, _parentWindow.Height);
                UpdateLocations(new WINDOWPOS
                {
                    x = _parentWindow.Left,
                    y = _parentWindow.Top,
                    cx = _parentWindow.Width,
                    cy = _parentWindow.Height,

                    flags = (uint)SetWindowPosFlags.SWP_SHOWWINDOW
                });

            }
        }

        _isEnabled = enable;
    }

    public void SetOwner(IntPtr owner)
    {
        foreach (FormShadow sideShadow in _shadows)
        {
            sideShadow.SetOwner(owner);
        }
    }
    public void SetFocus()
    {
        if (!_isEnabled)
            return;

        UpdateFocus(true);
    }
    public void KillFocus()
    {
        if (!_isEnabled)
            return;

        UpdateFocus(false);
    }

    public NativeShadowedWindow(Form window, bool enable = true)
    {
        //ANIMATIONINFO ai = new ANIMATIONINFO();
        //ai.cbSize =(uint)Marshal.SizeOf(ai);
        //ai.iMinAnimate = 400;   // turn all animation off
        //SystemParametersInfo(SPI_GETANIMATION, (uint)Marshal.SizeOf(typeof(ANIMATIONINFO)), ref ai, 0);

        _parentWindow = window;
        _isEnabled = enable;

        InitializeBitmapCache();
    }

    private void InitializeBitmapCache()
    {
        Bitmap bitmap = null;

        if (ShadowPattern == ShadowPatternType.Around)
            bitmap = PureFormResources.ShadowAround;
        else if (ShadowPattern == ShadowPatternType.Real)
            bitmap = PureFormResources.ShadowReal;
        else if (ShadowPattern == ShadowPatternType.Custom && ShadowPatternImage != null)
            bitmap = ShadowPatternImage;
        else
            bitmap = PureFormResources.ShadowNone;

        if (bitmap == null)
            throw new NullReferenceException("Bitmap is empty");

        _cachedImages = new Bitmap[3];
        _cachedImages[0] = bitmap;
        _cachedImages[1] = (Bitmap)bitmap.Clone();
        _cachedImages[2] = (Bitmap)bitmap.Clone();

        BlendBitmapWithColor(_cachedImages[1], ShadowColor);
        BlendBitmapWithColor(_cachedImages[2], ShadowColor, 0.6f);
    }

    private void BlendBitmapWithColor(Bitmap source, Color color, float alphaDepth = 1f)
    {
        var rect = new Rectangle(0, 0, source.Width, source.Height);

        if (alphaDepth > 1)
            alphaDepth = 1;

        var bmp = new LockBitmap(source);
        bmp.LockBits();

        for (var y = rect.Top; y < rect.Bottom; y++)
        {
            for (var x = rect.Left; x < rect.Right; x++)
            {
                var targetColor = bmp.GetPixel(x, y);

                var alpha = Convert.ToByte(targetColor.A * alphaDepth);

                var r = color.R;
                var g = color.G;
                var b = color.B;

                bmp.SetPixel(x, y, Color.FromArgb(alpha, r, g, b));
            }
        }

        bmp.UnlockBits();
    }

    protected override void WndProc(ref Message m)
    {
        if (!_isEnabled || IsDisposed)
        {
            base.WndProc(ref m);
            return;
        }

        var msg = (WindowsMessages)m.Msg;

        switch (msg)
        {

            case WindowsMessages.WM_WINDOWPOSCHANGED:
                _lastLocation = (WINDOWPOS)Marshal.PtrToStructure(m.LParam, typeof(WINDOWPOS));
                WindowPosChanged(_lastLocation);
                base.WndProc(ref m);

                break;
            case WindowsMessages.WM_ACTIVATEAPP:
                {
                    var className = new StringBuilder(256);

                    if (m.LParam != IntPtr.Zero && User32.GetClassName(m.LParam, className, className.Capacity) != 0)
                    {
                        var hWndShadow = m.LParam;
                        var name = className.ToString();

                        if (name.StartsWith(CLASS_NAME) && _isFocused && _shadows.Exists(p => p.Handle == hWndShadow))
                            return;
                    }


                    if (m.WParam == WinHelper.FALSE)
                    {
                        _isFocused = false;
                        KillFocus();
                    }
                    else
                    {
                        _isFocused = true;
                        SetFocus();
                    }

                }

                break;
            case WindowsMessages.WM_SIZE:
                base.WndProc(ref m);
                Size(m.WParam, m.LParam);

                break;
            default:
                base.WndProc(ref m);

                break;
        }

    }

    private void DestroyShadows()
    {
        CloseShadows();

        _parentWindow = null;
    }

    private void RegisterEvents()
    {
        foreach (var sideShadow in _shadows)
        {
            sideShadow.MouseDown += HandleSideMouseDown;
        }

        if (_parentWindow != null)
            _parentWindow.VisibleChanged += HandleWindowVisibleChanged;
    }

    private void HandleWindowVisibleChanged(object sender, EventArgs e)
    {
        ShowBorder(_parentWindow.Visible);
    }

    private void UnregisterEvents()
    {
        foreach (var sideShadow in _shadows)
        {
            sideShadow.MouseDown -= HandleSideMouseDown;
        }

        if (_parentWindow != null)
            _parentWindow.VisibleChanged -= HandleWindowVisibleChanged;
    }

    private void HandleSideMouseDown(object sender, FormShadowResizeArgs e)
    {
        if (e.Mode == HitTest.HTNOWHERE || e.Mode == HitTest.HTCAPTION)
            return;

        if (Resizable)
            User32.SendMessage(parentWindowHWnd, (uint)WindowsMessages.WM_SYSCOMMAND, (IntPtr)(GetSizeMode(e.Mode)), IntPtr.Zero);
    }


    private int GetSizeMode(HitTest handles)
    {
        switch (handles)
        {
            case HitTest.HTNOWHERE:
            case HitTest.HTCAPTION:
                return 0;
            case HitTest.HTLEFT:
                return (int)ResizeDirection.Left;
            case HitTest.HTRIGHT:
                return (int)ResizeDirection.Right;
            case HitTest.HTTOP:
                return (int)ResizeDirection.Top;
            case HitTest.HTTOPLEFT:
                return (int)ResizeDirection.TopLeft;
            case HitTest.HTTOPRIGHT:
                return (int)ResizeDirection.TopRight;
            case HitTest.HTBOTTOM:
                return (int)ResizeDirection.Bottom;
            case HitTest.HTBOTTOMLEFT:
                return (int)ResizeDirection.BottomLeft;
            case HitTest.HTBOTTOMRIGHT:
                return (int)ResizeDirection.BottomRight;
            default:
                return 0;
        }

    }

    private void CloseShadows()
    {
        foreach (var sideShadow in _shadows)
        {
            sideShadow.Close();
        }

        _shadows.Clear();

        _topFormShadow = null;
        _bottomFormShadow = null;
        _leftFormShadow = null;
        _rightFormShadow = null;
    }

    private void ShowBorder(bool show)
    {
        var action = new Action(() =>
        {

            foreach (var sideShadow in _shadows)
            {
                sideShadow.Show(show);
            }

            if (show)
                _isWindowMinimized = false;
        });

        if (show == true && _isWindowMinimized)
        {
            if (_isAnimationDelayed)
                return;

            _isAnimationDelayed = true;

            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(300);

                if (_isAnimationDelayed)
                    _parentWindow.Invoke(new MethodInvoker(action));

                _isAnimationDelayed = false;

            });

        }
        else
        {
            action();

            _isAnimationDelayed = false;
        }

    }

    private void UpdateFocus(bool isFocused)
    {
        foreach (var sideShadow in _shadows)
        {
            sideShadow.ParentWindowIsFocused = isFocused;
        }


    }

    private void UpdateSizes(int width, int height)
    {
        foreach (var sideShadow in _shadows)
        {
            sideShadow.SetSize(width, height);
        }
    }

    private void UpdateLocations(WINDOWPOS location)
    {
        foreach (var sideShadow in _shadows)
        {
            sideShadow.SetLocation(location);
        }

        if ((location.flags & (uint)SetWindowPosFlags.SWP_HIDEWINDOW) != 0)
            ShowBorder(false);
        else if ((location.flags & (uint)SetWindowPosFlags.SWP_SHOWWINDOW) != 0)
            ShowBorder(true);
    }

    private void AlignSideShadowToTopMost()
    {
        if (_shadows == null)
            return;

        foreach (var sideShadow in _shadows)
        {
            sideShadow.UpdateZOrder();
        }
    }




    private void WindowPosChanged(WINDOWPOS location)
    {
        if (!_isEnabled)
            return;

        UpdateLocations(location);
    }


    private void Size(IntPtr wParam, IntPtr lParam)
    {
        int width = (int)WinHelper.LOWORD(lParam);
        int height = (int)WinHelper.HIWORD(lParam);

        if (!_isEnabled)
            return;

        if ((int)wParam == 2 || (int)wParam == 1) // maximized/minimized
        {
            //if ((int)wParam == 1) { }

            _isWindowMinimized = true;

            ShowBorder(false);

        }
        else
        {
            var rect = new RECT();

            User32.GetWindowRect(_parentWindow.TopLevelControl != null ? _parentWindow.TopLevelControl.Handle : _parentWindow.Handle, ref rect);

            UpdateSizes(rect.right - rect.left, rect.bottom - rect.top);
            ShowBorder(true);

        }
    }


    #region Dispose

    private bool _isDisposed;

    /// <summary>
    /// IsDisposed status
    /// </summary>
    public bool IsDisposed
    {
        get => _isDisposed;
    }

    /// <summary>
    /// Standard Dispose
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Dispose
    /// </summary>
    /// <param name="disposing">True if disposing, false otherwise</param>
    protected virtual void Dispose(bool disposing)
    {
        if (!_isDisposed)
        {
            if (disposing)
                // release unmanaged resources

                _isDisposed = true;

            DestroyShadows();
            UnregisterEvents();
            this.ReleaseHandle();

            _parentWindow = null;
        }
    }
    #endregion
}