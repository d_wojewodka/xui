﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

[ToolboxItem(false)]
public class InputBase : Control
{
    #region Designer properties
    // Copy of colors
    private Color _foreBaseColor = Color.Empty;
    private Color _borderBaseColor = Color.Empty;
    private Color _fillBaseColor = Color.Empty;

    // Fore colors
    private Color _foreColor = Color.Empty;
    private Color _foreHoverColor = Color.Empty;
    private Color _foreActiveColor = Color.Empty;
    private Color _foreDisabledColor = Color.Empty;

    // Border colors
    private Color _borderColor = Color.Empty;
    private Color _borderHoverColor = Color.Empty;
    private Color _borderActiveColor = Color.Empty;
    private Color _borderDisabledColor = Color.Empty;

    // Fill colors
    private Color _fillColor = Color.Empty;
    private Color _fillHoverColor = Color.Empty;
    private Color _fillActiveColor = Color.Empty;
    private Color _fillDisabledColor = Color.Empty;









    // Behaviuor
    private bool _readOnly = false;
    private bool _enabled = true;

    // Border and filling
    private int _borderRadius = 5;
    private bool _borderShadows = true;
    private GraphicsPath _shape = null;
    private GraphicsPath _fill = null;
    #endregion

    #region Appearance properties assignment
    [Category("Appearance"), Description("Default control background, have to be Color.Transparent in order to support rounded border and custom fill color.")]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public override Color BackColor
    {
        get => base.BackColor;
        set => base.BackColor = Color.Transparent;
    }



    [Category("Appearance"), Description("Color that is applied to font.")]
    [DefaultValue(typeof(Color), "Black")]
    public override Color ForeColor
    {
        get => _foreColor;
        set
        {
            _foreBaseColor = value;
            _foreColor = value;
            base.Invalidate();
        }
    }
    [Category("Appearance"), Description("Color that is applied to font when mouse is over control.")]
    [DefaultValue(typeof(Color), "Black")]
    public Color ForeHoverColor
    {
        get => _foreHoverColor;
        set
        {
            _foreHoverColor = value;
            base.Invalidate();
        }
    }
    [Category("Appearance"), Description("Color that is applied to font when control is active (focused etc.).")]
    [DefaultValue(typeof(Color), "Black")]
    public Color ForeActiveColor
    {
        get => _foreActiveColor;
        set
        {
            _foreActiveColor = value;
            base.Invalidate();
        }
    }
    [Category("Appearance"), Description("Color that is applied to font when control is disabled.")]
    [DefaultValue(typeof(Color), "Black")]
    public Color ForeDisabledColor
    {
        get => _foreDisabledColor;
        set
        {
            _foreDisabledColor = value;
            base.Invalidate();
        }
    }



    [Category("Appearance"), Description("Color that is applied to fill area inside border.")]
    [DefaultValue(typeof(Color), "White")]
    public Color FillColor
    {
        get => _fillColor;
        set
        {
            _fillBaseColor = value;
            _fillColor = value;
            base.Invalidate();
        }
    }
    [Category("Appearance"), Description("Color that is applied to fill area inside border when mouse is over control.")]
    [DefaultValue(typeof(Color), "White")]
    public Color FillHoverColor
    {
        get => _fillHoverColor;
        set
        {
            _fillHoverColor = value;
            base.Invalidate();
        }
    }
    [Category("Appearance"), Description("Color that is applied to fill area inside border when control is active (focused etc.).")]
    [DefaultValue(typeof(Color), "White")]
    public Color FillActiveColor
    {
        get => _fillActiveColor;
        set
        {
            _fillActiveColor = value;
            base.Invalidate();
        }
    }
    [Category("Appearance"), Description("Color that is applied to fill area inside border when control is disabled.")]
    [DefaultValue(typeof(Color), "White")]
    public Color FillDisabledColor
    {
        get => _fillDisabledColor;
        set
        {
            _fillDisabledColor = value;
            base.Invalidate();
        }
    }



    [Category("Border"), Description("Color that is applied to border.")]
    [DefaultValue(typeof(Color), "Gray")]
    public Color BorderColor
    {
        get => _borderColor;
        set
        {
            _borderBaseColor = value;
            _borderColor = value;
            base.Invalidate();
        }
    }
    [Category("Border"), Description("Color that is applied to border when mouse is over control.")]
    [DefaultValue(typeof(Color), "Gray")]
    public Color BorderHoverColor
    {
        get => _borderHoverColor;
        set
        {
            _borderHoverColor = value;
            base.Invalidate();
        }
    }
    [Category("Border"), Description("Color that is applied to border when control is active (focused etc.).")]
    [DefaultValue(typeof(Color), "Gray")]
    public Color BorderActiveColor
    {
        get => _borderActiveColor;
        set
        {
            _borderActiveColor = value;
            base.Invalidate();
        }
    }
    [Category("Border"), Description("Color that is applied to border when control is disabled.")]
    [DefaultValue(typeof(Color), "Gray")]
    public Color BorderDisabledColor
    {
        get => _borderDisabledColor;
        set
        {
            _borderDisabledColor = value;
            base.Invalidate();
        }
    }





    [Category("Border"), Description("Radius of the border, if set to 0 then is not rouneded at all.")]
    [DefaultValue(5)]
    public int BorderRadius
    {
        get => _borderRadius;
        set
        {
            _borderRadius = value;
            base.Invalidate();
        }
    }


    [Category("Border"), Description("Draw additional border shadows")]
    [DefaultValue(true)]
    public bool BorderShadows
    {
        get => _borderShadows;
        set
        {
            _borderShadows = value;
            base.Invalidate();
        }
    }
    #endregion

    #region Properties assignment
    [Category("Behaviour"), Description("Basic readonly, if set to true you can only view, select and copy value. Functions does not work when true, otherwise it works.")]
    [DefaultValue(false)]
    public bool ReadOnly
    {
        get => _readOnly;
        set
        {
            _readOnly = value;
            base.Invalidate();
        }
    }
    [Category("Behaviour"), Description("Set that control is enabled on disabled.")]
    [DefaultValue(false)]
    public new bool Enabled
    {
        get => _enabled;
        set
        {
            Console.WriteLine("Setting enabled to " + value);
            _enabled = value;
            base.Invalidate();
        }
    }
    #endregion

    private Label lbl;

    public InputBase() : base()
    {
        base.SetStyle(ControlStyles.UserPaint, true);
        base.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        base.SetStyle(ControlStyles.ResizeRedraw, true);
        base.SetStyle(ControlStyles.DoubleBuffer, true);
        base.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
        UpdateStyles();

        lbl = new Label();
        lbl.Text = "Przykładowy tekst";
        this.Controls.Add(lbl);
        lbl.Location = new Point(4, this.Height / 2 + lbl.Height / 4);

        this.BackColor = Color.Transparent;

        this.ForeColor = Color.Black;
        this.ForeHoverColor = Color.Black;
        this.ForeActiveColor = Color.Black;
        this.ForeDisabledColor = Color.Gray;

        this.FillColor = Color.White;
        this.FillHoverColor = Color.WhiteSmoke;
        this.FillActiveColor = Color.White;
        this.FillDisabledColor = Color.White;

        this.BorderColor = Color.Gainsboro;
        this.BorderHoverColor = Color.LightSteelBlue;
        this.BorderActiveColor = Color.CornflowerBlue;
        this.BorderDisabledColor = Color.Gray;



        this.Size = new Size(200, 25);
        this.DoubleBuffered = true;
    }

    public void SetTemporaryForeColor(Color color)
    {
        _foreColor = color;
        base.Invalidate();
    }
    public void SetTemporaryFillColor(Color color)
    {
        _fillColor = color;
        base.Invalidate();
    }
    public void SetTemporaryBorderColor(Color color)
    {
        _borderColor = color;
        base.Invalidate();
    }


    #region Control events and methods
    protected override void OnPaint(PaintEventArgs e)
    {
        e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
        Bitmap bitmap = new Bitmap(base.Width, base.Height);

        using (Graphics g = Graphics.FromImage(bitmap))
        {
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            e.Graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.PixelOffsetMode = PixelOffsetMode.HighSpeed;
            e.Graphics.CompositingQuality = CompositingQuality.HighQuality;

            Color foreColor = ForeColor;
            Color fillColor = FillColor;
            Color borderColor = BorderColor;
            bool renderShadows = true;
            bool isActive = false;

            Console.WriteLine(_enabled);

            if (!_enabled)
            {
                foreColor = ForeDisabledColor;
                fillColor = FillDisabledColor;
                borderColor = BorderDisabledColor;
                renderShadows = false;
            }
            else if (!ReadOnly)
            {
                if (isActive)
                {
                    foreColor = ForeActiveColor;
                    fillColor = FillActiveColor;
                    borderColor = BorderActiveColor;
                }
                else if (this.ClientRectangle.Contains(this.PointToClient(Cursor.Position)))
                {
                    Console.WriteLine("C");
                    foreColor = ForeHoverColor;
                    fillColor = FillHoverColor;
                    borderColor = BorderHoverColor;
                }
            }


            lbl.ForeColor = foreColor;

            // Shadows
            Matrix _Matrix = new Matrix();
            int borderShadows = BorderShadows ? 2 : 0;

            if (BorderShadows && renderShadows)
            {
                GraphicsPath shadows = new RoundedRectangle((float)base.Width, (float)base.Height, (float)this.BorderRadius + borderShadows, 0f, 0f).Path;
                FuzzyShadows(shadows, e.Graphics, borderColor, 222, 3, 1);
            }

            // Border shape
            using (Pen pen = new Pen(borderColor))
            {
                this._shape = new RoundedRectangle((float)base.Width - borderShadows, (float)base.Height - borderShadows, (float)this.BorderRadius, 0f, 0f).Path;

                if (BorderShadows)
                {
                    _Matrix.Translate((float)borderShadows / 2, (float)borderShadows / 2);
                    _shape.Transform(_Matrix);
                }

                e.Graphics.DrawPath(pen, this._shape);
            }

            // Fill area
            using (SolidBrush brush = new SolidBrush(fillColor))
            {
                float fillMargin = BorderRadius > 0 ? 0.5f : 1f;
                this._fill = new RoundedRectangle((float)(base.Width - fillMargin - borderShadows), (float)(base.Height - fillMargin - borderShadows), (float)this.BorderRadius, .5f, .5f).Path;
                _fill.Transform(_Matrix);

                e.Graphics.FillPath((Brush)brush, this._fill);
            }

            Transparency.MakeTransparent(this, e.Graphics);
        }

        base.OnPaint(e);
    }

    protected override void OnEnter(EventArgs e)
    {
        base.OnEnter(e);
        base.Invalidate();
    }
    protected override void OnLeave(EventArgs e)
    {
        base.OnLeave(e);
        base.Invalidate();
    }
    protected override void OnMouseEnter(EventArgs e)
    {
        base.OnMouseEnter(e);
        base.Invalidate();
    }
    protected override void OnMouseLeave(EventArgs e)
    {
        base.OnMouseLeave(e);
        base.Invalidate();
    }
    #endregion

    #region Methods
    private void FuzzyShadows(GraphicsPath path, Graphics gr, Color base_color, int max_opacity, int width, int opaque_width)
    {
        int num_steps = width - opaque_width + 1;
        float delta = (float)max_opacity / num_steps / num_steps;

        float alpha = delta;

        for (int thickness = width; thickness >= opaque_width; thickness--)
        {
            Color color = Color.FromArgb((int)alpha, base_color.R, base_color.G, base_color.B);
            using (Pen pen = new Pen(color, thickness))
            {
                pen.EndCap = LineCap.Round;
                pen.StartCap = LineCap.Round;
                gr.DrawPath(pen, path);
            }
            alpha += delta;
        }
    }
    #endregion
}
