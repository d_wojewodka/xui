﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

[ToolboxItem(false)]
public class InputTextBox : TextBox
{
    private string _placeholderText;
    private Color _placeholderColor = SystemColors.InactiveCaption;
    private int _paddingRight = 0;

    private bool _isPlaceholderVisible = true;

    private Label label;

    public string Placeholder
    {
        get => _placeholderText;
        set
        {
            _placeholderText = value;
            this.Invalidate();
        }
    }

    public Color PlaceholderColor
    {
        get => _placeholderColor;
        set
        {
            _placeholderColor = value;
            this.Invalidate();
        }
    }

    public int PaddingRight
    {
        get => _paddingRight;
        set
        {
            _paddingRight = value;
            User32.SendMessage(Handle, WindowsApi.EM_SETMARGINS, (IntPtr)WindowsApi.EC_RIGHTMARGIN, (IntPtr)(_paddingRight << 16));
            label.Width = _paddingRight;
            this.Invalidate();
        }
    }

    [Browsable(false)]
    public bool IsPlaceholderVisible
    {
        get => _isPlaceholderVisible;
        set => _isPlaceholderVisible = value;
    }

    protected override void OnTextChanged(EventArgs e)
    {
        base.OnTextChanged(e);

        _isPlaceholderVisible = string.IsNullOrEmpty(this.Text);

        this.Invalidate();
    }

    public InputTextBox() : base()
    {
        base.SetStyle(ControlStyles.DoubleBuffer, true);
        base.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
        UpdateStyles();

        label = new Label();
        label.Dock = DockStyle.Right;
        label.Width = _paddingRight;
        label.BackColor = Color.Transparent;

        this.Controls.Add(label);
    }

    protected override void OnHandleCreated(EventArgs e)
    {
        base.OnHandleCreated(e);
        User32.SendMessage(Handle, WindowsApi.EM_SETMARGINS, (IntPtr)WindowsApi.EC_RIGHTMARGIN, (IntPtr)(_paddingRight << 16));
    }

    protected override void WndProc(ref Message m)
    {
        if (m.Msg == WindowsApi.EM_SETMARGINS && m.WParam.ToInt32() == WindowsApi.EC_RIGHTMARGIN)
            m.Result = IntPtr.Zero;

        base.WndProc(ref m);

        if (m.Msg == 0xF)
        {
            if (_isPlaceholderVisible && string.IsNullOrEmpty(this.Text) && !string.IsNullOrEmpty(this.Placeholder))
            {
                Rectangle rect = new Rectangle(this.ClientRectangle.Location, this.ClientSize);
                rect.Width -= _paddingRight;
                //rect.Y += 1;
                //rect.X -= 1;

                TextFormatFlags textFormatFlags = TextFormatFlags.Top | TextFormatFlags.Left;

                if (this.Multiline)
                    textFormatFlags |= TextFormatFlags.WordBreak;

                using (Graphics g = this.CreateGraphics())
                {
                    TextRenderer.DrawText(g, this.Placeholder, this.Font, rect,
                        _placeholderColor, this.BackColor,
                        textFormatFlags);
                }
            }
        }
    }
}