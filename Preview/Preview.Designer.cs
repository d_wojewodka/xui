﻿namespace Preview
{
    partial class Preview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Preview));
            this.titlebar = new System.Windows.Forms.Panel();
            this.title = new System.Windows.Forms.Label();
            this.minimize = new System.Windows.Forms.Button();
            this.maximize = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.formTP = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.shadowColorButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.customButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.noneButton = new System.Windows.Forms.Button();
            this.realButton = new System.Windows.Forms.Button();
            this.aroundButton = new System.Windows.Forms.Button();
            this.xButtonTP = new System.Windows.Forms.TabPage();
            this.xButtonJ = new xButton();
            this.xButtonI = new xButton();
            this.xButtonH = new xButton();
            this.xButtonG = new xButton();
            this.xButtonF = new xButton();
            this.xButtonE = new xButton();
            this.xButtonD = new xButton();
            this.xButtonC = new xButton();
            this.xButtonB = new xButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.xButtonFlash = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.xButtonColor = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.xButtonDuration = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.xButtonSteps = new System.Windows.Forms.NumericUpDown();
            this.xButtonLoad = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.foreDefault = new System.Windows.Forms.Panel();
            this.fillDefault = new System.Windows.Forms.Panel();
            this.foreHover = new System.Windows.Forms.Panel();
            this.xButtonA = new xButton();
            this.fillHover = new System.Windows.Forms.Panel();
            this.foreActive = new System.Windows.Forms.Panel();
            this.fillActive = new System.Windows.Forms.Panel();
            this.borderDefault = new System.Windows.Forms.Panel();
            this.foreDisabled = new System.Windows.Forms.Panel();
            this.borderHover = new System.Windows.Forms.Panel();
            this.borderActive = new System.Windows.Forms.Panel();
            this.imageColorDefault = new System.Windows.Forms.Panel();
            this.imageColorHover = new System.Windows.Forms.Panel();
            this.fillDisabled = new System.Windows.Forms.Panel();
            this.imageColorActive = new System.Windows.Forms.Panel();
            this.borderDisabled = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.custom = new System.Windows.Forms.RadioButton();
            this.original = new System.Windows.Forms.RadioButton();
            this.fontSize = new System.Windows.Forms.RadioButton();
            this.imageColorDisabled = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.textFront = new System.Windows.Forms.RadioButton();
            this.imageFront = new System.Windows.Forms.RadioButton();
            this.textFirst = new System.Windows.Forms.RadioButton();
            this.imageFirst = new System.Windows.Forms.RadioButton();
            this.imageDefault = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.imageRight = new System.Windows.Forms.RadioButton();
            this.imageCenter = new System.Windows.Forms.RadioButton();
            this.imageLeft = new System.Windows.Forms.RadioButton();
            this.imageHover = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textRight = new System.Windows.Forms.RadioButton();
            this.textCenter = new System.Windows.Forms.RadioButton();
            this.textLeft = new System.Windows.Forms.RadioButton();
            this.imageActive = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.imageDisabled = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.xButton = new xButton();
            this.label14 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.xToolTipTP = new System.Windows.Forms.TabPage();
            this.xToolTipListBox = new System.Windows.Forms.ListBox();
            this.questionMark = new System.Windows.Forms.PictureBox();
            this.SysInfoTP = new System.Windows.Forms.TabPage();
            this.textBox = new System.Windows.Forms.TextBox();
            this.listBox = new System.Windows.Forms.ListBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.xButton1 = new xButton();
            this.xButton11 = new xButton();
            this.xButton9 = new xButton();
            this.xButton6 = new xButton();
            this.xToolTip = new xToolTip();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.xButton2 = new xButton();
            this.titlebar.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.formTP.SuspendLayout();
            this.xButtonTP.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xButtonFlash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xButtonDuration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xButtonSteps)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageDefault)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageHover)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageDisabled)).BeginInit();
            this.xToolTipTP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.questionMark)).BeginInit();
            this.SysInfoTP.SuspendLayout();
            this.SuspendLayout();
            // 
            // titlebar
            // 
            this.titlebar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.titlebar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.titlebar.Controls.Add(this.title);
            this.titlebar.Controls.Add(this.minimize);
            this.titlebar.Controls.Add(this.maximize);
            this.titlebar.Controls.Add(this.exit);
            this.titlebar.Location = new System.Drawing.Point(0, 0);
            this.titlebar.Name = "titlebar";
            this.titlebar.Size = new System.Drawing.Size(1209, 36);
            this.titlebar.TabIndex = 0;
            this.titlebar.Click += new System.EventHandler(this.titlebar_Click);
            this.titlebar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.titlebar_MouseDown);
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.title.Location = new System.Drawing.Point(3, 7);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(189, 21);
            this.title.TabIndex = 3;
            this.title.Text = "Sample application title";
            this.title.Click += new System.EventHandler(this.title_Click);
            // 
            // minimize
            // 
            this.minimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.minimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.minimize.Location = new System.Drawing.Point(1109, 2);
            this.minimize.Name = "minimize";
            this.minimize.Size = new System.Drawing.Size(32, 32);
            this.minimize.TabIndex = 2;
            this.minimize.Text = "_";
            this.minimize.UseVisualStyleBackColor = true;
            this.minimize.Click += new System.EventHandler(this.minimize_Click);
            // 
            // maximize
            // 
            this.maximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.maximize.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.maximize.Location = new System.Drawing.Point(1143, 2);
            this.maximize.Name = "maximize";
            this.maximize.Size = new System.Drawing.Size(32, 32);
            this.maximize.TabIndex = 1;
            this.maximize.Text = "□";
            this.maximize.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.maximize.UseVisualStyleBackColor = true;
            this.maximize.Click += new System.EventHandler(this.maximize_Click);
            // 
            // exit
            // 
            this.exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.exit.Location = new System.Drawing.Point(1176, 2);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(32, 32);
            this.exit.TabIndex = 0;
            this.exit.Text = "X";
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.formTP);
            this.tabControl.Controls.Add(this.xButtonTP);
            this.tabControl.Controls.Add(this.xToolTipTP);
            this.tabControl.Controls.Add(this.SysInfoTP);
            this.tabControl.Location = new System.Drawing.Point(0, 37);
            this.tabControl.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl.Name = "tabControl";
            this.tabControl.Padding = new System.Drawing.Point(0, 0);
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1209, 656);
            this.tabControl.TabIndex = 7;
            // 
            // formTP
            // 
            this.formTP.Controls.Add(this.xButton2);
            this.formTP.Controls.Add(this.button3);
            this.formTP.Controls.Add(this.shadowColorButton);
            this.formTP.Controls.Add(this.button2);
            this.formTP.Controls.Add(this.customButton);
            this.formTP.Controls.Add(this.button1);
            this.formTP.Controls.Add(this.noneButton);
            this.formTP.Controls.Add(this.realButton);
            this.formTP.Controls.Add(this.aroundButton);
            this.formTP.Location = new System.Drawing.Point(4, 22);
            this.formTP.Name = "formTP";
            this.formTP.Size = new System.Drawing.Size(1201, 630);
            this.formTP.TabIndex = 2;
            this.formTP.Text = "Form";
            this.formTP.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(343, 43);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(156, 30);
            this.button3.TabIndex = 8;
            this.button3.Text = "Border color picker";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.borderColorButton_Click);
            // 
            // shadowColorButton
            // 
            this.shadowColorButton.Location = new System.Drawing.Point(343, 7);
            this.shadowColorButton.Name = "shadowColorButton";
            this.shadowColorButton.Size = new System.Drawing.Size(156, 30);
            this.shadowColorButton.TabIndex = 7;
            this.shadowColorButton.Text = "Shadow color picker";
            this.shadowColorButton.UseVisualStyleBackColor = true;
            this.shadowColorButton.Click += new System.EventHandler(this.shadowColorButton_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(541, 7);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(114, 30);
            this.button2.TabIndex = 2;
            this.button2.Text = "Start animation";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.startAnimationButton_Click);
            // 
            // customButton
            // 
            this.customButton.Location = new System.Drawing.Point(8, 94);
            this.customButton.Name = "customButton";
            this.customButton.Size = new System.Drawing.Size(75, 23);
            this.customButton.TabIndex = 6;
            this.customButton.Text = "custom";
            this.customButton.UseVisualStyleBackColor = true;
            this.customButton.Click += new System.EventHandler(this.customButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(541, 43);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 30);
            this.button1.TabIndex = 1;
            this.button1.Text = "Stop animation";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.stopAnimationButton_Click);
            // 
            // noneButton
            // 
            this.noneButton.Location = new System.Drawing.Point(8, 7);
            this.noneButton.Name = "noneButton";
            this.noneButton.Size = new System.Drawing.Size(75, 23);
            this.noneButton.TabIndex = 5;
            this.noneButton.Text = "none";
            this.noneButton.UseVisualStyleBackColor = true;
            this.noneButton.Click += new System.EventHandler(this.noneButton_Click);
            // 
            // realButton
            // 
            this.realButton.Location = new System.Drawing.Point(8, 65);
            this.realButton.Name = "realButton";
            this.realButton.Size = new System.Drawing.Size(75, 23);
            this.realButton.TabIndex = 4;
            this.realButton.Text = "real";
            this.realButton.UseVisualStyleBackColor = true;
            this.realButton.Click += new System.EventHandler(this.realButton_Click);
            // 
            // aroundButton
            // 
            this.aroundButton.Location = new System.Drawing.Point(8, 36);
            this.aroundButton.Name = "aroundButton";
            this.aroundButton.Size = new System.Drawing.Size(75, 23);
            this.aroundButton.TabIndex = 3;
            this.aroundButton.Text = "around";
            this.aroundButton.UseVisualStyleBackColor = true;
            this.aroundButton.Click += new System.EventHandler(this.aroundButton_Click);
            // 
            // xButtonTP
            // 
            this.xButtonTP.Controls.Add(this.xButtonJ);
            this.xButtonTP.Controls.Add(this.xButtonI);
            this.xButtonTP.Controls.Add(this.xButtonH);
            this.xButtonTP.Controls.Add(this.xButtonG);
            this.xButtonTP.Controls.Add(this.xButtonF);
            this.xButtonTP.Controls.Add(this.xButtonE);
            this.xButtonTP.Controls.Add(this.xButtonD);
            this.xButtonTP.Controls.Add(this.xButtonC);
            this.xButtonTP.Controls.Add(this.xButtonB);
            this.xButtonTP.Controls.Add(this.groupBox1);
            this.xButtonTP.Controls.Add(this.xButtonLoad);
            this.xButtonTP.Controls.Add(this.label5);
            this.xButtonTP.Controls.Add(this.label6);
            this.xButtonTP.Controls.Add(this.label7);
            this.xButtonTP.Controls.Add(this.label8);
            this.xButtonTP.Controls.Add(this.label9);
            this.xButtonTP.Controls.Add(this.label10);
            this.xButtonTP.Controls.Add(this.label11);
            this.xButtonTP.Controls.Add(this.label12);
            this.xButtonTP.Controls.Add(this.label13);
            this.xButtonTP.Controls.Add(this.foreDefault);
            this.xButtonTP.Controls.Add(this.fillDefault);
            this.xButtonTP.Controls.Add(this.foreHover);
            this.xButtonTP.Controls.Add(this.xButtonA);
            this.xButtonTP.Controls.Add(this.fillHover);
            this.xButtonTP.Controls.Add(this.foreActive);
            this.xButtonTP.Controls.Add(this.fillActive);
            this.xButtonTP.Controls.Add(this.borderDefault);
            this.xButtonTP.Controls.Add(this.foreDisabled);
            this.xButtonTP.Controls.Add(this.borderHover);
            this.xButtonTP.Controls.Add(this.borderActive);
            this.xButtonTP.Controls.Add(this.imageColorDefault);
            this.xButtonTP.Controls.Add(this.imageColorHover);
            this.xButtonTP.Controls.Add(this.fillDisabled);
            this.xButtonTP.Controls.Add(this.imageColorActive);
            this.xButtonTP.Controls.Add(this.borderDisabled);
            this.xButtonTP.Controls.Add(this.panel5);
            this.xButtonTP.Controls.Add(this.imageColorDisabled);
            this.xButtonTP.Controls.Add(this.panel4);
            this.xButtonTP.Controls.Add(this.imageDefault);
            this.xButtonTP.Controls.Add(this.panel3);
            this.xButtonTP.Controls.Add(this.imageHover);
            this.xButtonTP.Controls.Add(this.panel1);
            this.xButtonTP.Controls.Add(this.imageActive);
            this.xButtonTP.Controls.Add(this.label17);
            this.xButtonTP.Controls.Add(this.imageDisabled);
            this.xButtonTP.Controls.Add(this.label16);
            this.xButtonTP.Controls.Add(this.button7);
            this.xButtonTP.Controls.Add(this.label15);
            this.xButtonTP.Controls.Add(this.xButton);
            this.xButtonTP.Controls.Add(this.label14);
            this.xButtonTP.Controls.Add(this.button6);
            this.xButtonTP.Controls.Add(this.button8);
            this.xButtonTP.Controls.Add(this.button5);
            this.xButtonTP.Location = new System.Drawing.Point(4, 22);
            this.xButtonTP.Name = "xButtonTP";
            this.xButtonTP.Padding = new System.Windows.Forms.Padding(3);
            this.xButtonTP.Size = new System.Drawing.Size(1201, 630);
            this.xButtonTP.TabIndex = 0;
            this.xButtonTP.Text = "xButton";
            this.xButtonTP.UseVisualStyleBackColor = true;
            // 
            // xButtonJ
            // 
            this.xButtonJ.FillDisabledColor = System.Drawing.Color.White;
            this.xButtonJ.ImageCustomSize = new System.Drawing.Size(0, 0);
            this.xButtonJ.Location = new System.Drawing.Point(1065, 169);
            this.xButtonJ.Name = "xButtonJ";
            this.xButtonJ.Size = new System.Drawing.Size(26, 25);
            this.xButtonJ.TabIndex = 92;
            this.xButtonJ.Text = " ";
            // 
            // xButtonI
            // 
            this.xButtonI.FillDisabledColor = System.Drawing.Color.White;
            this.xButtonI.ImageCustomSize = new System.Drawing.Size(0, 0);
            this.xButtonI.Location = new System.Drawing.Point(1033, 169);
            this.xButtonI.Name = "xButtonI";
            this.xButtonI.Size = new System.Drawing.Size(26, 25);
            this.xButtonI.TabIndex = 91;
            this.xButtonI.Text = " ";
            // 
            // xButtonH
            // 
            this.xButtonH.FillDisabledColor = System.Drawing.Color.White;
            this.xButtonH.ImageCustomSize = new System.Drawing.Size(0, 0);
            this.xButtonH.Location = new System.Drawing.Point(1001, 169);
            this.xButtonH.Name = "xButtonH";
            this.xButtonH.Size = new System.Drawing.Size(26, 25);
            this.xButtonH.TabIndex = 90;
            this.xButtonH.Text = " ";
            // 
            // xButtonG
            // 
            this.xButtonG.FillDisabledColor = System.Drawing.Color.White;
            this.xButtonG.ImageCustomSize = new System.Drawing.Size(0, 0);
            this.xButtonG.Location = new System.Drawing.Point(969, 169);
            this.xButtonG.Name = "xButtonG";
            this.xButtonG.Size = new System.Drawing.Size(26, 25);
            this.xButtonG.TabIndex = 89;
            this.xButtonG.Text = " ";
            // 
            // xButtonF
            // 
            this.xButtonF.FillDisabledColor = System.Drawing.Color.White;
            this.xButtonF.ImageCustomSize = new System.Drawing.Size(0, 0);
            this.xButtonF.Location = new System.Drawing.Point(937, 169);
            this.xButtonF.Name = "xButtonF";
            this.xButtonF.Size = new System.Drawing.Size(26, 25);
            this.xButtonF.TabIndex = 88;
            this.xButtonF.Text = " ";
            // 
            // xButtonE
            // 
            this.xButtonE.FillDisabledColor = System.Drawing.Color.White;
            this.xButtonE.ImageCustomSize = new System.Drawing.Size(0, 0);
            this.xButtonE.Location = new System.Drawing.Point(905, 169);
            this.xButtonE.Name = "xButtonE";
            this.xButtonE.Size = new System.Drawing.Size(26, 25);
            this.xButtonE.TabIndex = 87;
            this.xButtonE.Text = " ";
            // 
            // xButtonD
            // 
            this.xButtonD.FillDisabledColor = System.Drawing.Color.White;
            this.xButtonD.ImageCustomSize = new System.Drawing.Size(0, 0);
            this.xButtonD.Location = new System.Drawing.Point(873, 169);
            this.xButtonD.Name = "xButtonD";
            this.xButtonD.Size = new System.Drawing.Size(26, 25);
            this.xButtonD.TabIndex = 86;
            this.xButtonD.Text = " ";
            // 
            // xButtonC
            // 
            this.xButtonC.FillDisabledColor = System.Drawing.Color.White;
            this.xButtonC.ImageCustomSize = new System.Drawing.Size(0, 0);
            this.xButtonC.Location = new System.Drawing.Point(841, 169);
            this.xButtonC.Name = "xButtonC";
            this.xButtonC.Size = new System.Drawing.Size(26, 25);
            this.xButtonC.TabIndex = 85;
            this.xButtonC.Text = " ";
            // 
            // xButtonB
            // 
            this.xButtonB.FillDisabledColor = System.Drawing.Color.White;
            this.xButtonB.ImageCustomSize = new System.Drawing.Size(0, 0);
            this.xButtonB.Location = new System.Drawing.Point(809, 169);
            this.xButtonB.Name = "xButtonB";
            this.xButtonB.Size = new System.Drawing.Size(26, 25);
            this.xButtonB.TabIndex = 84;
            this.xButtonB.Text = " ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.xButtonFlash);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.xButtonColor);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.xButtonDuration);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.xButtonSteps);
            this.groupBox1.Location = new System.Drawing.Point(769, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(329, 128);
            this.groupBox1.TabIndex = 83;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Animator";
            // 
            // button9
            // 
            this.button9.Image = global::Preview.Properties.Resources.stop;
            this.button9.Location = new System.Drawing.Point(177, 65);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(138, 35);
            this.button9.TabIndex = 30;
            this.button9.Text = "  Stop animation";
            this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.xButton4_Click);
            // 
            // button4
            // 
            this.button4.Image = global::Preview.Properties.Resources.play;
            this.button4.Location = new System.Drawing.Point(177, 26);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(138, 35);
            this.button4.TabIndex = 29;
            this.button4.Text = "  Run animation";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.xButton3_Click);
            // 
            // xButtonFlash
            // 
            this.xButtonFlash.Location = new System.Drawing.Point(88, 100);
            this.xButtonFlash.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.xButtonFlash.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.xButtonFlash.Name = "xButtonFlash";
            this.xButtonFlash.Size = new System.Drawing.Size(63, 20);
            this.xButtonFlash.TabIndex = 28;
            this.xButtonFlash.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Flash color";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Flash count";
            // 
            // xButtonColor
            // 
            this.xButtonColor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.xButtonColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.xButtonColor.Location = new System.Drawing.Point(88, 26);
            this.xButtonColor.Name = "xButtonColor";
            this.xButtonColor.Size = new System.Drawing.Size(63, 14);
            this.xButtonColor.TabIndex = 21;
            this.xButtonColor.Click += new System.EventHandler(this.xButtonColor_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Color steps";
            // 
            // xButtonDuration
            // 
            this.xButtonDuration.Location = new System.Drawing.Point(88, 48);
            this.xButtonDuration.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.xButtonDuration.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.xButtonDuration.Name = "xButtonDuration";
            this.xButtonDuration.Size = new System.Drawing.Size(63, 20);
            this.xButtonDuration.TabIndex = 23;
            this.xButtonDuration.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Duration";
            // 
            // xButtonSteps
            // 
            this.xButtonSteps.Location = new System.Drawing.Point(88, 74);
            this.xButtonSteps.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.xButtonSteps.Name = "xButtonSteps";
            this.xButtonSteps.Size = new System.Drawing.Size(63, 20);
            this.xButtonSteps.TabIndex = 24;
            this.xButtonSteps.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // xButtonLoad
            // 
            this.xButtonLoad.Location = new System.Drawing.Point(25, 16);
            this.xButtonLoad.Name = "xButtonLoad";
            this.xButtonLoad.Size = new System.Drawing.Size(83, 40);
            this.xButtonLoad.TabIndex = 33;
            this.xButtonLoad.Text = "LOAD";
            this.xButtonLoad.UseVisualStyleBackColor = true;
            this.xButtonLoad.Click += new System.EventHandler(this.xButtonLoad_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(258, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Default";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(355, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Hover";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(465, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Active";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(573, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Disabled";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(157, 92);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "ForeColor";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(157, 117);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "FillColor";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(157, 141);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "BorderColor";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(157, 177);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "Image";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(157, 208);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "ImageColor";
            // 
            // foreDefault
            // 
            this.foreDefault.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.foreDefault.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.foreDefault.Location = new System.Drawing.Point(261, 90);
            this.foreDefault.Name = "foreDefault";
            this.foreDefault.Size = new System.Drawing.Size(32, 16);
            this.foreDefault.TabIndex = 22;
            this.foreDefault.Click += new System.EventHandler(this.foreDefault_Click);
            // 
            // fillDefault
            // 
            this.fillDefault.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.fillDefault.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fillDefault.Location = new System.Drawing.Point(261, 115);
            this.fillDefault.Name = "fillDefault";
            this.fillDefault.Size = new System.Drawing.Size(32, 16);
            this.fillDefault.TabIndex = 24;
            this.fillDefault.Click += new System.EventHandler(this.fillDefault_Click);
            // 
            // foreHover
            // 
            this.foreHover.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.foreHover.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.foreHover.Location = new System.Drawing.Point(358, 90);
            this.foreHover.Name = "foreHover";
            this.foreHover.Size = new System.Drawing.Size(32, 16);
            this.foreHover.TabIndex = 23;
            this.foreHover.Click += new System.EventHandler(this.foreHover_Click);
            // 
            // xButtonA
            // 
            this.xButtonA.FillDisabledColor = System.Drawing.Color.White;
            this.xButtonA.ImageCustomSize = new System.Drawing.Size(0, 0);
            this.xButtonA.Location = new System.Drawing.Point(777, 169);
            this.xButtonA.Name = "xButtonA";
            this.xButtonA.Size = new System.Drawing.Size(26, 25);
            this.xButtonA.TabIndex = 68;
            this.xButtonA.Text = " ";
            // 
            // fillHover
            // 
            this.fillHover.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.fillHover.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fillHover.Location = new System.Drawing.Point(358, 115);
            this.fillHover.Name = "fillHover";
            this.fillHover.Size = new System.Drawing.Size(32, 16);
            this.fillHover.TabIndex = 27;
            this.fillHover.Click += new System.EventHandler(this.fillHover_Click);
            // 
            // foreActive
            // 
            this.foreActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.foreActive.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.foreActive.Location = new System.Drawing.Point(468, 90);
            this.foreActive.Name = "foreActive";
            this.foreActive.Size = new System.Drawing.Size(32, 16);
            this.foreActive.TabIndex = 23;
            this.foreActive.Click += new System.EventHandler(this.foreActive_Click);
            // 
            // fillActive
            // 
            this.fillActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.fillActive.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fillActive.Location = new System.Drawing.Point(468, 115);
            this.fillActive.Name = "fillActive";
            this.fillActive.Size = new System.Drawing.Size(32, 16);
            this.fillActive.TabIndex = 26;
            this.fillActive.Click += new System.EventHandler(this.fillActive_Click);
            // 
            // borderDefault
            // 
            this.borderDefault.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.borderDefault.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.borderDefault.Location = new System.Drawing.Point(261, 141);
            this.borderDefault.Name = "borderDefault";
            this.borderDefault.Size = new System.Drawing.Size(32, 16);
            this.borderDefault.TabIndex = 24;
            this.borderDefault.Click += new System.EventHandler(this.borderDefault_Click);
            // 
            // foreDisabled
            // 
            this.foreDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.foreDisabled.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.foreDisabled.Location = new System.Drawing.Point(576, 90);
            this.foreDisabled.Name = "foreDisabled";
            this.foreDisabled.Size = new System.Drawing.Size(32, 16);
            this.foreDisabled.TabIndex = 23;
            this.foreDisabled.Click += new System.EventHandler(this.foreDisabled_Click);
            // 
            // borderHover
            // 
            this.borderHover.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.borderHover.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.borderHover.Location = new System.Drawing.Point(358, 141);
            this.borderHover.Name = "borderHover";
            this.borderHover.Size = new System.Drawing.Size(32, 16);
            this.borderHover.TabIndex = 27;
            this.borderHover.Click += new System.EventHandler(this.borderHover_Click);
            // 
            // borderActive
            // 
            this.borderActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.borderActive.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.borderActive.Location = new System.Drawing.Point(468, 141);
            this.borderActive.Name = "borderActive";
            this.borderActive.Size = new System.Drawing.Size(32, 16);
            this.borderActive.TabIndex = 26;
            this.borderActive.Click += new System.EventHandler(this.borderActive_Click);
            // 
            // imageColorDefault
            // 
            this.imageColorDefault.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.imageColorDefault.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageColorDefault.Location = new System.Drawing.Point(261, 208);
            this.imageColorDefault.Name = "imageColorDefault";
            this.imageColorDefault.Size = new System.Drawing.Size(32, 16);
            this.imageColorDefault.TabIndex = 24;
            this.imageColorDefault.Click += new System.EventHandler(this.imageColorDefault_Click);
            // 
            // imageColorHover
            // 
            this.imageColorHover.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.imageColorHover.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageColorHover.Location = new System.Drawing.Point(358, 208);
            this.imageColorHover.Name = "imageColorHover";
            this.imageColorHover.Size = new System.Drawing.Size(32, 16);
            this.imageColorHover.TabIndex = 27;
            this.imageColorHover.Click += new System.EventHandler(this.imageColorHover_Click);
            // 
            // fillDisabled
            // 
            this.fillDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.fillDisabled.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fillDisabled.Location = new System.Drawing.Point(576, 115);
            this.fillDisabled.Name = "fillDisabled";
            this.fillDisabled.Size = new System.Drawing.Size(32, 16);
            this.fillDisabled.TabIndex = 25;
            this.fillDisabled.Click += new System.EventHandler(this.fillDisabled_Click);
            // 
            // imageColorActive
            // 
            this.imageColorActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.imageColorActive.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageColorActive.Location = new System.Drawing.Point(468, 208);
            this.imageColorActive.Name = "imageColorActive";
            this.imageColorActive.Size = new System.Drawing.Size(32, 16);
            this.imageColorActive.TabIndex = 26;
            this.imageColorActive.Click += new System.EventHandler(this.imageColorActive_Click);
            // 
            // borderDisabled
            // 
            this.borderDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.borderDisabled.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.borderDisabled.Location = new System.Drawing.Point(576, 141);
            this.borderDisabled.Name = "borderDisabled";
            this.borderDisabled.Size = new System.Drawing.Size(32, 16);
            this.borderDisabled.TabIndex = 25;
            this.borderDisabled.Click += new System.EventHandler(this.borderDisabled_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.custom);
            this.panel5.Controls.Add(this.original);
            this.panel5.Controls.Add(this.fontSize);
            this.panel5.Location = new System.Drawing.Point(160, 331);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(448, 22);
            this.panel5.TabIndex = 56;
            // 
            // custom
            // 
            this.custom.AutoSize = true;
            this.custom.Enabled = false;
            this.custom.Location = new System.Drawing.Point(151, 3);
            this.custom.Name = "custom";
            this.custom.Size = new System.Drawing.Size(60, 17);
            this.custom.TabIndex = 53;
            this.custom.TabStop = true;
            this.custom.Text = "Custom";
            this.custom.UseVisualStyleBackColor = true;
            // 
            // original
            // 
            this.original.AutoSize = true;
            this.original.Location = new System.Drawing.Point(80, 3);
            this.original.Name = "original";
            this.original.Size = new System.Drawing.Size(60, 17);
            this.original.TabIndex = 52;
            this.original.TabStop = true;
            this.original.Text = "Original";
            this.original.UseVisualStyleBackColor = true;
            this.original.CheckedChanged += new System.EventHandler(this.original_CheckedChanged);
            // 
            // fontSize
            // 
            this.fontSize.AutoSize = true;
            this.fontSize.Location = new System.Drawing.Point(3, 3);
            this.fontSize.Name = "fontSize";
            this.fontSize.Size = new System.Drawing.Size(66, 17);
            this.fontSize.TabIndex = 51;
            this.fontSize.TabStop = true;
            this.fontSize.Text = "FontSize";
            this.fontSize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fontSize.UseVisualStyleBackColor = true;
            this.fontSize.CheckedChanged += new System.EventHandler(this.fontSize_CheckedChanged);
            // 
            // imageColorDisabled
            // 
            this.imageColorDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(174)))), ((int)(((byte)(0)))));
            this.imageColorDisabled.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageColorDisabled.Location = new System.Drawing.Point(576, 208);
            this.imageColorDisabled.Name = "imageColorDisabled";
            this.imageColorDisabled.Size = new System.Drawing.Size(32, 16);
            this.imageColorDisabled.TabIndex = 25;
            this.imageColorDisabled.Click += new System.EventHandler(this.imageColorDisabled_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.textFront);
            this.panel4.Controls.Add(this.imageFront);
            this.panel4.Controls.Add(this.textFirst);
            this.panel4.Controls.Add(this.imageFirst);
            this.panel4.Location = new System.Drawing.Point(160, 305);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(448, 22);
            this.panel4.TabIndex = 55;
            // 
            // textFront
            // 
            this.textFront.AutoSize = true;
            this.textFront.Location = new System.Drawing.Point(235, 3);
            this.textFront.Name = "textFront";
            this.textFront.Size = new System.Drawing.Size(70, 17);
            this.textFront.TabIndex = 54;
            this.textFront.TabStop = true;
            this.textFront.Text = "TextFront";
            this.textFront.UseVisualStyleBackColor = true;
            this.textFront.CheckedChanged += new System.EventHandler(this.textFront_CheckedChanged);
            // 
            // imageFront
            // 
            this.imageFront.AutoSize = true;
            this.imageFront.Location = new System.Drawing.Point(151, 3);
            this.imageFront.Name = "imageFront";
            this.imageFront.Size = new System.Drawing.Size(78, 17);
            this.imageFront.TabIndex = 53;
            this.imageFront.TabStop = true;
            this.imageFront.Text = "ImageFront";
            this.imageFront.UseVisualStyleBackColor = true;
            this.imageFront.CheckedChanged += new System.EventHandler(this.imageFront_CheckedChanged);
            // 
            // textFirst
            // 
            this.textFirst.AutoSize = true;
            this.textFirst.Location = new System.Drawing.Point(80, 3);
            this.textFirst.Name = "textFirst";
            this.textFirst.Size = new System.Drawing.Size(65, 17);
            this.textFirst.TabIndex = 52;
            this.textFirst.TabStop = true;
            this.textFirst.Text = "TextFirst";
            this.textFirst.UseVisualStyleBackColor = true;
            this.textFirst.CheckedChanged += new System.EventHandler(this.textFirst_CheckedChanged);
            // 
            // imageFirst
            // 
            this.imageFirst.AutoSize = true;
            this.imageFirst.Location = new System.Drawing.Point(3, 3);
            this.imageFirst.Name = "imageFirst";
            this.imageFirst.Size = new System.Drawing.Size(73, 17);
            this.imageFirst.TabIndex = 51;
            this.imageFirst.TabStop = true;
            this.imageFirst.Text = "ImageFirst";
            this.imageFirst.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.imageFirst.UseVisualStyleBackColor = true;
            this.imageFirst.CheckedChanged += new System.EventHandler(this.imageFirst_CheckedChanged);
            // 
            // imageDefault
            // 
            this.imageDefault.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageDefault.Location = new System.Drawing.Point(261, 166);
            this.imageDefault.Name = "imageDefault";
            this.imageDefault.Size = new System.Drawing.Size(32, 32);
            this.imageDefault.TabIndex = 28;
            this.imageDefault.TabStop = false;
            this.imageDefault.Click += new System.EventHandler(this.imageDefault_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.imageRight);
            this.panel3.Controls.Add(this.imageCenter);
            this.panel3.Controls.Add(this.imageLeft);
            this.panel3.Location = new System.Drawing.Point(160, 279);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(448, 22);
            this.panel3.TabIndex = 54;
            // 
            // imageRight
            // 
            this.imageRight.AutoSize = true;
            this.imageRight.Location = new System.Drawing.Point(114, 3);
            this.imageRight.Name = "imageRight";
            this.imageRight.Size = new System.Drawing.Size(50, 17);
            this.imageRight.TabIndex = 53;
            this.imageRight.TabStop = true;
            this.imageRight.Text = "Right";
            this.imageRight.UseVisualStyleBackColor = true;
            this.imageRight.CheckedChanged += new System.EventHandler(this.imageRight_CheckedChanged);
            // 
            // imageCenter
            // 
            this.imageCenter.AutoSize = true;
            this.imageCenter.Location = new System.Drawing.Point(52, 3);
            this.imageCenter.Name = "imageCenter";
            this.imageCenter.Size = new System.Drawing.Size(56, 17);
            this.imageCenter.TabIndex = 52;
            this.imageCenter.TabStop = true;
            this.imageCenter.Text = "Center";
            this.imageCenter.UseVisualStyleBackColor = true;
            this.imageCenter.CheckedChanged += new System.EventHandler(this.imageCenter_CheckedChanged);
            // 
            // imageLeft
            // 
            this.imageLeft.AutoSize = true;
            this.imageLeft.Location = new System.Drawing.Point(3, 3);
            this.imageLeft.Name = "imageLeft";
            this.imageLeft.Size = new System.Drawing.Size(43, 17);
            this.imageLeft.TabIndex = 51;
            this.imageLeft.TabStop = true;
            this.imageLeft.Text = "Left";
            this.imageLeft.UseVisualStyleBackColor = true;
            this.imageLeft.CheckedChanged += new System.EventHandler(this.imageLeft_CheckedChanged);
            // 
            // imageHover
            // 
            this.imageHover.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageHover.Location = new System.Drawing.Point(358, 166);
            this.imageHover.Name = "imageHover";
            this.imageHover.Size = new System.Drawing.Size(32, 32);
            this.imageHover.TabIndex = 29;
            this.imageHover.TabStop = false;
            this.imageHover.Click += new System.EventHandler(this.imageHover_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textRight);
            this.panel1.Controls.Add(this.textCenter);
            this.panel1.Controls.Add(this.textLeft);
            this.panel1.Location = new System.Drawing.Point(160, 253);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(448, 22);
            this.panel1.TabIndex = 52;
            // 
            // textRight
            // 
            this.textRight.AutoSize = true;
            this.textRight.Location = new System.Drawing.Point(114, 3);
            this.textRight.Name = "textRight";
            this.textRight.Size = new System.Drawing.Size(50, 17);
            this.textRight.TabIndex = 53;
            this.textRight.TabStop = true;
            this.textRight.Text = "Right";
            this.textRight.UseVisualStyleBackColor = true;
            this.textRight.CheckedChanged += new System.EventHandler(this.textRight_CheckedChanged);
            // 
            // textCenter
            // 
            this.textCenter.AutoSize = true;
            this.textCenter.Location = new System.Drawing.Point(52, 3);
            this.textCenter.Name = "textCenter";
            this.textCenter.Size = new System.Drawing.Size(56, 17);
            this.textCenter.TabIndex = 52;
            this.textCenter.TabStop = true;
            this.textCenter.Text = "Center";
            this.textCenter.UseVisualStyleBackColor = true;
            this.textCenter.CheckedChanged += new System.EventHandler(this.textCenter_CheckedChanged);
            // 
            // textLeft
            // 
            this.textLeft.AutoSize = true;
            this.textLeft.Location = new System.Drawing.Point(3, 3);
            this.textLeft.Name = "textLeft";
            this.textLeft.Size = new System.Drawing.Size(43, 17);
            this.textLeft.TabIndex = 51;
            this.textLeft.TabStop = true;
            this.textLeft.Text = "Left";
            this.textLeft.UseVisualStyleBackColor = true;
            this.textLeft.CheckedChanged += new System.EventHandler(this.textLeft_CheckedChanged);
            // 
            // imageActive
            // 
            this.imageActive.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageActive.Location = new System.Drawing.Point(468, 166);
            this.imageActive.Name = "imageActive";
            this.imageActive.Size = new System.Drawing.Size(32, 32);
            this.imageActive.TabIndex = 30;
            this.imageActive.TabStop = false;
            this.imageActive.Click += new System.EventHandler(this.imageActive_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(36, 337);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 37;
            this.label17.Text = "ImageSize";
            // 
            // imageDisabled
            // 
            this.imageDisabled.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageDisabled.Location = new System.Drawing.Point(576, 166);
            this.imageDisabled.Name = "imageDisabled";
            this.imageDisabled.Size = new System.Drawing.Size(32, 32);
            this.imageDisabled.TabIndex = 31;
            this.imageDisabled.TabStop = false;
            this.imageDisabled.Click += new System.EventHandler(this.imageDisabled_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(35, 311);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 13);
            this.label16.TabIndex = 36;
            this.label16.Text = "ImageTextRelation";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(41, 367);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(126, 23);
            this.button7.TabIndex = 4;
            this.button7.Text = "Toggle border shadows";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(36, 284);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "ImageAlign";
            // 
            // xButton
            // 
            this.xButton.BorderDisabledColor = System.Drawing.Color.MediumOrchid;
            this.xButton.FillDisabledColor = System.Drawing.Color.RosyBrown;
            this.xButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.xButton.ForeDisabledColor = System.Drawing.Color.OrangeRed;
            this.xButton.Image = ((System.Drawing.Image)(resources.GetObject("xButton.Image")));
            this.xButton.ImageActive = ((System.Drawing.Image)(resources.GetObject("xButton.ImageActive")));
            this.xButton.ImageActiveColor = System.Drawing.Color.Fuchsia;
            this.xButton.ImageColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.xButton.ImageCustomSize = new System.Drawing.Size(0, 0);
            this.xButton.ImageDisabled = global::Preview.Properties.Resources.locked;
            this.xButton.ImageDisabledColor = System.Drawing.Color.LimeGreen;
            this.xButton.ImageHover = ((System.Drawing.Image)(resources.GetObject("xButton.ImageHover")));
            this.xButton.ImageHoverColor = System.Drawing.Color.Yellow;
            this.xButton.Location = new System.Drawing.Point(261, 16);
            this.xButton.Name = "xButton";
            this.xButton.Size = new System.Drawing.Size(360, 40);
            this.xButton.TabIndex = 32;
            this.xButton.Text = "This is sample button";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(35, 259);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 13);
            this.label14.TabIndex = 34;
            this.label14.Text = "TextAlign";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(431, 367);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 3;
            this.button6.Text = "SetValid";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(173, 367);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(126, 23);
            this.button8.TabIndex = 6;
            this.button8.Text = "Toggle enable state";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(350, 367);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 2;
            this.button5.Text = "Set invalid";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // xToolTipTP
            // 
            this.xToolTipTP.Controls.Add(this.xToolTipListBox);
            this.xToolTipTP.Controls.Add(this.questionMark);
            this.xToolTipTP.Location = new System.Drawing.Point(4, 22);
            this.xToolTipTP.Name = "xToolTipTP";
            this.xToolTipTP.Padding = new System.Windows.Forms.Padding(3);
            this.xToolTipTP.Size = new System.Drawing.Size(1201, 630);
            this.xToolTipTP.TabIndex = 3;
            this.xToolTipTP.Text = "xToolTip";
            this.xToolTipTP.UseVisualStyleBackColor = true;
            // 
            // xToolTipListBox
            // 
            this.xToolTipListBox.FormattingEnabled = true;
            this.xToolTipListBox.HorizontalScrollbar = true;
            this.xToolTipListBox.Location = new System.Drawing.Point(6, 6);
            this.xToolTipListBox.Name = "xToolTipListBox";
            this.xToolTipListBox.ScrollAlwaysVisible = true;
            this.xToolTipListBox.Size = new System.Drawing.Size(613, 615);
            this.xToolTipListBox.TabIndex = 10;
            // 
            // questionMark
            // 
            this.questionMark.Image = ((System.Drawing.Image)(resources.GetObject("questionMark.Image")));
            this.questionMark.Location = new System.Drawing.Point(684, 44);
            this.questionMark.Name = "questionMark";
            this.questionMark.Size = new System.Drawing.Size(33, 53);
            this.questionMark.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.questionMark.TabIndex = 8;
            this.questionMark.TabStop = false;
            // 
            // SysInfoTP
            // 
            this.SysInfoTP.Controls.Add(this.textBox);
            this.SysInfoTP.Controls.Add(this.listBox);
            this.SysInfoTP.Location = new System.Drawing.Point(4, 22);
            this.SysInfoTP.Name = "SysInfoTP";
            this.SysInfoTP.Padding = new System.Windows.Forms.Padding(3);
            this.SysInfoTP.Size = new System.Drawing.Size(1201, 630);
            this.SysInfoTP.TabIndex = 1;
            this.SysInfoTP.Text = "System info";
            this.SysInfoTP.UseVisualStyleBackColor = true;
            // 
            // textBox
            // 
            this.textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox.Location = new System.Drawing.Point(233, 3);
            this.textBox.Multiline = true;
            this.textBox.Name = "textBox";
            this.textBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox.Size = new System.Drawing.Size(426, 460);
            this.textBox.TabIndex = 1;
            // 
            // listBox
            // 
            this.listBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBox.FormattingEnabled = true;
            this.listBox.Location = new System.Drawing.Point(3, 3);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(224, 459);
            this.listBox.TabIndex = 0;
            this.listBox.SelectedIndexChanged += new System.EventHandler(this.listBox_SelectedIndexChanged);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // xButton1
            // 
            this.xButton1.BorderColor = System.Drawing.Color.Black;
            this.xButton1.Enabled = false;
            this.xButton1.FillColor = System.Drawing.Color.DimGray;
            this.xButton1.FillDisabledColor = System.Drawing.Color.White;
            this.xButton1.ForeColor = System.Drawing.Color.Gainsboro;
            this.xButton1.ImageColor = System.Drawing.Color.White;
            this.xButton1.ImageCustomSize = new System.Drawing.Size(0, 0);
            this.xButton1.Location = new System.Drawing.Point(317, 243);
            this.xButton1.Name = "xButton1";
            this.xButton1.Size = new System.Drawing.Size(200, 25);
            this.xButton1.TabIndex = 23;
            // 
            // xButton11
            // 
            this.xButton11.Enabled = false;
            this.xButton11.FillDisabledColor = System.Drawing.Color.White;
            this.xButton11.Image = ((System.Drawing.Image)(resources.GetObject("xButton11.Image")));
            this.xButton11.ImageActive = ((System.Drawing.Image)(resources.GetObject("xButton11.ImageActive")));
            this.xButton11.ImageColor = System.Drawing.Color.Sienna;
            this.xButton11.ImageCustomSize = new System.Drawing.Size(24, 24);
            this.xButton11.ImageDisabled = ((System.Drawing.Image)(resources.GetObject("xButton11.ImageDisabled")));
            this.xButton11.ImageHover = ((System.Drawing.Image)(resources.GetObject("xButton11.ImageHover")));
            this.xButton11.ImageSize = ImageSize.Custom;
            this.xButton11.Location = new System.Drawing.Point(14, 294);
            this.xButton11.Name = "xButton11";
            this.xButton11.Size = new System.Drawing.Size(130, 60);
            this.xButton11.TabIndex = 16;
            // 
            // xButton9
            // 
            this.xButton9.BorderRadius = 30;
            this.xButton9.Enabled = false;
            this.xButton9.FillDisabledColor = System.Drawing.Color.White;
            this.xButton9.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold);
            this.xButton9.Image = ((System.Drawing.Image)(resources.GetObject("xButton9.Image")));
            this.xButton9.ImageActive = ((System.Drawing.Image)(resources.GetObject("xButton9.ImageActive")));
            this.xButton9.ImageAlignment = ObjectAlignment.Right;
            this.xButton9.ImageColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.xButton9.ImageCustomSize = new System.Drawing.Size(73, 73);
            this.xButton9.ImageDisabled = ((System.Drawing.Image)(resources.GetObject("xButton9.ImageDisabled")));
            this.xButton9.ImageHover = ((System.Drawing.Image)(resources.GetObject("xButton9.ImageHover")));
            this.xButton9.ImageSize = ImageSize.Original;
            this.xButton9.Location = new System.Drawing.Point(431, 278);
            this.xButton9.Name = "xButton9";
            this.xButton9.Size = new System.Drawing.Size(218, 76);
            this.xButton9.TabIndex = 15;
            this.xButton9.TextAlignment = ObjectAlignment.Left;
            // 
            // xButton6
            // 
            this.xButton6.BorderRadius = 20;
            this.xButton6.BorderThickness = 3;
            this.xButton6.DisplayOrder = ObjectDisplayOrder.TextFirst;
            this.xButton6.Enabled = false;
            this.xButton6.FillDisabledColor = System.Drawing.Color.White;
            this.xButton6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.xButton6.Image = ((System.Drawing.Image)(resources.GetObject("xButton6.Image")));
            this.xButton6.ImageActive = ((System.Drawing.Image)(resources.GetObject("xButton6.ImageActive")));
            this.xButton6.ImageColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.xButton6.ImageCustomSize = new System.Drawing.Size(0, 0);
            this.xButton6.ImageDisabled = ((System.Drawing.Image)(resources.GetObject("xButton6.ImageDisabled")));
            this.xButton6.ImageHover = ((System.Drawing.Image)(resources.GetObject("xButton6.ImageHover")));
            this.xButton6.Location = new System.Drawing.Point(150, 294);
            this.xButton6.Name = "xButton6";
            this.xButton6.Size = new System.Drawing.Size(261, 60);
            this.xButton6.TabIndex = 10;
            // 
            // xToolTip
            // 
            this.xToolTip.FillColor = System.Drawing.Color.Gray;
            this.xToolTip.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.xToolTip.ForeColor = System.Drawing.SystemColors.Info;
            this.xToolTip.OwnerDraw = true;
            this.xToolTip.Padding = new System.Windows.Forms.Padding(20);
            this.xToolTip.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.xToolTip.TitleForeColor = System.Drawing.Color.Navy;
            this.xToolTip.TitleTextSpace = 10;
            this.xToolTip.ToolTipTitle = "xdd";
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(0, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(348, 241);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(0, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(200, 75);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(366, 228);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 34);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(192, 62);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(0, 25);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(359, 292);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "tabPage5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(0, 25);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(200, 75);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "tabPage6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Location = new System.Drawing.Point(4, 34);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(350, 279);
            this.tabPage7.TabIndex = 0;
            this.tabPage7.Text = "tabPage7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // tabPage8
            // 
            this.tabPage8.Location = new System.Drawing.Point(4, 34);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(192, 62);
            this.tabPage8.TabIndex = 1;
            this.tabPage8.Text = "tabPage8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // xButton2
            // 
            this.xButton2.Location = new System.Drawing.Point(568, 253);
            this.xButton2.Name = "xButton2";
            this.xButton2.Size = new System.Drawing.Size(200, 25);
            this.xButton2.TabIndex = 9;
            this.xButton2.Text = "xButton2";
            // 
            // Preview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1209, 694);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.titlebar);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "Preview";
            this.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(131)))), ((int)(((byte)(215)))));
            this.ShadowPattern = ShadowPatternType.Custom;
            this.ShadowPatternImage = ((System.Drawing.Bitmap)(resources.GetObject("$this.ShadowPatternImage")));
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.titlebar.ResumeLayout(false);
            this.titlebar.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.formTP.ResumeLayout(false);
            this.xButtonTP.ResumeLayout(false);
            this.xButtonTP.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xButtonFlash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xButtonDuration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xButtonSteps)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageDefault)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageHover)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageDisabled)).EndInit();
            this.xToolTipTP.ResumeLayout(false);
            this.xToolTipTP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.questionMark)).EndInit();
            this.SysInfoTP.ResumeLayout(false);
            this.SysInfoTP.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel titlebar;
        private System.Windows.Forms.Button minimize;
        private System.Windows.Forms.Button maximize;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.Label title;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage xButtonTP;
        private System.Windows.Forms.TabPage SysInfoTP;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.ListBox listBox;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button8;
        private xButton xButton9;
        private xButton xButton6;
        private xButton xButton11;
        private xButton xButton1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox imageDisabled;
        private System.Windows.Forms.PictureBox imageActive;
        private System.Windows.Forms.PictureBox imageHover;
        private System.Windows.Forms.PictureBox imageDefault;
        private System.Windows.Forms.Panel imageColorDisabled;
        private System.Windows.Forms.Panel borderDisabled;
        private System.Windows.Forms.Panel imageColorActive;
        private System.Windows.Forms.Panel fillDisabled;
        private System.Windows.Forms.Panel imageColorHover;
        private System.Windows.Forms.Panel imageColorDefault;
        private System.Windows.Forms.Panel borderActive;
        private System.Windows.Forms.Panel borderHover;
        private System.Windows.Forms.Panel foreDisabled;
        private System.Windows.Forms.Panel borderDefault;
        private System.Windows.Forms.Panel fillActive;
        private System.Windows.Forms.Panel foreActive;
        private System.Windows.Forms.Panel fillHover;
        private System.Windows.Forms.Panel foreHover;
        private System.Windows.Forms.Panel fillDefault;
        private System.Windows.Forms.Panel foreDefault;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private xButton xButton;
        private System.Windows.Forms.Button xButtonLoad;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton textCenter;
        private System.Windows.Forms.RadioButton textLeft;
        private System.Windows.Forms.RadioButton textRight;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton textFront;
        private System.Windows.Forms.RadioButton imageFront;
        private System.Windows.Forms.RadioButton textFirst;
        private System.Windows.Forms.RadioButton imageFirst;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton imageRight;
        private System.Windows.Forms.RadioButton imageCenter;
        private System.Windows.Forms.RadioButton imageLeft;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton custom;
        private System.Windows.Forms.RadioButton original;
        private System.Windows.Forms.RadioButton fontSize;
        private System.Windows.Forms.NumericUpDown xButtonFlash;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown xButtonSteps;
        private System.Windows.Forms.NumericUpDown xButtonDuration;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel xButtonColor;
        private System.Windows.Forms.Button button7;
        private xButton xButtonA;
        private System.Windows.Forms.GroupBox groupBox1;
        private xButton xButtonH;
        private xButton xButtonG;
        private xButton xButtonF;
        private xButton xButtonE;
        private xButton xButtonD;
        private xButton xButtonC;
        private xButton xButtonB;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button4;
        private xButton xButtonJ;
        private xButton xButtonI;
        private System.Windows.Forms.PictureBox questionMark;
        private xToolTip xToolTip;
        private System.Windows.Forms.TabPage xToolTipTP;
        private System.Windows.Forms.ListBox xToolTipListBox;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage formTP;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button shadowColorButton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button customButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button noneButton;
        private System.Windows.Forms.Button realButton;
        private System.Windows.Forms.Button aroundButton;
        private xButton xButton2;
    }
}
