﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Preview
{
    partial class Preview : PureForm
    {
        public Preview()
        {
            InitializeComponent();

            Type t = typeof(System.Windows.Forms.SystemInformation);
            PropertyInfo[] pi = t.GetProperties();

            for (int i = 0; i < pi.Length; i++)
            {
                listBox.Items.Add(pi[i].Name);
            }

            textBox.Text = "The SystemInformation class has " + pi.Length.ToString() + " properties.\r\n";


            // xToolTip
            xToolTip.SetToolTip(questionMark, "Xddasasdnhgaslkashgljasg\nsagfhasghaslgfhsahjkgfsa", "Tajtle");
            ListPropertiesFieldsAndMethods(xToolTip, xToolTipListBox);
        }

        public static void ListPropertiesFieldsAndMethods(object obj, ListBox listBox)
        {
            Type type = obj.GetType();

            // List properties
            PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo property in properties)
            {
                string description = GetPropertyDescription(property);
                listBox.Items.Add($"Property: {property.Name} ({property.PropertyType.Name}): {description}");
            }

            // List fields
            FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
            foreach (FieldInfo field in fields)
            {
                listBox.Items.Add($"Field: {field.Name} ({field.FieldType.Name})");
            }

            // List methods
            MethodInfo[] methods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            foreach (MethodInfo method in methods)
            {
                if (!method.IsSpecialName) // Ignore property accessors and other special methods
                {
                    listBox.Items.Add($"Method: {method.Name} (Return type: {method.ReturnType.Name})");
                }
            }
        }

        private static string GetPropertyDescription(PropertyInfo property)
        {
            DescriptionAttribute descriptionAttribute = property.GetCustomAttribute<DescriptionAttribute>();
            return descriptionAttribute?.Description ?? "No description available";
        }



        private void title_Click(object sender, EventArgs e)
        {
            Console.Clear();
        }

        private const int DoubleClickTimeThreshold = 300; // Adjust threshold as needed (in milliseconds)
        private DateTime lastClickTime = DateTime.MinValue;

        private void titlebar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                TimeSpan timeSinceLastClick = DateTime.Now - lastClickTime;

                if (timeSinceLastClick.TotalMilliseconds <= DoubleClickTimeThreshold)
                    maximize_Click(null, null);

                lastClickTime = DateTime.Now;


                DragWindow();
            }

        }
        private void exit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
        private void maximize_Click(object sender, EventArgs e)
        {
            this.WindowState = (this.WindowState == FormWindowState.Normal ? FormWindowState.Maximized : FormWindowState.Normal);
        }
        private void minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void titlebar_Click(object sender, EventArgs e)
        {
            Console.WriteLine("TEst");
        }




        private bool _animationRunning = false;
        private Color _shadowColor = Color.Empty;

        private void startAnimationButton_Click(object sender, EventArgs e)
        {
            if (_animationRunning)
                return;

            _animationRunning = true;

            FlashWindow(true, true, 3);

            Task.Run(async () =>
            {
                _shadowColor = ShadowColor;

                while (_animationRunning)
                {
                    this.ShadowColor = ColorTranslator.FromHtml("#ffae00");
                    await Task.Delay(20);
                    this.ShadowColor = ColorTranslator.FromHtml("#ffba1a");
                    await Task.Delay(15);
                    this.ShadowColor = ColorTranslator.FromHtml("#FFC534");
                    await Task.Delay(10);
                    this.ShadowColor = ColorTranslator.FromHtml("#FFD04F");
                    await Task.Delay(5);
                    this.ShadowColor = ColorTranslator.FromHtml("#FFDB69");
                    await Task.Delay(5);
                    this.ShadowColor = ColorTranslator.FromHtml("#FFE683");
                    await Task.Delay(5);
                    this.ShadowColor = ColorTranslator.FromHtml("#FFF19D");
                    await Task.Delay(5);
                    this.ShadowColor = ColorTranslator.FromHtml("#FFFCB7");
                    await Task.Delay(5);
                    this.ShadowColor = ColorTranslator.FromHtml("#FFFFD2");
                    await Task.Delay(5);
                    this.ShadowColor = ColorTranslator.FromHtml("#FFFFFF");
                    await Task.Delay(5);

                    this.ShadowColor = ColorTranslator.FromHtml("#FFFFD2");
                    await Task.Delay(5);
                    this.ShadowColor = ColorTranslator.FromHtml("#FFFCB7");
                    await Task.Delay(5);
                    this.ShadowColor = ColorTranslator.FromHtml("#FFF19D");
                    await Task.Delay(5);
                    this.ShadowColor = ColorTranslator.FromHtml("#FFE683");
                    await Task.Delay(5);
                    this.ShadowColor = ColorTranslator.FromHtml("#FFDB69");
                    await Task.Delay(5);
                    this.ShadowColor = ColorTranslator.FromHtml("#FFD04F");
                    await Task.Delay(5);
                    this.ShadowColor = ColorTranslator.FromHtml("#FFC534");
                    await Task.Delay(10);
                    this.ShadowColor = ColorTranslator.FromHtml("#ffba1a");
                    await Task.Delay(15);
                }
            });
        }

        private void stopAnimationButton_Click(object sender, EventArgs e)
        {
            _animationRunning = false;

            Task.Run(async () =>
            {
                await Task.Delay(100);
                ShadowColor = _shadowColor;
            });
        }


        private void noneButton_Click(object sender, EventArgs e)
        {
            this.ShadowPattern = ShadowPatternType.None;
        }

        private void aroundButton_Click(object sender, EventArgs e)
        {
            this.ShadowPattern = ShadowPatternType.Around;
        }

        private void realButton_Click(object sender, EventArgs e)
        {
            this.ShadowPattern = ShadowPatternType.Real;
        }

        private void customButton_Click(object sender, EventArgs e)
        {
            this.ShadowPattern = ShadowPatternType.Custom;
        }

        private void shadowColorButton_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                this.ShadowColor = colorDialog.Color;
        }

        private void borderColorButton_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                this.BorderColor = colorDialog.Color;
        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Return if no list item is selected.
            if (listBox.SelectedIndex == -1) return;

            // Get the property name from the list item.
            string propname = listBox.Text;

            if (propname == "PowerStatus")
            {
                // Cycle and display the values of each property of the PowerStatus property.
                textBox.Text += "\r\nThe value of the PowerStatus property is:";
                Type t = typeof(System.Windows.Forms.PowerStatus);
                PropertyInfo[] pi = t.GetProperties();
                for (int i = 0; i < pi.Length; i++)
                {
                    object propval = pi[i].GetValue(SystemInformation.PowerStatus, null);
                    textBox.Text += "\r\n    PowerStatus." + pi[i].Name + " is: " + propval.ToString();
                }
            }
            else
            {
                // Display the value of the selected property of the SystemInformation type.
                Type t = typeof(System.Windows.Forms.SystemInformation);
                PropertyInfo[] pi = t.GetProperties();
                PropertyInfo prop = null;

                for (int i = 0; i < pi.Length; i++)
                {
                    if (pi[i].Name == propname)
                    {
                        prop = pi[i];
                        break;
                    }
                }
                object propval = prop.GetValue(null, null);

                textBox.AppendText("\r\nThe value of the " + propname + " property is: " + propval.ToString());
            }
        }













        private void button4_Click(object sender, EventArgs e)
        {
            if (xButton.TextAlignment == ObjectAlignment.Left)
            {
                xButton.TextAlignment = ObjectAlignment.Center;
                xButton.ImageAlignment = ObjectAlignment.Left;
            }
            else if (xButton.TextAlignment == ObjectAlignment.Center)
            {
                xButton.TextAlignment = ObjectAlignment.Right;
                xButton.ImageAlignment = ObjectAlignment.Center;
            }
            else if (xButton.TextAlignment == ObjectAlignment.Right)
            {
                xButton.TextAlignment = ObjectAlignment.Left;
                xButton.ImageAlignment = ObjectAlignment.Right;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            xButton.SetTemporaryForeColor(Color.Red);
            xButton.SetTemporaryFillColor(Color.MistyRose);
            xButton.SetTemporaryBorderColor(Color.Red);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            xButton.SetTemporaryForeColor(Color.Green);
            xButton.SetTemporaryFillColor(Color.LightGreen);
            xButton.SetTemporaryBorderColor(Color.Green);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            xButton.BorderShadows = !xButton.BorderShadows;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            xButton.Enabled = !xButton.Enabled;
        }

        private void xButton3_Click(object sender, EventArgs e)
        {
            //Console.WriteLine(this.xButtonColor.BackColor);

            int duration = (int)xButtonDuration.Value;
            int colorSteps = (int)xButtonSteps.Value;
            int flashCount = (int)xButtonFlash.Value;

            Console.WriteLine($"Color: {xButtonColor.BackColor}, duration: {duration}ms, colorSteps: {colorSteps}, flashCount: {flashCount}");

            xButton.StartFlash(xButtonColor.BackColor, duration, colorSteps, flashCount);

            xButton[] buttons = new xButton[] { xButtonA, xButtonB, xButtonC, xButtonD, xButtonE, xButtonF, xButtonG, xButtonH, xButtonI, xButtonJ };
            Task.Run(async () =>
            {
                foreach (xButton button in buttons)
                {
                    button.StartFlash(xButtonColor.BackColor, duration, colorSteps, flashCount);
                    await Task.Delay(duration / colorSteps);
                }
            });
        }
        private void xButton4_Click(object sender, EventArgs e)
        {
            xButton.StopFlash();
            xButtonA.StopFlash(); 
            xButtonB.StopFlash(); 
            xButtonC.StopFlash(); 
            xButtonD.StopFlash(); 
            xButtonE.StopFlash(); 
            xButtonF.StopFlash(); 
            xButtonG.StopFlash();
            xButtonH.StopFlash(); 
            xButtonI.StopFlash(); 
            xButtonJ.StopFlash();
        }
        private void xButtonColor_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                this.xButtonColor.BackColor = colorDialog.Color;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            // Set properties for the file picker dialog
            openFileDialog1.Title = "Choose Image";
            openFileDialog1.Filter = "Image Files (*.png; *.jpg; *.jpeg; *.gif; *.bmp)|*.png; *.jpg; *.jpeg; *.gif; *.bmp";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            // Show the file picker dialog and get the result
            DialogResult result = openFileDialog1.ShowDialog();

            // Check if the user selected a file
            if (result == DialogResult.OK)
            {
                try
                {
                    // Get the path of the selected file
                    string selectedFile = openFileDialog1.FileName;

                    // Load the image from the selected file
                    Bitmap newImage = (Bitmap)Image.FromFile(selectedFile);

                    // Assign the loaded image to the Image property of your custom control
                    xButton.Image = newImage;

                    // Optionally, you might want to resize the image to fit your custom control
                    // xButton.Image = ResizeImage(newImage, xButton.Width, xButton.Height);
                }
                catch (Exception ex)
                {
                    // Handle any exceptions that might occur during loading the image
                    MessageBox.Show("Error loading the image: " + ex.Message);
                }
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.ImageColor = colorDialog.Color;
        }










        public Bitmap PickImage()
        {
            // Set properties for the file picker dialog
            openFileDialog1.Title = "Choose Image";
            openFileDialog1.Filter = "Image Files (*.png; *.jpg; *.jpeg; *.gif; *.bmp)|*.png; *.jpg; *.jpeg; *.gif; *.bmp";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            // Show the file picker dialog and get the result
            DialogResult result = openFileDialog1.ShowDialog();

            // Check if the user selected a file
            if (result == DialogResult.OK)
            {
                try
                {
                    // Get the path of the selected file
                    string selectedFile = openFileDialog1.FileName;

                    // Load the image from the selected file
                    Bitmap newImage = (Bitmap)Image.FromFile(selectedFile);

                    // Assign the loaded image to the Image property of your custom control
                    return newImage;

                    // Optionally, you might want to resize the image to fit your custom control
                    // xButton.Image = ResizeImage(newImage, xButton.Width, xButton.Height);
                }
                catch (Exception ex)
                {
                    // Handle any exceptions that might occur during loading the image
                    MessageBox.Show("Error loading the image: " + ex.Message);
                    return null;
                }
            }

            return null;
        }


        #region Default
        private void foreDefault_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.ForeColor = colorDialog.Color;

            xButtonLoad_Click(null, null);
        }
        private void fillDefault_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.FillColor = colorDialog.Color;

            xButtonLoad_Click(null, null);
        }
        private void borderDefault_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.BorderColor = colorDialog.Color;

            xButtonLoad_Click(null, null);
        }
        private void imageDefault_Click(object sender, EventArgs e)
        {
            xButton.Image = PickImage();

            xButtonLoad_Click(null, null);
        }
        private void imageColorDefault_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.ImageColor = colorDialog.Color;
            else
                xButton.ImageColor = Color.Empty;

            xButtonLoad_Click(null, null);
        }
        #endregion

        #region Hover
        private void foreHover_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.ForeHoverColor = colorDialog.Color;
            else
                xButton.ForeHoverColor = Color.Empty;

            xButtonLoad_Click(null, null);
        }
        private void fillHover_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.FillHoverColor = colorDialog.Color;
            else
                xButton.FillHoverColor = Color.Empty;

            xButtonLoad_Click(null, null);
        }
        private void borderHover_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.BorderHoverColor = colorDialog.Color;
            else
                xButton.BorderHoverColor = Color.Empty;

            xButtonLoad_Click(null, null);
        }
        private void imageHover_Click(object sender, EventArgs e)
        {
            xButton.ImageHover = PickImage();

            xButtonLoad_Click(null, null);
        }
        private void imageColorHover_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.ImageHoverColor = colorDialog.Color;
            else
                xButton.ImageHoverColor = Color.Empty;

            xButtonLoad_Click(null, null);
        }
        #endregion

        #region Active
        private void foreActive_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.ForeActiveColor = colorDialog.Color;
            else
                xButton.ForeActiveColor = Color.Empty;

            xButtonLoad_Click(null, null);
        }
        private void fillActive_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.FillActiveColor = colorDialog.Color;
            else
                xButton.FillActiveColor = Color.Empty;

            xButtonLoad_Click(null, null);
        }
        private void borderActive_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.BorderActiveColor = colorDialog.Color;
            else
                xButton.BorderActiveColor = Color.Empty;

            xButtonLoad_Click(null, null);
        }
        private void imageActive_Click(object sender, EventArgs e)
        {
            xButton.ImageActive = PickImage();

            xButtonLoad_Click(null, null);
        }
        private void imageColorActive_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.ImageActiveColor = colorDialog.Color;
            else
                xButton.ImageActiveColor = Color.Empty;

            xButtonLoad_Click(null, null);
        }
        #endregion

        #region Disabled
        private void foreDisabled_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.ForeDisabledColor = colorDialog.Color;
            else
                xButton.ForeDisabledColor = Color.Empty;

            xButtonLoad_Click(null, null);
        }
        private void fillDisabled_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.FillDisabledColor = colorDialog.Color;
            else
                xButton.FillDisabledColor = Color.Empty;

            xButtonLoad_Click(null, null);
        }
        private void borderDisabled_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.BorderDisabledColor = colorDialog.Color;
            else
                xButton.BorderDisabledColor = Color.Empty;

            xButtonLoad_Click(null, null);
        }
        private void imageDisabled_Click(object sender, EventArgs e)
        {
            xButton.ImageDisabled = PickImage();

            xButtonLoad_Click(null, null);
        }
        private void imageColorDisabled_Click(object sender, EventArgs e)
        {
            DialogResult dr = colorDialog.ShowDialog();

            if (dr == DialogResult.OK)
                xButton.ImageDisabledColor = colorDialog.Color;
            else
                xButton.ImageDisabledColor = Color.Empty;

            xButtonLoad_Click(null, null);
        }
        #endregion

        private void xButtonLoad_Click(object sender, EventArgs e)
        {
            foreDefault.BackColor = xButton.ForeColor;
            fillDefault.BackColor = xButton.FillColor;
            borderDefault.BackColor = xButton.BorderColor;
            imageDefault.Image = xButton.Image;
            imageColorDefault.BackColor = xButton.ImageColor;

            foreHover.BackColor = xButton.ForeHoverColor;
            fillHover.BackColor = xButton.FillHoverColor;
            borderHover.BackColor = xButton.BorderHoverColor;
            imageHover.Image = xButton.ImageHover;
            imageColorHover.BackColor = xButton.ImageHoverColor;

            foreActive.BackColor = xButton.ForeActiveColor;
            fillActive.BackColor = xButton.FillActiveColor;
            borderActive.BackColor = xButton.BorderActiveColor;
            imageActive.Image = xButton.ImageActive;
            imageColorActive.BackColor = xButton.ImageActiveColor;

            foreDisabled.BackColor = xButton.ForeDisabledColor;
            fillDisabled.BackColor = xButton.FillDisabledColor;
            borderDisabled.BackColor = xButton.BorderDisabledColor;
            imageDisabled.Image = xButton.ImageDisabled;
            imageColorDisabled.BackColor = xButton.ImageDisabledColor;

            textLeft.Checked = xButton.TextAlignment == ObjectAlignment.Left;
            textCenter.Checked = xButton.TextAlignment == ObjectAlignment.Center;
            textRight.Checked = xButton.TextAlignment == ObjectAlignment.Right;

            imageLeft.Checked = xButton.ImageAlignment == ObjectAlignment.Left;
            imageCenter.Checked = xButton.ImageAlignment == ObjectAlignment.Center;
            imageRight.Checked = xButton.ImageAlignment == ObjectAlignment.Right;

            imageFirst.Checked = xButton.DisplayOrder == ObjectDisplayOrder.ImageFirst;
            textFirst.Checked = xButton.DisplayOrder == ObjectDisplayOrder.TextFirst;
            imageFront.Checked = xButton.DisplayOrder == ObjectDisplayOrder.ImageFront;
            textFront.Checked = xButton.DisplayOrder == ObjectDisplayOrder.TextFront;

            fontSize.Checked = xButton.ImageSize == ImageSize.FontHeight;
            original.Checked = xButton.ImageSize == ImageSize.Original;
        }

        private void textLeft_CheckedChanged(object sender, EventArgs e) { xButton.TextAlignment = ObjectAlignment.Left; }
        private void textCenter_CheckedChanged(object sender, EventArgs e) { xButton.TextAlignment = ObjectAlignment.Center; }
        private void textRight_CheckedChanged(object sender, EventArgs e) { xButton.TextAlignment = ObjectAlignment.Right; }

        private void imageLeft_CheckedChanged(object sender, EventArgs e) { xButton.ImageAlignment = ObjectAlignment.Left; }
        private void imageCenter_CheckedChanged(object sender, EventArgs e) { xButton.ImageAlignment = ObjectAlignment.Center; }
        private void imageRight_CheckedChanged(object sender, EventArgs e) { xButton.ImageAlignment = ObjectAlignment.Right; }

        private void imageFirst_CheckedChanged(object sender, EventArgs e) { xButton.DisplayOrder = ObjectDisplayOrder.ImageFirst; }
        private void textFirst_CheckedChanged(object sender, EventArgs e) { xButton.DisplayOrder = ObjectDisplayOrder.TextFirst; }
        private void imageFront_CheckedChanged(object sender, EventArgs e) { xButton.DisplayOrder = ObjectDisplayOrder.ImageFront; }
        private void textFront_CheckedChanged(object sender, EventArgs e) { xButton.DisplayOrder = ObjectDisplayOrder.TextFront; }

        private void fontSize_CheckedChanged(object sender, EventArgs e) { xButton.ImageSize = ImageSize.FontHeight; }
        private void original_CheckedChanged(object sender, EventArgs e) { xButton.ImageSize = ImageSize.Original; }
    }
}
