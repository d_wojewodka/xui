﻿
# UIX

Collection of controls for WinForms



## xButton
# Video - click to open in YouTube
[![Video Demo](https://gitlab.com/d_wojewodka/uix/-/raw/main/xButton.png)](https://www.youtube.com/watch?v=sOlPkqMLi6U)
| Property | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `BorderColor` | `Color` | base, hover, active, disabled, temporary |
| `FillColor` | `Color` | base, hover, active, disabled, temporary |
| `ForeColor` | `Color` | base, hover, active, disabled, temporary |
| `ImageColor` | `Color` | base, hover, active, disabled, temporary |
| `BorderShadow` | `bool` | extra feature that is not solid color, looks like shaded color |
| `BorderRadius` | `int` | radius of border |
| `BorderThickness` | `int` | thickness of border |
| `BorderStyle` | `enum` | style of border |
| `ImageSize` | `enum` | sizes of the image |
| `ImageCustomSize` | `Size` | custom size of image: width and height |
| `TextAlignment` | `enum` | alignment of text |
| `ImageAlignment` | `enum` | alignment of image |
| `DisplayOrder` | `enum` | when alignment of text and image are same, then this property decide display order |
| `SpaceBetween` | `int` | space between text and image |
| `Image` | `Bitmap` | image that can be displayed in button -> base, hover, active, disabled |

StartFlash - flash will stop when you mouse over control or animation finished it's task
| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `targetColor` | `Color` | target flash color |
| `durationMillis` | `int` | how long animation should last (colors: A -> B -> A) |
| `colorShiftCount` | `int` | how many colors are added between color A and B |
| `flashCount` | `int` | how many times flash have to occur (-1 for infinite) |

I wrote that control so you can customize it to your need. It supports 4 states: default, hover, active and disabled. 
In each of them you can set custom color, even image color can change. I've added some 
basic positioning like left, right and center and image to text order.



## xToolTip
![Logo](https://gitlab.com/d_wojewodka/uix/-/raw/main/xToolTip.png)
| Property | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `Padding` | `Padding` | padding around the tooltip text. |
| `TextAlign` | `TextAlign` | alignment of text |
| `FillColor` | `Color` | background color |
| `TextShadowColor` | `Color` | text shadow color |
| `BorderColor` | `Color` | border color |
| `BorderThickness` | `int` | thickness of border |
| `TitleFont` | `Font` | font used for the tooltip title |
| `TitleForeColor` | `Color` | color of tooltip title |
| `TitleAlign` | `TextAlign` | alignment of tooltip title |
| `TitleTextSpace` | `int` | space between tooltip title and text |
| `TitleShadowColor` | `Color` | title shadow color |

SetToolTip - Attach tooltip to control
| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `control` | `Control` | control that tooltip has to be attached |
| `text` | `string` | tooltip text |
| `title` | `string = ""` | tooltip title, can be empty |