﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

public class RoundedRectangle
{
    private float radius;
    private GraphicsPath graphicsPath;
    private float x;
    private float y;
    private float width;
    private float height;

    public RoundedRectangle() { }

    public RoundedRectangle(float width, float height, float radius, float x = 0f, float y = 0f)
    {
        this.width = width;
        this.height = height;
        this.radius = radius;
        this.x = x;
        this.y = y;
        this.graphicsPath = new GraphicsPath();

        if (radius <= 0f)
            this.graphicsPath.AddRectangle(new RectangleF(x, y, width - 1, height - 1));
        else
        {
            RectangleF ef = new RectangleF(x, y, 2f * radius, 2f * radius);
            RectangleF ef2 = new RectangleF((width - (2f * radius)) - 1f, y, 2f * radius, 2f * radius);
            RectangleF ef3 = new RectangleF(x, (height - (2f * radius)) - 1f, 2f * radius, 2f * radius);
            RectangleF ef4 = new RectangleF((width - (2f * radius)) - 1f,
                (height - (2f * radius)) - 1f, 2f * radius, 2f * radius);

            this.graphicsPath.AddArc(ef, 180f, 90f);
            this.graphicsPath.AddArc(ef2, 270f, 90f);
            this.graphicsPath.AddArc(ef4, 0f, 90f);
            this.graphicsPath.AddArc(ef3, 90f, 90f);
            this.graphicsPath.CloseAllFigures();
        }
    }

    public RoundedRectangle(Rectangle rect, int radius)
    {
        this.graphicsPath = new GraphicsPath();
        float curveSize = radius * 2F;

        this.graphicsPath.StartFigure();
        this.graphicsPath.AddArc(rect.X, rect.Y, curveSize, curveSize, 180, 90);
        this.graphicsPath.AddArc(rect.Right - curveSize, rect.Y, curveSize, curveSize, 270, 90);
        this.graphicsPath.AddArc(rect.Right - curveSize, rect.Bottom - curveSize, curveSize, curveSize, 0, 90);
        this.graphicsPath.AddArc(rect.X, rect.Bottom - curveSize, curveSize, curveSize, 90, 90);
        this.graphicsPath.CloseFigure();
    }

    public GraphicsPath Path => this.graphicsPath;

    public RectangleF Rect => new RectangleF(this.x, this.y, this.width, this.height);

    public float Radius
    {
        get => this.radius;
        set => this.radius = value;
    }
}