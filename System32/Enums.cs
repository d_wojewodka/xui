﻿using System;
using System.Runtime.InteropServices;

public enum MonitorDpiType : int
{
    MDT_EFFECTIVE_DPI = 0,
    MDT_ANGULAR_DPI = 1,
    MDT_RAW_DPI = 2,
    MDT_DEFAULT = MDT_EFFECTIVE_DPI
}

public enum MonitorFromWindowFlags : uint
{
    MONITOR_DEFAULTTONULL = 0x0,
    MONITORINFOF_PRIMARY = 0x1,
    MONITOR_DEFAULTTONEAREST = 0x2,
    MONITOR_DEFAULTTOPRIMARY = 0x1
}

public enum PROCESS_DPI_AWARENESS
{
    PROCESS_DPI_UNAWARE = 0,
    PROCESS_SYSTEM_DPI_AWARE = 1,
    PROCESS_PER_MONITOR_DPI_AWARE = 2
}

public enum FlashWindowFlags : uint
{
    FLASHW_STOP = 0,
    FLASHW_CAPTION = 1,
    FLASHW_TRAY = 2,
    FLASHW_ALL = 3,
    FLASHW_TIMER = 4,
    FLASHW_TIMERNOFG = 12
}
public enum DpiType
{
    Effective = 0,
    Angular = 1,
    Raw = 2,
}

public enum SetWindowPosFlags
{
    SWP_NOSIZE = 0x0001,
    SWP_NOMOVE = 0x0002,
    SWP_NOZORDER = 0x0004,
    SWP_NOREDRAW = 0x0008,
    SWP_NOACTIVATE = 0x0010,
    SWP_FRAMECHANGED = 0x0020,
    SWP_SHOWWINDOW = 0x0040,
    SWP_HIDEWINDOW = 0x0080,
    SWP_NOCOPYBITS = 0x0100,
    SWP_NOOWNERZORDER = 0x0200,
    SWP_NOSENDCHANGING = 0x0400,
    SWP_DRAWFRAME = 0x0020,
    SWP_NOREPOSITION = 0x0200,
    SWP_DEFERERASE = 0x2000,
    SWP_ASYNCWINDOWPOS = 0x4000
}

[Flags]
public enum RedrawWindowFlags
{
    RDW_INVALIDATE = 0x0001,
    RDW_INTERNALPAINT = 0x0002,
    RDW_ERASE = 0x0004,
    RDW_VALIDATE = 0x0008,
    RDW_NOINTERNALPAINT = 0x0010,
    RDW_NOERASE = 0x0020,
    RDW_NOCHILDREN = 0x0040,
    RDW_ALLCHILDREN = 0x0080,
    RDW_UPDATENOW = 0x0100,
    RDW_ERASENOW = 0x0200,
    RDW_FRAME = 0x0400,
    RDW_NOFRAME = 0x0800,
}


// enum instead of internal struct
public enum SystemCommandFlags
{
    SC_SIZE = 0xF000,
    SC_MOVE = 0xF010,
    SC_MINIMIZE = 0xF020,
    SC_MAXIMIZE = 0xF030,
    SC_NEXTWINDOW = 0xF040,
    SC_PREVWINDOW = 0xF050,
    SC_CLOSE = 0xF060,
    SC_VSCROLL = 0xF070,
    SC_HSCROLL = 0xF080,
    SC_MOUSEMENU = 0xF090,
    SC_KEYMENU = 0xF100,
    SC_ARRANGE = 0xF110,
    SC_RESTORE = 0xF120,
    SC_TASKLIST = 0xF130,
    SC_SCREENSAVE = 0xF140,
    SC_HOTKEY = 0xF150,
    SC_DEFAULT = 0xF160,
    SC_MONITORPOWER = 0xF170,
    SC_CONTEXTHELP = 0xF180,
    SC_SEPARATOR = 0xF00F,
    SCF_ISSECURE = 0x00000001,
}

public enum HitTest : int
{
    HTERROR = (-2),
    HTTRANSPARENT = (-1),
    HTNOWHERE = 0,
    HTCLIENT = 1,
    HTCAPTION = 2,
    HTSYSMENU = 3,
    HTGROWBOX = 4,
    HTSIZE = HTGROWBOX,
    HTMENU = 5,
    HTHSCROLL = 6,
    HTVSCROLL = 7,
    HTMINBUTTON = 8,
    HTMAXBUTTON = 9,
    HTLEFT = 10,
    HTRIGHT = 11,
    HTTOP = 12,
    HTTOPLEFT = 13,
    HTTOPRIGHT = 14,
    HTBOTTOM = 15,
    HTBOTTOMLEFT = 16,
    HTBOTTOMRIGHT = 17,
    HTBORDER = 18,
    HTREDUCE = HTMINBUTTON,
    HTZOOM = HTMAXBUTTON,
    HTSIZEFIRST = HTLEFT,
    HTSIZELAST = HTBOTTOMRIGHT,
    HTOBJECT = 19,
    HTCLOSE = 20,
    HTHELP = 21
}

[Flags]
public enum WindowStyles : long
{
    WS_OVERLAPPED = 0x00000000,
    WS_POPUP = 0x80000000,
    WS_CHILD = 0x40000000,
    WS_MINIMIZE = 0x20000000,
    WS_VISIBLE = 0x10000000,
    WS_DISABLED = 0x08000000,
    WS_CLIPSIBLINGS = 0x04000000,
    WS_CLIPCHILDREN = 0x02000000,
    WS_MAXIMIZE = 0x01000000,
    WS_CAPTION = 0x00C00000,
    WS_BORDER = 0x00800000,
    WS_DLGFRAME = 0x00400000,
    WS_VSCROLL = 0x00200000,
    WS_HSCROLL = 0x00100000,
    WS_SYSMENU = 0x00080000,
    WS_THICKFRAME = 0x00040000,
    WS_GROUP = 0x00020000,
    WS_TABSTOP = 0x00010000,
    WS_MINIMIZEBOX = 0x00020000,
    WS_MAXIMIZEBOX = 0x00010000,
    WS_TILED = 0x00000000,
    WS_ICONIC = 0x20000000,
    WS_SIZEBOX = 0x00040000,
    WS_POPUPWINDOW = 0x80880000,
    WS_OVERLAPPEDWINDOW = 0x00CF0000,
    WS_TILEDWINDOW = 0x00CF0000,
    WS_CHILDWINDOW = 0x40000000
}

[Flags]
public enum WindowExStyles
{
    WS_EX_DLGMODALFRAME = 0x00000001,
    WS_EX_NOPARENTNOTIFY = 0x00000004,
    WS_EX_NOACTIVATE = 0x08000000,
    WS_EX_TOPMOST = 0x00000008,
    WS_EX_ACCEPTFILES = 0x00000010,
    WS_EX_TRANSPARENT = 0x00000020,
    WS_EX_MDICHILD = 0x00000040,
    WS_EX_TOOLWINDOW = 0x00000080,
    WS_EX_WINDOWEDGE = 0x00000100,
    WS_EX_CLIENTEDGE = 0x00000200,
    WS_EX_CONTEXTHELP = 0x00000400,
    WS_EX_RIGHT = 0x00001000,
    WS_EX_LEFT = 0x00000000,
    WS_EX_RTLREADING = 0x00002000,
    WS_EX_LTRREADING = 0x00000000,
    WS_EX_LEFTSCROLLBAR = 0x00004000,
    WS_EX_RIGHTSCROLLBAR = 0x00000000,
    WS_EX_CONTROLPARENT = 0x00010000,
    WS_EX_STATICEDGE = 0x00020000,
    WS_EX_APPWINDOW = 0x00040000,
    WS_EX_OVERLAPPEDWINDOW = 0x00000300,
    WS_EX_PALETTEWINDOW = 0x00000188,
    WS_EX_LAYERED = 0x00080000
}

public enum GetWindowLongFlags
{
    GWL_WNDPROC = -4,
    GWL_HINSTANCE = -6,
    GWL_HWNDPARENT = -8,
    GWL_STYLE = -16,
    GWL_EXSTYLE = -20,
    GWL_USERDATA = -21,
    GWL_ID = -12
}

public enum IDC_STANDARD_CURSORS
{
    IDC_ARROW = 32512,
    IDC_IBEAM = 32513,
    IDC_WAIT = 32514,
    IDC_CROSS = 32515,
    IDC_UPARROW = 32516,
    IDC_SIZE = 32640,
    IDC_ICON = 32641,
    IDC_SIZENWSE = 32642,
    IDC_SIZENESW = 32643,
    IDC_SIZEWE = 32644,
    IDC_SIZENS = 32645,
    IDC_SIZEALL = 32646,
    IDC_NO = 32648,
    IDC_HAND = 32649,
    IDC_APPSTARTING = 32650,
    IDC_HELP = 32651
}

public enum ResizeDirection
{
    Left = 61441,
    Right = 61442,
    Top = 61443,
    TopLeft = 61444,
    TopRight = 61445,
    Bottom = 61446,
    BottomLeft = 61447,
    BottomRight = 61448,
}

public enum ShowWindowStyles : short
{
    SW_HIDE = 0,
    SW_SHOWNORMAL = 1,
    SW_NORMAL = 1,
    SW_SHOWMINIMIZED = 2,
    SW_SHOWMAXIMIZED = 3,
    SW_MAXIMIZE = 3,
    SW_SHOWNOACTIVATE = 4,
    SW_SHOW = 5,
    SW_MINIMIZE = 6,
    SW_SHOWMINNOACTIVE = 7,
    SW_SHOWNA = 8,
    SW_RESTORE = 9,
    SW_SHOWDEFAULT = 10,
    SW_FORCEMINIMIZE = 11,
    SW_MAX = 11
}

[Flags()]
public enum DCX
{
    CACHE = 0x2,
    CLIPCHILDREN = 0x8,
    CLIPSIBLINGS = 0x10,
    EXCLUDERGN = 0x40,
    EXCLUDEUPDATE = 0x100,
    INTERSECTRGN = 0x80,
    INTERSECTUPDATE = 0x200,
    LOCKWINDOWUPDATE = 0x400,
    NORECOMPUTE = 0x100000,
    NORESETATTRS = 0x4,
    PARENTCLIP = 0x20,
    VALIDATE = 0x200000,
    WINDOW = 0x1,
}

/// <summary>
/// The services requested. This member can be a combination of the following values.
/// </summary>
/// <seealso cref="http://msdn.microsoft.com/en-us/library/ms645604%28v=vs.85%29.aspx"/>
[Flags]
public enum TMEFlags : uint
{
    /// <summary>
    /// The caller wants to cancel a prior tracking request. The caller should also specify the type of tracking that it wants to cancel. For example, to cancel hover tracking, the caller must pass the TME_CANCEL and TME_HOVER flags.
    /// </summary>
    TME_CANCEL = 0x80000000,
    /// <summary>
    /// The caller wants hover notification. Notification is delivered as a WM_MOUSEHOVER message.
    /// If the caller requests hover tracking while hover tracking is already active, the hover timer will be reset.
    /// This flag is ignored if the mouse pointer is not over the specified window or area.
    /// </summary>
    TME_HOVER = 0x00000001,
    /// <summary>
    /// The caller wants leave notification. Notification is delivered as a WM_MOUSELEAVE message. If the mouse is not over the specified window or area, a leave notification is generated immediately and no further tracking is performed.
    /// </summary>
    TME_LEAVE = 0x00000002,
    /// <summary>
    /// The caller wants hover and leave notification for the nonclient areas. Notification is delivered as WM_NCMOUSEHOVER and WM_NCMOUSELEAVE messages.
    /// </summary>
    TME_NONCLIENT = 0x00000010,
    /// <summary>
    /// The function fills in the structure instead of treating it as a tracking request. The structure is filled such that had that structure been passed to TrackMouseEvent, it would generate the current tracking. The only anomaly is that the hover time-out returned is always the actual time-out and not HOVER_DEFAULT, if HOVER_DEFAULT was specified during the original TrackMouseEvent request.
    /// </summary>
    TME_QUERY = 0x40000000,
}


public enum WindowsMessages
{
    // used for message filtering or indicating that a window has no messages to process.
    WM_NULL = 0x0000, // Null message
    WM_CREATE = 0x0001, // Window created
    WM_DESTROY = 0x0002, // Window destroyed
    WM_MOVE = 0x0003, // Window moved
    WM_SIZE = 0x0005, // Window sized
    WM_ACTIVATE = 0x0006, // Window activated
    WM_SETFOCUS = 0x0007, // Set focus to window
    WM_KILLFOCUS = 0x0008, // Kill focus of window
    WM_ENABLE = 0x000A, // Window enabled
    WM_SETREDRAW = 0x000B, // Set redraw of window
    WM_SETTEXT = 0x000C, // Set text of window
    WM_GETTEXT = 0x000D, // Get text of window
    WM_GETTEXTLENGTH = 0x000E, // Get length of text of window
    WM_PAINT = 0x000F, // Window painted
    WM_CLOSE = 0x0010, // Window close message
    WM_QUERYENDSESSION = 0x0011, // Query end session
    WM_QUIT = 0x0012, // Window quit message
    WM_QUERYOPEN = 0x0013, // Query open
    WM_ERASEBKGND = 0x0014, // Erase background of window
    WM_SYSCOLORCHANGE = 0x0015, // System color changed
    WM_ENDSESSION = 0x0016, // End session
    WM_SHOWWINDOW = 0x0018, // Show window
    WM_WININICHANGE = 0x001A, // Windows initialization change
    WM_SETTINGCHANGE = 0x001A, // Setting change
    WM_DEVMODECHANGE = 0x001B, // Device mode change
    WM_ACTIVATEAPP = 0x001C, // Activate application
    WM_FONTCHANGE = 0x001D, // Font change
    WM_TIMECHANGE = 0x001E, // Time change
    WM_CANCELMODE = 0x001F, // Cancel mode
    WM_SETCURSOR = 0x0020, // Set cursor
    WM_MOUSEACTIVATE = 0x0021, // Mouse activate
    WM_CHILDACTIVATE = 0x0022, // Child activate
    WM_QUEUESYNC = 0x0023, // Queue sync
    WM_GETMINMAXINFO = 0x0024, // Get min max info
    WM_PAINTICON = 0x0026, // Paint icon
    WM_ICONERASEBKGND = 0x0027, // Icon erase background
    WM_NEXTDLGCTL = 0x0028, // Next dialog control
    WM_SPOOLERSTATUS = 0x002A, // Spooler status
    WM_DRAWITEM = 0x002B, // Draw item
    WM_MEASUREITEM = 0x002C, // Measure item
    WM_DELETEITEM = 0x002D, // Delete item
    WM_VKEYTOITEM = 0x002E, // Virtual key to item
    WM_CHARTOITEM = 0x002F, // Char to item
    WM_SETFONT = 0x0030, // Set font
    WM_GETFONT = 0x0031, // Get font
    WM_SETHOTKEY = 0x0032, // Set hotkey
    WM_GETHOTKEY = 0x0033, // Get hotkey
    WM_QUERYDRAGICON = 0x0037, // Query drag icon
    WM_COMPAREITEM = 0x0039, // Compare item
    WM_GETOBJECT = 0x003D, // Get object
    WM_COMPACTING = 0x0041, // Compacting
    WM_COMMNOTIFY = 0x0044, // Communication notify
    WM_WINDOWPOSCHANGING = 0x0046, // Window position changing
    WM_WINDOWPOSCHANGED = 0x0047, // Window position changed
    WM_POWER = 0x0048, // Power
    WM_COPYDATA = 0x004A, // Copy data
    WM_CANCELJOURNAL = 0x004B, // Cancel journal
    WM_NOTIFY = 0x004E, // Notify
    WM_INPUTLANGCHANGEREQUEST = 0x0050, // Input language change request
    WM_INPUTLANGCHANGE = 0x0051, // Input language change
    WM_TCARD = 0x0052, // T card
    WM_HELP = 0x0053, // Help
    WM_USERCHANGED = 0x0054, // User changed
    WM_NOTIFYFORMAT = 0x0055, // Notify format
    WM_CONTEXTMENU = 0x007B, // Context menu
    WM_STYLECHANGING = 0x007C, // Style changing
    WM_STYLECHANGED = 0x007D, // Style changed
    WM_DISPLAYCHANGE = 0x007E, // Display change
    WM_GETICON = 0x007F, // Get icon
    WM_SETICON = 0x0080, // Set icon
    WM_NCCREATE = 0x0081, // Non-client created
    WM_NCDESTROY = 0x0082, // Non-client destroyed
    WM_NCCALCSIZE = 0x0083, // Non-client calculate size
    WM_NCHITTEST = 0x0084, // Non-client hit test
    WM_NCPAINT = 0x0085, // Non-client paint
    WM_NCACTIVATE = 0x0086, // Non-client activate
    WM_GETDLGCODE = 0x0087, // Get dialog code
    WM_SYNCPAINT = 0x0088, // Sync paint
    WM_NCMOUSEMOVE = 0x00A0, // Non-client mouse move
    WM_NCLBUTTONDOWN = 0x00A1, // Non-client left button down
    WM_NCLBUTTONUP = 0x00A2, // Non-client left button up
    WM_NCLBUTTONDBLCLK = 0x00A3, // Non-client left button double click
    WM_NCRBUTTONDOWN = 0x00A4, // Non-client right button down
    WM_NCRBUTTONUP = 0x00A5, // Non-client right button up
    WM_NCRBUTTONDBLCLK = 0x00A6, // Non-client right button double click
    WM_NCMBUTTONDOWN = 0x00A7, // Non-client middle button down
    WM_NCMBUTTONUP = 0x00A8, // Non-client middle button up
    WM_NCMBUTTONDBLCLK = 0x00A9, // Non-client middle button double click
    WM_NCXBUTTONDOWN = 0x00AB, // Non-client X button down
    WM_NCXBUTTONUP = 0x00AC, // Non-client X button up
    WM_NCXBUTTONDBLCLK = 0x00AD, // Non-client X button double click
    WM_INPUT_DEVICE_CHANGE = 0x00FE, // Input device change
    WM_INPUT = 0x00FF, // Input
    WM_KEYDOWN = 0x0100, // Key down
    WM_KEYUP = 0x0101, // Key up
    WM_CHAR = 0x0102, // Character
    WM_DEADCHAR = 0x0103, // Dead character
    WM_SYSKEYDOWN = 0x0104, // System key down
    WM_SYSKEYUP = 0x0105, // System key up
    WM_SYSCHAR = 0x0106, // System character
    WM_SYSDEADCHAR = 0x0107, // System dead character
    WM_UNICHAR = 0x0109, // Unicode character
    WM_KEYLAST = 0x0109, // Key last
    WM_IME_STARTCOMPOSITION = 0x010D, // IME start composition
    WM_IME_ENDCOMPOSITION = 0x010E, // IME end composition
    WM_IME_COMPOSITION = 0x010F, // IME composition
    WM_IME_KEYLAST = 0x010F, // IME key last
    WM_INITDIALOG = 0x0110, // Initialize dialog
    WM_COMMAND = 0x0111, // Command
    WM_SYSCOMMAND = 0x0112, // System command
    WM_TIMER = 0x0113, // Timer
    WM_HSCROLL = 0x0114, // Horizontal scroll
    WM_VSCROLL = 0x0115, // Vertical scroll
    WM_INITMENU = 0x0116, // Initialize menu
    WM_INITMENUPOPUP = 0x0117, // Initialize menu popup
    WM_MENUSELECT = 0x011F, // Menu select
    WM_MENUCHAR = 0x0120, // Menu character
    WM_ENTERIDLE = 0x0121, // Enter idle
    WM_MENURBUTTONUP = 0x0122, // Menu right button up
    WM_MENUDRAG = 0x0123, // Menu drag
    WM_MENUGETOBJECT = 0x0124, // Menu get object
    WM_UNINITMENUPOPUP = 0x0125, // Uninitialize menu popup
    WM_MENUCOMMAND = 0x0126, // Menu command
    WM_CHANGEUISTATE = 0x0127, // Change UI state
    WM_UPDATEUISTATE = 0x0128, // Update UI state
    WM_QUERYUISTATE = 0x0129, // Query UI state
    WM_CTLCOLORMSGBOX = 0x0132, // Control color message box
    WM_CTLCOLOREDIT = 0x0133, // Control color edit
    WM_CTLCOLORLISTBOX = 0x0134, // Control color list box
    WM_CTLCOLORBTN = 0x0135, // Control color button
    WM_CTLCOLORDLG = 0x0136, // Control color dialog
    WM_CTLCOLORSCROLLBAR = 0x0137, // Control color scrollbar
    WM_CTLCOLORSTATIC = 0x0138, // Control color static
    WM_MOUSEMOVE = 0x0200, // Mouse move
    WM_LBUTTONDOWN = 0x0201, // Left button down
    WM_LBUTTONUP = 0x0202, // Left button up
    WM_LBUTTONDBLCLK = 0x0203, // Left button double click
    WM_RBUTTONDOWN = 0x0204, // Right button down
    WM_RBUTTONUP = 0x0205, // Right button up
    WM_RBUTTONDBLCLK = 0x0206, // Right button double click
    WM_MBUTTONDOWN = 0x0207, // Middle button down
    WM_MBUTTONUP = 0x0208, // Middle button up
    WM_MBUTTONDBLCLK = 0x0209, // Middle button double click
    WM_MOUSEWHEEL = 0x020A, // Mouse wheel
    WM_XBUTTONDOWN = 0x020B, // X button down
    WM_XBUTTONUP = 0x020C, // X button up
    WM_XBUTTONDBLCLK = 0x020D, // X button double click
    WM_MOUSEHWHEEL = 0x020E, // Mouse horizontal wheel
    WM_PARENTNOTIFY = 0x0210, // Parent notify
    WM_ENTERMENULOOP = 0x0211, // Enter menu loop
    WM_EXITMENULOOP = 0x0212, // Exit menu loop
    WM_NEXTMENU = 0x0213, // Next menu
    WM_SIZING = 0x0214, // Sizing
    WM_CAPTURECHANGED = 0x0215, // Capture changed
    WM_MOVING = 0x0216, // Moving
    WM_POWERBROADCAST = 0x0218, // Power broadcast
    WM_DEVICECHANGE = 0x0219, // Device change
    WM_MDICREATE = 0x0220, // MDI create
    WM_MDIDESTROY = 0x0221, // MDI destroy
    WM_MDIACTIVATE = 0x0222, // MDI activate
    WM_MDIRESTORE = 0x0223, // MDI restore
    WM_MDINEXT = 0x0224, // MDI next
    WM_MDIMAXIMIZE = 0x0225, // MDI maximize
    WM_MDITILE = 0x0226, // MDI tile
    WM_MDICASCADE = 0x0227, // MDI cascade
    WM_MDIICONARRANGE = 0x0228, // MDI icon arrange
    WM_MDIGETACTIVE = 0x0229, // MDI get active
    WM_MDISETMENU = 0x0230, // MDI set menu
    WM_ENTERSIZEMOVE = 0x0231, // Enter size move
    WM_EXITSIZEMOVE = 0x0232, // Exit size move
    WM_DROPFILES = 0x0233, // Drop files
    WM_MDIREFRESHMENU = 0x0234, // MDI refresh menu
    WM_IME_SETCONTEXT = 0x0281, // IME set context
    WM_IME_NOTIFY = 0x0282, // IME notify
    WM_IME_CONTROL = 0x0283, // IME control
    WM_IME_COMPOSITIONFULL = 0x0284, // IME composition full
    WM_IME_SELECT = 0x0285, // IME select
    WM_IME_CHAR = 0x0286, // IME character
    WM_IME_REQUEST = 0x0288, // IME request
    WM_IME_KEYDOWN = 0x0290, // IME key down
    WM_IME_KEYUP = 0x0291, // IME key up
    WM_MOUSEHOVER = 0x02A1, // Mouse hover
    WM_MOUSELEAVE = 0x02A3, // Mouse leave
    WM_NCMOUSEHOVER = 0x02A0, // Non-client mouse hover
    WM_NCMOUSELEAVE = 0x02A2, // Non-client mouse leave
    WM_WTSSESSION_CHANGE = 0x02B1, // WTS session change
    WM_TABLET_FIRST = 0x02c0, // Tablet first
    WM_TABLET_LAST = 0x02df, // Tablet last
    WM_CUT = 0x0300, // Cut
    WM_COPY = 0x0301, // Copy
    WM_PASTE = 0x0302, // Paste
    WM_CLEAR = 0x0303, // Clear
    WM_UNDO = 0x0304, // Undo
    WM_RENDERFORMAT = 0x0305, // Render format
    WM_RENDERALLFORMATS = 0x0306, // Render all formats
    WM_DESTROYCLIPBOARD = 0x0307, // Destroy clipboard
    WM_DRAWCLIPBOARD = 0x0308, // Draw clipboard
    WM_PAINTCLIPBOARD = 0x0309, // Paint clipboard
    WM_VSCROLLCLIPBOARD = 0x030A, // Vertical scroll clipboard
    WM_SIZECLIPBOARD = 0x030B, // Size clipboard
    WM_ASKCBFORMATNAME = 0x030C, // Ask clipboard format name
    WM_CHANGECBCHAIN = 0x030D, // Change clipboard chain
    WM_HSCROLLCLIPBOARD = 0x030E, // Horizontal scroll clipboard
    WM_QUERYNEWPALETTE = 0x030F, // Query new palette
    WM_PALETTEISCHANGING = 0x0310, // Palette is changing
    WM_PALETTECHANGED = 0x0311, // Palette changed
    WM_HOTKEY = 0x0312, // Hotkey
    WM_PRINT = 0x0317, // Print
    WM_PRINTCLIENT = 0x0318, // Print client
    WM_APPCOMMAND = 0x0319, // App command
    WM_THEMECHANGED = 0x031A, // Theme changed
    WM_CLIPBOARDUPDATE = 0x031D, // Clipboard update
    WM_DWMCOMPOSITIONCHANGED = 0x031E, // DWM composition changed
    WM_DWMNCRENDERINGCHANGED = 0x031F, // DWM non-client rendering changed
    WM_DWMCOLORIZATIONCOLORCHANGED = 0x0320, // DWM colorization color changed
    WM_DWMWINDOWMAXIMIZEDCHANGE = 0x0321, // DWM window maximized changed
    WM_DWMSENDICONICTHUMBNAIL = 0x0323, // DWM send iconic thumbnail
    WM_DWMSENDICONICLIVEPREVIEWBITMAP = 0x0326, // DWM send iconic live preview bitmap
    WM_GETTITLEBARINFOEX = 0x033F, // Get title bar info extended
    WM_HANDHELDFIRST = 0x0358, // Handheld first
    WM_HANDHELDLAST = 0x035F, // Handheld last
    WM_AFXFIRST = 0x0360, // AFX first
    WM_AFXLAST = 0x037F, // AFX last
    WM_PENWINFIRST = 0x0380, // Pen window first
    WM_PENWINLAST = 0x038F, // Pen window last
    WM_APP = 0x8000, // Application
    WM_USER = 0x0400, // User message

    // Messages sent to the window procedure for drawing in the non-client area.
    WM_NCUAHDRAWCAPTION = 0x00AE,   // Sent to draw the caption in the non-client area of a window.
    WM_NCUAHDRAWFRAME = 0x00AF,     // Sent to draw the frame in the non-client area of a window.

    // Notification messages sent by the taskbar notification area.
    NIN_BALLOONSHOW = 0x402,         // Indicates that a balloon tooltip should be shown.
    NIN_BALLOONHIDE = 0x403,         // Indicates that a balloon tooltip should be hidden.
    NIN_BALLOONTIMEOUT = 0x404,      // Indicates that a balloon tooltip has timed out.
    NIN_BALLOONUSERCLICK = 0x405,    // Indicates that the user clicked on a balloon tooltip.

    // Reflected messages - messages that are sent to the control's parent window rather than directly to the control.
    WM_REFLECT = WM_USER + 0x1C00,   // Used to send messages to child windows from their parent window.

    // System command messages - messages sent when a user selects a command from the window menu.
    SC_MOVE = 0xF010,                 // Indicates that the user is attempting to move the window.
    SC_SIZE = 0xF000,                 // Indicates that the user is attempting to resize the window.

    // Custom or unknown messages.
    WM_UNKNOWN_GHOST = 0xC1BC,       // Indicates an unknown or custom Windows message.

    // DPI change message - sent to a window when the DPI (dots per inch) setting for the display changes.
    WM_DPICHANGED = 0x02E0           // Indicates that the DPI settings of the display have changed.
}