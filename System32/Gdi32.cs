﻿
using System.Runtime.InteropServices;
using System;
using System.Drawing;

public class Gdi32
{
    const string gdi32 = "gdi32.dll";


    [DllImport(gdi32, EntryPoint = "DeleteObject")] [return: MarshalAs(UnmanagedType.Bool)] public static extern bool DeleteObject(
        [In] IntPtr hObject
    );
    [DllImport(gdi32, EntryPoint = "SelectObject", SetLastError = true)] public static extern IntPtr SelectObject(
        [In] IntPtr hdc, 
        [In] IntPtr hgdiobj
    );
    [DllImport(gdi32, EntryPoint = "CreateCompatibleDC", SetLastError = true)] public static extern IntPtr CreateCompatibleDC(
        [In] IntPtr hdc
    );
    [DllImport(gdi32, EntryPoint = "DeleteDC")] public static extern bool DeleteDC(
        [In] IntPtr hdc
    );
    [DllImport(gdi32)] public static extern IntPtr CreateRectRgn(
        int nLeftRect, 
        int nTopRect, 
        int nRightRect,
        int nBottomRect
    );
    [DllImport(gdi32)] internal static extern int CombineRgn(
        IntPtr hrgnDest, 
        IntPtr hrgnSrc1, 
        IntPtr hrgnSrc2, 
        int fnCombineMode
    );










    public static IntPtr CreateRectRgn(Rectangle rect)
    {
        return CreateRectRgn(rect.Left, rect.Top, rect.Right, rect.Bottom);
    }
}
