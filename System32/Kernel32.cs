﻿using System.Runtime.InteropServices;
using System;

public class Kernel32
{
    const string kernel32 = "kernel32.dll";

    [DllImport(kernel32, SetLastError = true)]
    public static extern IntPtr OpenProcess(
        uint dwDesiredAccess, 
        [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, 
        uint dwProcessId
    );

    [DllImport(kernel32)] [return: MarshalAs(UnmanagedType.Bool)] public static extern bool CloseHandle(
        IntPtr handle
    );
}
