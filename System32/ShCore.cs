﻿using System;
using System.Runtime.InteropServices;


public class ShCore
{
    const string shcore = "shcore.dll";


    [DllImport(shcore)] public static extern int GetDpiForMonitor(
        IntPtr hMonitor, 
        MonitorDpiType dpiType, 
        out int dpiX, 
        out int dpiY
    );

    //https://msdn.microsoft.com/en-us/library/windows/desktop/dn280510(v=vs.85).aspx
    [DllImport(shcore)] public static extern IntPtr GetDpiForMonitor(
        [In] IntPtr hmonitor, 
        [In] DpiType dpiType, 
        [Out] out uint dpiX, 
        [Out] out uint dpiY
    );
    [DllImport(shcore)] public static extern int GetProcessDpiAwareness(
        IntPtr hprocess, 
        out PROCESS_DPI_AWARENESS value
    );

}
