﻿
using System.Runtime.InteropServices;
using System;
using System.Drawing;

[StructLayout(LayoutKind.Sequential)]
public struct RECT
{
    public int left, top, right, bottom;
}

[StructLayout(LayoutKind.Sequential)]
public struct TRACKMOUSEEVENTS
{
    public uint cbSize;
    public uint dwFlags;
    public IntPtr hWnd;
    public uint dwHoverTime;
}

[StructLayout(LayoutKind.Sequential)]
public struct FLASHWINFO
{
    public uint cbSize;
    public IntPtr hwnd;
    public FlashWindowFlags dwFlags;
    public uint uCount;
    public uint dwTimeout;
}

[StructLayout(LayoutKind.Sequential)]
public struct POINT
{
    /// <summary>
    /// x field of structure
    /// </summary>
    public int x;

    /// <summary>
    /// y field of structure
    /// </summary>
    public int y;

    #region Constructors
    public POINT(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public POINT(Point point)
    {
        x = point.X;
        y = point.Y;
    }

    public override string ToString()
    {
        return $"x:{x} y:{y}";
    }
    #endregion
}

[StructLayout(LayoutKind.Sequential)]
public struct NCCALCSIZE_PARAMS
{
    /// <summary>
    /// Contains the new coordinates of a window that has been moved or resized, that is, it is the proposed new window coordinates.
    /// </summary>
    public RECT rectProposed;
    /// <summary>
    /// Contains the coordinates of the window before it was moved or resized.
    /// </summary>
    public RECT rectBeforeMove;
    /// <summary>
    /// Contains the coordinates of the window's client area before the window was moved or resized.
    /// </summary>
    public RECT rectClientBeforeMove;
    /// <summary>
    /// Pointer to a WINDOWPOS structure that contains the size and position values specified in the operation that moved or resized the window.
    /// </summary>
    public IntPtr lppos;

    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
    public RECT[] rgrc;
}


[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct BLENDFUNCTION
{
    /// <summary>
    /// BlendOp field of structure
    /// </summary>
    public byte BlendOp;

    /// <summary>
    /// BlendFlags field of structure
    /// </summary>
    public byte BlendFlags;

    /// <summary>
    /// SourceConstantAlpha field of structure
    /// </summary>
    public byte SourceConstantAlpha;

    /// <summary>
    /// AlphaFormat field of structure
    /// </summary>
    public byte AlphaFormat;
}

[StructLayout(LayoutKind.Sequential)]
public struct SIZE
{
    /// <summary>
    /// cx field of structure
    /// </summary>
    public int cx;

    /// <summary>
    /// cy field of structure
    /// </summary>
    public int cy;

    public SIZE(Int32 cx, Int32 cy)
    {
        this.cx = cx;
        this.cy = cy;
    }
}

[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
public struct WNDCLASS
{
    public uint style;
    public IntPtr lpfnWndProc;
    public int cbClsExtra;
    public int cbWndExtra;
    public IntPtr hInstance;
    public IntPtr hIcon;
    public IntPtr hCursor;
    public IntPtr hbrBackground;
    [MarshalAs(UnmanagedType.LPWStr)]
    public string lpszMenuName;
    [MarshalAs(UnmanagedType.LPWStr)]
    public string lpszClassName;
}