﻿using System;
using System.Runtime.InteropServices;
using static Win32Helpers;


public class User32
{
    const string user32 = "user32.dll";


    [DllImport(user32, CallingConvention = CallingConvention.Cdecl)] [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool FlashWindowEx(
        ref FLASHWINFO pwfi
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern int ShowWindow(
        IntPtr hWnd,
        ShowWindowStyles cmdShow
    );
    [DllImport(user32)] public static extern bool ShowWindow(
        IntPtr hWnd,
        int nCmdShow
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern bool ShowWindow(
        int hWnd, 
        int nCmdShow
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern int ShowWindow(
        IntPtr hWnd,
        short cmdShow
    );
    [DllImport(user32)] public static extern void DisableProcessWindowsGhosting();
    [DllImport(user32)] public static extern IntPtr MonitorFromWindow(
        IntPtr hWnd, 
        uint dwFlags
    );
    [DllImport(user32)] public static extern bool GetClientRect(
        IntPtr hWnd, 
        ref RECT lpRect
    );
    [DllImport(user32)] public static extern IntPtr GetDCEx(
        IntPtr hwnd, 
        IntPtr hrgnclip, 
        int fdwOptions
    );

    [DllImport(user32)] public static extern IntPtr GetWindow(
        IntPtr hWnd, 
        uint wCmd
    );
    [DllImport("user32.dll")] public static extern IntPtr GetWindowDC(
        IntPtr hwnd
    );
    [DllImport(user32)] public static extern int SetWindowRgn(
        IntPtr hWnd, 
        IntPtr hRgn, 
        bool bRedraw
    );
    [DllImport(user32)] public static extern int SetParent(
        int hWndChild, 
        int hWndParent
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern IntPtr GetDC(
        IntPtr hWnd
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern void AdjustWindowRectEx(
        ref RECT rect, 
        int dwStyle, 
        bool hasMenu, 
        int dwExSytle
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public extern static bool SetWindowPos(
        int hWnd, 
        IntPtr 
        hWndInsertAfter, 
        int x, 
        int y, 
        int cx, 
        int cy, 
        uint uFlags
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern int SetWindowPos(
        IntPtr hWnd, 
        IntPtr hWndAfter, 
        int x, 
        int y, 
        int width, 
        int height, 
        SetWindowPosFlags flags
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern bool GetWindowRect(
        IntPtr hWnd, 
        ref RECT rect
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern bool TrackMouseEvent(
        ref TRACKMOUSEEVENTS tme
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern int ReleaseDC(
        IntPtr hWnd, 
        IntPtr hDc
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern uint GetWindowLong(
        IntPtr hWnd,
        GetWindowLongFlags nIndex
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern int SetWindowLong(
        IntPtr hWnd, 
        GetWindowLongFlags nIndex, 
        IntPtr newLong
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern int SetWindowLong(
        IntPtr hWnd, 
        GetWindowLongFlags nIndex, 
        uint newLong
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern IntPtr LoadCursor(
        IntPtr hInstance, 
        uint cursor
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern bool ScreenToClient(
        IntPtr hWnd, 
        ref POINT pt
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern IntPtr SetCursor(
        IntPtr hCursor
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern int SetParent(
        IntPtr hWndChild, 
        IntPtr hWndNewParent
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern bool UpdateLayeredWindow(
        IntPtr hwnd, 
        IntPtr hdcDst, 
        ref POINT pptDst, 
        ref SIZE psize, 
        IntPtr hdcSrc, 
        ref POINT pprSrc, 
        Int32 crKey, 
        ref BLENDFUNCTION pblend, 
        Int32 dwFlags
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern bool ClientToScreen(
        IntPtr hWnd, 
        ref POINT lpPoint
    );
    [DllImport(user32, CharSet = CharSet.Auto)] public static extern IntPtr SendMessage(
        IntPtr hWnd, 
        uint msg, 
        IntPtr wParam, 
        IntPtr lParam
    );

    [DllImport(user32, SetLastError = true)] public static extern IntPtr CreateWindowExW(
        UInt32 dwExStyle,
        [MarshalAs(UnmanagedType.LPWStr)] string lpClassName,
        [MarshalAs(UnmanagedType.LPWStr)] string lpWindowName,
        UInt32 dwStyle,
        Int32 x,
        Int32 y,
        Int32 nWidth,
        Int32 nHeight,
        IntPtr hWndParent,
        IntPtr hMenu,
        IntPtr hInstance,
        IntPtr lpParam
    );
    [DllImport(user32, SetLastError = true)] public static extern int CloseWindow(
        IntPtr hWnd
    );
    [DllImport(user32, SetLastError = true)] public static extern UInt16 RegisterClassW(
        [In] ref WNDCLASS lpWndClass
    );
    [DllImport(user32, SetLastError = true)] public static extern IntPtr DefWindowProcW(
        IntPtr hWnd, 
        uint msg, 
        IntPtr wParam, 
        IntPtr lParam
    );
    [DllImport(user32, SetLastError = true)] public static extern bool PostMessage(
        IntPtr hWnd, 
        uint Msg, 
        IntPtr wParam,
        IntPtr lParam
    );

    [DllImport(user32)][return: MarshalAs(UnmanagedType.Bool)] public static extern bool RedrawWindow(
        IntPtr hWnd, 
        IntPtr lprcUpdate, 
        IntPtr hrgnUpdate, 
        uint flags
    );
    [DllImport(user32)] [return: MarshalAs(UnmanagedType.Bool)] public static extern bool DestroyWindow(
        IntPtr hWnd
    );

    [DllImport(user32, SetLastError = true, CharSet = CharSet.Auto)] public static extern int GetCursorPos(
        ref POINT lpPoint
    );
    [DllImport(user32, SetLastError = true, CharSet = CharSet.Auto)] public static extern int GetClassName(
        IntPtr hWnd, 
        System.Text.StringBuilder lpClassName, 
        int nMaxCount
    );
    [DllImport(user32)] public static extern int GetAwarenessFromDpiAwarenessContext(
        IntPtr dpiContext
    );
    [DllImport(user32)] public static extern IntPtr GetWindowDpiAwarenessContext(
        IntPtr hWnd
    );
    [DllImport(user32)] public static extern IntPtr MonitorFromPoint(
        [In] System.Drawing.Point pt, 
        [In] uint dwFlags
    );
    [DllImport(user32)] public static extern int SendMessage(
        IntPtr hWnd, 
        int Msg, 
        int wParam, 
        int lParam
    );
    [DllImport("user32.dll")] public static extern bool InvalidateRect(
        IntPtr hWnd, 
        RECT lpRect, 
        bool bErase
    );
    [DllImport(user32)] public static extern bool ReleaseCapture();
    //[DllImport(user32, SetLastError = true)] [return: MarshalAs(UnmanagedType.Bool)] public static extern bool SystemParametersInfo(uint uiAction,
    //    uint uiParam,
    //    ref ANIMATIONINFO pvParam,
    //    uint fWinIni
    //);
}
