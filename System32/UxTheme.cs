﻿using System;
using System.Runtime.InteropServices;

public class UxTheme
{
    const string uxtheme = "uxtheme.dll";


    [DllImport(uxtheme, ExactSpelling = true, CharSet = CharSet.Unicode)] public static extern int SetWindowTheme(
        IntPtr hWnd, 
        String pszSubAppName, 
        String pszSubIdList
    );
}
