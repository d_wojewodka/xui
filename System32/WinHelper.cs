﻿
using System;
using System.Windows.Forms;

public class WinHelper
{
    internal delegate IntPtr WndProcHandler(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

    public static readonly IntPtr TRUE = new IntPtr(1);
    public static readonly IntPtr FALSE = new IntPtr(0);

    public static readonly IntPtr MESSAGE_PROCESS = new IntPtr(0);
    public static readonly IntPtr MESSAGE_HANDLED = new IntPtr(1);

    public static int CornerAreaSize = SystemInformation.FrameBorderSize.Width / 2;





    public static int GetOriginalDeviceDpi(IntPtr hWnd)
    {
        var hMonitor = User32.MonitorFromWindow(hWnd, (uint)MonitorFromWindowFlags.MONITOR_DEFAULTTONEAREST);

        //这句不能正确的检测Win8和8.1
        //if ((System.Environment.OSVersion.Version.Major >= 8 && System.Environment.OSVersion.Version.Minor >= 1) || System.Environment.OSVersion.Version.Major > 8)
        try
        {
            //GetDpiForMonitor(hMonitor, MonitorDpiType.MDT_DEFAULT, out int x, out int y);
            ShCore.GetDpiForMonitor(hMonitor, MonitorDpiType.MDT_DEFAULT, out int x, out int y);
            return x;
        }
        catch
        {
            return 96;
        }
    }
    public static void SendFrameChanged(IntPtr hWnd)
    {
        User32.SetWindowPos(hWnd, IntPtr.Zero, 0, 0, 0, 0,
            SetWindowPosFlags.SWP_FRAMECHANGED | SetWindowPosFlags.SWP_NOACTIVATE | SetWindowPosFlags.SWP_NOCOPYBITS |
            SetWindowPosFlags.SWP_NOMOVE | SetWindowPosFlags.SWP_NOOWNERZORDER | SetWindowPosFlags.SWP_NOREPOSITION |
            SetWindowPosFlags.SWP_NOSENDCHANGING | SetWindowPosFlags.SWP_NOSIZE | SetWindowPosFlags.SWP_NOZORDER);
    }
    public static HitTest GetSizeMode(POINT point, int width, int height)
    {
        HitTest mode = HitTest.HTCLIENT;

        int x = point.x, y = point.y;

        if (x < CornerAreaSize & y < CornerAreaSize)
            mode = HitTest.HTTOPLEFT;
        else if (x < CornerAreaSize & y + CornerAreaSize > height - CornerAreaSize)
            mode = HitTest.HTBOTTOMLEFT;
        else if (x + CornerAreaSize > width - CornerAreaSize & y + CornerAreaSize > height - CornerAreaSize)
            mode = HitTest.HTBOTTOMRIGHT;
        else if (x + CornerAreaSize > width - CornerAreaSize & y < CornerAreaSize)
            mode = HitTest.HTTOPRIGHT;
        else if (x < CornerAreaSize)
            mode = HitTest.HTLEFT;
        else if (x + CornerAreaSize > width - CornerAreaSize)
            mode = HitTest.HTRIGHT;
        else if (y < CornerAreaSize)
            mode = HitTest.HTTOP;
        else if (y + CornerAreaSize > height - CornerAreaSize)
            mode = HitTest.HTBOTTOM;

        return mode;
    }

    internal static POINT GetPostionFromPtr(IntPtr lparam)
    {
        var scaledX = (int)LoWord(lparam);
        var scaledY = (int)HiWord(lparam);

        var x = scaledX;
        var y = scaledY;

        return new POINT(x, y);
    }

    public static uint HiWord(IntPtr ptr)
    {
        if (((uint)ptr & 0x80000000) == 0x80000000)
            return ((uint)ptr >> 16);

        return ((uint)ptr >> 16) & 0xffff;
    }
    public static uint HIWORD(IntPtr ptr)
    {
        return HiWord(ptr);
    }
    public static uint LoWord(IntPtr ptr)
    {
        return (uint)(ptr.ToInt32() & 0xFFFF);
    }
    public static uint LOWORD(IntPtr ptr)
    {
        return LoWord(ptr);
    }
}

