﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

public enum TextAlign
{
    Left,
    Center,
    Right
}

public class xToolTip : ToolTip
{
    public xToolTip()
    {
        this.OwnerDraw = true;
        this.Popup += new PopupEventHandler(OnPopup);
        this.Draw += new DrawToolTipEventHandler(OnDraw);

        // Base
        this.Padding = new Padding(4);
        this.TextAlign = TextAlign.Left;
        this.TextShadowColor = Color.Gray;
        this.BorderColor = Color.Black;

        if (this.Font == null)
            this.Font = SystemFonts.DefaultFont;

        // Title
        this.TitleFont = new Font(this.Font.FontFamily, this.Font.Size + 1, this.Font.Style);
        this.TitleForeColor = Color.DarkGreen;
        this.TitleAlign = TextAlign.Left;
        this.TitleTextSpace = 2;
    }

    // Title cache
    private Dictionary<Control, string> _controlTitles = new Dictionary<Control, string>();

    // Base
    private Font _font = null;
    private Padding _padding = new Padding(4);
    private TextAlign _textAlign = TextAlign.Left;
    private Color _fillColor = Color.White;
    private Color _textShadowColor = Color.Gray;
    private Color _borderColor = Color.Black;
    private int _borderThickness = 1;
    #region Public properties
    public Font Font
    {
        get => _font;
        set => _font = value;
    }
    public Padding Padding
    {
        get => _padding;
        set => _padding = value;
    }

    [Description("Alignment of text")] [DefaultValue(typeof(TextAlign), "Left")]
    public TextAlign TextAlign
    {
        get => _textAlign;
        set => _textAlign = value;
    }

    [Description("Background color")] [DefaultValue(typeof(Color), "White")]
    public Color FillColor
    {
        get => _fillColor;
        set => _fillColor = value;
    }

    [Description("Text shadow color")] [DefaultValue(typeof(Color), "Gray")]
    public Color TextShadowColor
    {
        get => _textShadowColor;
        set => _textShadowColor = value;
    }

    [Description("Border color")] [DefaultValue(typeof(Color), "Black")]
    public Color BorderColor
    {
        get => _borderColor;
        set => _borderColor = value;
    }

    [Description("Thickness of border")] [DefaultValue(1)]
    public int BorderThickness
    {
        get => _borderThickness;
        set => _borderThickness = value;
    }
    #endregion

    // Title
    private Font _titleFont = null;
    private Color _titleForeColor = Color.Black;
    private Color _titleShadowColor = Color.Gray;
    private TextAlign _titleAlign = TextAlign.Left;
    private int _titleTextSpace = 2;
    #region Public properties of title
    [Description("The font used for the tooltip title")]
    public Font TitleFont
    {
        get => _titleFont;
        set => _titleFont = value;
    }

    [Description("Color of tooltip text")] [DefaultValue(typeof(Color), "Black")]
    public Color TitleForeColor
    {
        get => _titleForeColor;
        set => _titleForeColor = value;
    }

    [Description("Title shadow color")] [DefaultValue(typeof(Color), "Gray")]
    public Color TitleShadowColor
    {
        get => _titleShadowColor;
        set => _titleShadowColor = value;
    }

    [Description("Alignment of tooltip text")] [DefaultValue(typeof(TextAlign), "Left")]
    public TextAlign TitleAlign
    {
        get => _titleAlign;
        set => _titleAlign = value;
    }

    [Description("Space between tooltip title and text")] [DefaultValue(2)]
    public int TitleTextSpace
    {
        get => _titleTextSpace;
        set => _titleTextSpace = value;
    }
    #endregion

    public void SetToolTip(Control control, string text, string title = "")
    {
        base.SetToolTip(control, text);
        _controlTitles[control] = title;
    }

    private TextFormatFlags GetTextFormatFlags(TextAlign align)
    {
        TextFormatFlags flags = TextFormatFlags.VerticalCenter;

        switch (align)
        {
            case TextAlign.Left:
                flags |= TextFormatFlags.Left;
                break;
            case TextAlign.Center:
                flags |= TextFormatFlags.HorizontalCenter;
                break;
            case TextAlign.Right:
                flags |= TextFormatFlags.Right;
                break;
        }

        return flags;
    }
    private void OnPopup(object sender, PopupEventArgs e)
    {
        string toolTipText = this.GetToolTip(e.AssociatedControl);
        string titleText = string.Empty;

        if (_controlTitles.TryGetValue(e.AssociatedControl, out titleText))
        {
            // Use the title retrieved from the dictionary
        }

        using (Graphics g = e.AssociatedControl.CreateGraphics())
        {
            Size textSize = TextRenderer.MeasureText(g, toolTipText, this.Font, Size.Empty, GetTextFormatFlags(TextAlign));
            Size titleSize = new Size(0, 0);
            int titleTextSpace = 0;

            if (!string.IsNullOrEmpty(titleText))
            {
                titleSize = TextRenderer.MeasureText(g, titleText, this.TitleFont ?? this.Font, Size.Empty, GetTextFormatFlags(this.TitleAlign));
                titleTextSpace = this.TitleTextSpace;
            }

            int width = Math.Max(textSize.Width, titleSize.Width) + this.Padding.Horizontal + BorderThickness * 2;
            int height = textSize.Height + titleSize.Height + this.Padding.Vertical + BorderThickness * 2 + titleTextSpace;

            e.ToolTipSize = new Size(width, height);
        }
    }

    private void OnDraw(object sender, DrawToolTipEventArgs e)
    {
        // Background
        using (SolidBrush background = new SolidBrush(this.FillColor))
        {
            e.Graphics.FillRectangle(background, e.Bounds);
        }

        // Border
        if (this.BorderThickness > 0 && this.BorderColor != Color.Empty)
        {
            using (Pen border = new Pen(this.BorderColor, this.BorderThickness))
            {
                Rectangle borderBounds = new Rectangle(
                    e.Bounds.X,
                    e.Bounds.Y,
                    e.Bounds.Width - 1,
                    e.Bounds.Height - 1
                );
                e.Graphics.DrawRectangle(border, borderBounds);
            }
        }

        // Get the title from the dictionary
        string titleText;
        _controlTitles.TryGetValue(e.AssociatedControl, out titleText);

        // Title bounds
        Size titleSize = TextRenderer.MeasureText(e.Graphics, titleText, this.TitleFont ?? this.Font, Size.Empty, GetTextFormatFlags(this.TitleAlign));
        Rectangle titleBounds = new Rectangle(
            e.Bounds.X + this.Padding.Left + BorderThickness,
            e.Bounds.Y + this.Padding.Top,
            e.Bounds.Width - this.Padding.Horizontal,
            titleSize.Height
        );

        // Title Shadow
        if (TitleShadowColor != Color.Empty)
        {
            Rectangle titleShadowBounds = new Rectangle(new Point(titleBounds.X + 1, titleBounds.Y + 1), titleBounds.Size);
            TextRenderer.DrawText(e.Graphics, titleText, this.TitleFont ?? this.Font, titleShadowBounds, this.TextShadowColor, GetTextFormatFlags(this.TitleAlign));
        }

        // Title
        TextRenderer.DrawText(e.Graphics, titleText, this.TitleFont ?? this.Font, titleBounds, this.TitleForeColor, GetTextFormatFlags(this.TitleAlign));

        // Text bounds
        Size textSize = TextRenderer.MeasureText(e.Graphics, e.ToolTipText, this.Font, Size.Empty, GetTextFormatFlags(this.TextAlign));
        Rectangle textBounds;

        if (string.IsNullOrEmpty(titleText))
        {
            // No title, center the text vertically
            textBounds = new Rectangle(
                e.Bounds.X + this.Padding.Left + BorderThickness,
                e.Bounds.Y + this.Padding.Top,
                e.Bounds.Width - this.Padding.Horizontal,
                textSize.Height
            );
        }
        else
        {
            // Title is present, position the text below the title
            textBounds = new Rectangle(
                e.Bounds.X + this.Padding.Left + BorderThickness,
                e.Bounds.Y + this.Padding.Top + titleSize.Height + this.TitleTextSpace,
                e.Bounds.Width - this.Padding.Horizontal,
                textSize.Height
            );
        }

        // Text Shadow
        if (TextShadowColor != Color.Empty)
        {
            Rectangle shadowBounds = new Rectangle(new Point(textBounds.X + 1, textBounds.Y + 1), textBounds.Size);
            TextRenderer.DrawText(e.Graphics, e.ToolTipText, this.Font, shadowBounds, this.TextShadowColor, GetTextFormatFlags(this.TextAlign));
        }

        // Text
        TextRenderer.DrawText(e.Graphics, e.ToolTipText, this.Font, textBounds, this.ForeColor, GetTextFormatFlags(this.TextAlign));
    }
}
