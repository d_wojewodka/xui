﻿using System;
using System.Runtime.InteropServices;

public sealed class WindowsApi
{
    public const int EM_SETSEL = 0x00B1;
    public const int EM_SETMARGINS = 0xD3;
    public const int EC_RIGHTMARGIN = 2;
}