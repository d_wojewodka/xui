﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Windows.Forms.Design;
using System.Globalization;
using UIX.Input;

public enum InputType
{
    Default,
    Numeric,
    Decimal,
    Password,
    Date,
    Select,
    MultiSelect,
    File
}
public enum ClearVisibility
{
    Never,
    Always,
    WhenText,
}
public class InputDesigner : ControlDesigner
{
    public override void InitializeNewComponent(System.Collections.IDictionary defaultValues)
    {
        base.InitializeNewComponent(defaultValues);
        PropertyDescriptor descriptor = TypeDescriptor.GetProperties(base.Component)["Text"];

        if (((descriptor != null) && (descriptor.PropertyType == typeof(string))) && (!descriptor.IsReadOnly && descriptor.IsBrowsable))
            descriptor.SetValue(base.Component, string.Empty);
    }
}

[Designer(typeof(InputDesigner))]
public class xInput : Control, IMessageFilter
{
    #region Properties
    private InputTextBox _textBox = null;
    private PictureBox _pictureBox = null;
    private PictureBox _clearPictureBox = null;

    private ToolTip _toolTip = null;

    private ContextMenuStrip _contextMenuStrip = null;
    private ToolStripMenuItem _menuItemCut = null;
    private ToolStripMenuItem _menuItemCopy = null;
    private ToolStripMenuItem _menuItemPaste = null;
    private ToolStripSeparator _menuSeparator = null;
    private ToolStripMenuItem _menuItemClear = null;

    private ContextMenuStrip _selectContextMenuStrip = null;
    private MonthCalendar _monthCalendar = null;

    // Border & fill
    private GraphicsPath _shape = null;
    private GraphicsPath _fill = null;

    // Update image
    private bool _updateImage = false;

    // Modal
    private bool _isOpen = false;
    private DateTime _closeTime = DateTime.MinValue;

    // Select
    ToolStripMenuItem _allToolStripMenuItem = null;
    private string _previousText = null;
    private string _defaultMultiSelectDelmiter = ", ";
    private string _defaultDateFormat = "yyyy-MM-dd";
    private char _defaultDecimalDelimiter = ',';
    #endregion

    #region Designer properties
    private InputType _type = InputType.Default;
    private int _borderRadius = 5;
    private bool _borderShadows = true;
    private Color _backgroundColor = Color.White;
    private Color _borderColor = Color.Gray;
    private Color _borderHoverColor = Color.LightSkyBlue;
    private Color _borderFocusColor = Color.DodgerBlue;
    private Color _borderDefaultColor = Color.Gray;
    private Color _imageColor = Color.Black;
    private Image _imageFront = null;
    private Image _imageBack = null;
    private bool _useImage = true;
    private bool _textArea = false;
    private bool _readOnly = false;
    private bool _editable = true;
    private Collection<string> _options = new Collection<string>();
    private string _multiSelectDelimiter = ", ";
    private string _dateFormat = "yyyy-MM-dd";
    private string _textExtra = string.Empty;
    private string _currency = string.Empty;
    private char _decimalDelimiter = ',';
    private string _placeholderText = string.Empty;
    private Color _placeholderColor = Color.Gray;

    private ScrollBars _scrollBars = ScrollBars.None;

    private DateTime _dateMin = DateTime.MinValue;
    private DateTime _dateMax = DateTime.MaxValue;

    private Image _clearImage = null;
    private Color _clearImageColor = Color.Crimson;
    private ClearVisibility _clearVisibility = ClearVisibility.WhenText;

    private string _tooltipImageText = string.Empty;
    private string _tooltipClearImageText = string.Empty;
    #endregion

    #region Properties assignment
    [Category("Appearance"), Description("Default control background, should be Color.Transparent in order to properly use BackgroundColor fill.")]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public override Color BackColor
    {
        get => base.BackColor;
        set => base.BackColor = Color.Transparent;
    }

    [Category("Appearance"), Description("Default control background, should be Color.Transparent in order to properly use BackgroundColor fill.")]
    [DefaultValue(typeof(Color), "Black")]
    public override Color ForeColor
    {
        get => base.ForeColor;
        set
        {
            base.ForeColor = value;
            this._textBox.ForeColor = value;
            base.Invalidate();
        }
    }

    [Category("Appearance"), Description("Color that is filled inside border.")]
    [DefaultValue(typeof(Color), "White")]
    public Color BackgroundColor
    {
        get => _backgroundColor;
        set
        {
            this._backgroundColor = value;

            if (this._backgroundColor != Color.Transparent)
            {
                this._textBox.BackColor = value;

                if (this._pictureBox != null)
                    this._pictureBox.BackColor = value;
                if (this._clearPictureBox != null)
                    this._clearPictureBox.BackColor = value;
            }

            base.Invalidate();
        }
    }


    [Category("Border"), Description("Radius of the border, if set to 0 then is not rouneded at all.")]
    [DefaultValue(5)]
    public int BorderRadius
    {
        get => _borderRadius;
        set
        {
            //if (DesignMode)
            //{
            //    if (value > (base.Height / 2 - (base.Height % 2)) - (_borderShadows ? 2 : 0))
            //        _borderRadius = (base.Height / 2 - (base.Height % 2) - (_borderShadows ? 2 : 0));
            //    else
            //        _borderRadius = value;
            //}
            //else
            //    _borderRadius = value;

            _borderRadius = value;
            base.Invalidate();
        }
    }


    [Category("Border"), Description("Draw additional border shadows")]
    [DefaultValue(true)]
    public bool BorderShadows
    {
        get => _borderShadows;
        set
        {
            _borderShadows = value;
            base.Invalidate();
        }
    }

    [Category("Border"), Description("Visible border color, if you wanna use validation then assign color to this property. It will be overrided when text changes and goes back to normal.")]
    [DefaultValue(typeof(Color), "Gray")]
    public Color BorderColor
    {
        get => _borderColor;
        set
        {
            _borderColor = value;
            base.Invalidate();
        }
    }

    [Category("Border"), Description("Default color of border.")]
    [DefaultValue(typeof(Color), "Gray")]
    public Color BorderDefaultColor
    {
        get => _borderDefaultColor;
        set
        {
            _borderDefaultColor = value;
            BorderColor = value;
            base.Invalidate();
        }
    }

    [Category("Border"), Description("Color of border when user hovers over it.")]
    [DefaultValue(typeof(Color), "LightSkyBlue")]
    public Color BorderHoverColor
    {
        get => _borderHoverColor;
        set
        {
            _borderHoverColor = value;
            base.Invalidate();
        }
    }

    [Category("Border"), Description("Color of border when user clicked inside control.")]
    [DefaultValue(typeof(Color), "DodgerBlue")]
    public Color BorderFocusColor
    {
        get => _borderFocusColor;
        set
        {
            _borderFocusColor = value;
            base.Invalidate();
        }
    }


    [Category("Behaviour"), Description("Multiline support for all types except password.")]
    [DefaultValue(false)]
    public bool TextArea
    {
        get => _textArea;
        set
        {
            if (_type == InputType.Password)
            {
                _textArea = false;
                this._textBox.Multiline = false;
            }
            else
            {
                _textArea = value;
                this._textBox.Multiline = value;
            }

            base.Invalidate();
        }
    }

    [Category("Behaviour"), Description("Basic readonly, if set to true you can only view, select and copy value. Functions does not work when true, otherwise it works.")]
    [DefaultValue(false)]
    public bool ReadOnly
    {
        get => _readOnly;
        set
        {
            _readOnly = value;
            _updateImage = true;
            this._textBox.ReadOnly = value;

            if (!value && Type == InputType.Default && this._pictureBox != null && ImageFront == null)
            {
                this._pictureBox.Image = null;
                this._pictureBox.Visible = false;
            }

            if (Type == InputType.Decimal && !string.IsNullOrEmpty(Currency))
            {
                if (value && !base.Text.EndsWith(Currency) && base.Text.Length > 0)
                    base.Text += Currency;
                else if (!value && base.Text.EndsWith(Currency))
                    base.Text = base.Text.Substring(0, base.Text.Length - Currency.Length);
            }

            base.Invalidate();
        }
    }

    public override string Text
    {
        get => base.Text;
        set {
            if (Type == InputType.Decimal && ReadOnly && !string.IsNullOrEmpty(Currency) && !base.Text.EndsWith(Currency) && !string.IsNullOrEmpty(base.Text) && base.Text.Length > 0)
                base.Text = value + Currency;
            else
                base.Text = value;
        }
    }


    [Category("Behaviour"), Description("Indicates that user can edit content. ReadOnly overrides that")]
    [DefaultValue(true)]
    public bool Editable
    {
        get => _editable;
        set
        {
            if (Type == InputType.File)
                _editable = false;
            else
                _editable = value;

            base.Invalidate();
        }
    }
    [Category("Behaviour"), Description("Additional text value for control, it will be invisible.")]
    [DefaultValue("")]
    public string TextExtra
    {
        get => _textExtra;
        set { 
            _textExtra = value;
            _updateImage = true;
            TextExtraChange?.Invoke(this, null);
        }
    }

    [Category("Behaviour"), Description("Scrollbar")]
    [DefaultValue(ScrollBars.None)]
    public ScrollBars Scrollbars
    {
        get => _scrollBars;
        set { 
            _scrollBars = value;
            this._textBox.ScrollBars = value;
            base.Invalidate();
        }
    }

    [Category("Behaviour"), Browsable(true), Description("The type of input.")]
    [DefaultValue(InputType.Default)]
    public InputType Type
    {
        get => _type;
        set
        {
            if (_type != value)
            {
                _type = value;

                switch (value)
                {
                    case InputType.Select:
                    case InputType.MultiSelect:
                    case InputType.File:
                    case InputType.Date:
                        Editable = false;
                        break;
                }

                _updateImage = true;

                if (value == InputType.Password)
                {
                    TextArea = false;
                    this._textBox.UseSystemPasswordChar = true;
                    ClearVisibility = ClearVisibility.Never;
                }
                else
                {
                    this._textBox.UseSystemPasswordChar = false;
                    _previousText = null;
                }

                base.Invalidate();
            }
        }
    }

    [Category("Images"), Description("Color of image.")]
    [DefaultValue(typeof(Color), "Black")]
    public Color ImageColor
    {
        get => _imageColor;
        set
        {
            _imageColor = value;
            _updateImage = true;
            base.Invalidate();
        }
    }

    [Category("Images"), Browsable(true), Description("Image that is displayed.")]
    [DefaultValue(null)]
    public Image ImageFront
    {
        get => _imageFront;
        set
        {
            _imageFront = value;
            _updateImage = true;

            if (value == null && Type == InputType.Default)
                this._pictureBox.Image = null;

            base.Invalidate();
        }
    }

    [Category("Images"), Browsable(true), Description("Image that will be displayed as alternative for some types.")]
    [DefaultValue(null)]
    public Image ImageBack
    {
        get
        {
            return _imageBack;
        }
        set
        {
            _imageBack = value;
            base.Invalidate();
        }
    }
    [Category("Behaviour"), Description("Indicates that user can edit content. ReadOnly overrides that")]
    [DefaultValue(true)]
    public bool UseImage
    {
        get => _useImage;
        set
        {
            _useImage = value;
            _updateImage = true;
            base.Invalidate();
        }
    }


    [Category("Data"), Description("Values in select, it represents Items for ComboBox.")]
    [DefaultValue(null)]
    public Collection<string> Options
    {
        get => _options;
        set {
            if (!ReferenceEquals(_options, value))
            {
                _options = value;
                base.Invalidate();
            }
        }
    }


    [Category("Format"), Description("Symbol that is used to split items when control has Multiselect type.")]
    [DefaultValue(", ")]
    public string MultiSelectDelimiter
    {
        get => _multiSelectDelimiter;
        set => _multiSelectDelimiter = value;
    }

    [Category("Format"), Description("Date format when type is set to Date.")]
    [DefaultValue("yyyy-MM-dd")]
    public string DateFormat
    {
        get => _dateFormat;
        set => _dateFormat = value;
    }

    [Category("Format"), Description("Minimum date.")]
    //[DefaultValue(typeof(DateTime), "MinValue")]
    public DateTime DateMin
    {
        get => _dateMin;
        set
        {
            if (_type == InputType.Date && value != DateTime.MinValue)
            {
                _dateMin = value;
                base.Invalidate();
            }
        }
    }

    [Category("Format"), Description("Maximum date.")]
    //[DefaultValue(typeof(DateTime), "MaxValue")]
    public DateTime DateMax
    {
        get => _dateMax;
        set
        {
            if (_type == InputType.Date && value != DateTime.MinValue)
            {
                _dateMax = value;
                base.Invalidate();
            }
        }
    }

    [Category("Format"), Description("Symbol that is used to split decimal values.")]
    [DefaultValue(',')]
    public char DecimalDelimiter
    {
        get => _decimalDelimiter;
        set => _decimalDelimiter = value;
    }

    [Category("Behaviour"), Description("Text that is displayed when Text is empty.")]
    [DefaultValue("")]
    public string PlaceholderText
    {
        get => _placeholderText;
        set { 
            _placeholderText = value;
            this._textBox.Placeholder = value;
            base.Invalidate();
        }
    }

    [Category("Behaviour"), Description("Color of placeholder text.")]
    [DefaultValue(typeof(Color), "Gray")]
    public Color PlaceholderColor
    {
        get => _placeholderColor;
        set
        {
            _placeholderColor = value;
            this._textBox.PlaceholderColor = value;
            base.Invalidate();
        }
    }


    [Category("Behaviour"), Description("Clear button image.")]
    [DefaultValue(null)]
    public Image ClearImage
    {
        get => _clearImage;
        set
        {
            _clearImage = value;
            _updateImage = true;
            base.Invalidate();
        }
    }

    [Category("Behaviour"), Description("Color of clear image.")]
    [DefaultValue(typeof(Color), "Crimson")]
    public Color ClearImageColor
    {
        get => _clearImageColor;
        set
        {
            _clearImageColor = value;
            _updateImage = true;
            base.Invalidate();
        }
    }
    [Category("Behaviour"), Description("Is clear button visible.")]
    [DefaultValue(ClearVisibility.WhenText)]
    public ClearVisibility ClearVisibility
    {
        get => _clearVisibility;
        set
        {
            if (Type != InputType.Password || value == ClearVisibility.Never)
                _clearVisibility = value;
            else
                _clearVisibility = ClearVisibility.Never;

            _updateImage = true;
            base.Invalidate();
        }
    }

    [Category("Behaviour"), Description("Text tooltip of image.")]
    [DefaultValue("")]
    public string ToolTipImageText
    {
        get => _tooltipImageText;
        set
        {
            _tooltipImageText = value;
            base.Invalidate();
        }
    }

    [Category("Behaviour"), Description("Text tooltip of clear image.")]
    [DefaultValue("")]
    public string ToolTipClearImageText
    {
        get => _tooltipClearImageText;
        set
        {
            _tooltipClearImageText = value;
            base.Invalidate();
        }
    }

    [Category("Behaviour"), Description("Shows currency addition when control is ReadOnly.")]
    [DefaultValue("")]
    public string Currency
    {
        get => _currency;
        set
        {
            string currentCurrency = _currency;
            _currency = value;

            if (!string.IsNullOrEmpty(currentCurrency))
                Text = Text.Replace(currentCurrency, string.Empty);

            base.Invalidate();
        }
    }
    #endregion

    #region Event handlers
    private bool _disableImageClick = false;
    [Category("Border"), Description("Disable default action")]
    [DefaultValue(false)]
    public bool DisableImageClick
    {
        get => _disableImageClick;
        set => _disableImageClick = value;
    }
    public event EventHandler ImageClick;
    public event EventHandler ClearImageClick;
    public event EventHandler TextExtraChange;
    protected virtual void OnImageClick(EventArgs e)
    {
        ImageClick?.Invoke(this, e);
    }
    protected virtual void OnClearImageClick(EventArgs e)
    {
        ClearImageClick?.Invoke(this, e);
    }
    protected virtual void OnTextExtraChange(EventArgs e)
    {
        TextExtraChange?.Invoke(this, e);
    }
    #endregion

    public xInput() : base()
    {
        base.SetStyle(ControlStyles.UserPaint, true);
        base.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        base.SetStyle(ControlStyles.ResizeRedraw, true);
        base.SetStyle(ControlStyles.DoubleBuffer, true);
        base.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
        UpdateStyles();

        this._textBox = new InputTextBox();
        this._textBox.Parent = this;
        this.Controls.Add(_textBox);
        this._textBox.BorderStyle = BorderStyle.None;
        this._textBox.Font = this.Font;
        this._textBox.ShortcutsEnabled = false;
        this.BackColor = Color.Transparent;
        this.ForeColor = Color.Black;
        this._textBox.BackColor = _backgroundColor;
        this.Size = new Size(200, 25);
        this._textBox.Width = 1000;
        this.DoubleBuffered = true;

        Editable = true;

        // TextBox events
        this._textBox.PreviewKeyDown += textBox_PreviewKeyDown;
        this._textBox.KeyDown += textBox_KeyDown;
        this._textBox.KeyPress += textBox_KeyPress;
        this._textBox.TextChanged += textBox_TextChanged;
        this._textBox.MouseDoubleClick += textBox_MouseDoubleClick;
        this._textBox.MouseDown += textBox_MouseDown;
        this._textBox.GotFocus += textBox_GotFocus;
        this._textBox.LostFocus += textBox_LostFocus;

        _updateImage = true;

        // Create a new context menu strip
        _contextMenuStrip = new ContextMenuStrip();

        // Add items to the context menu strip
        _menuItemCopy = new ToolStripMenuItem("Kopiuj");
        _menuItemCopy.Click += (sender, e) => Copy();
        _contextMenuStrip.Items.Add(_menuItemCopy);

        _menuItemCut = new ToolStripMenuItem("Wytnij");
        _menuItemCut.Click += (sender, e) => Cut();
        _contextMenuStrip.Items.Add(_menuItemCut);

        _menuItemPaste = new ToolStripMenuItem("Wklej");
        _menuItemPaste.Click += (sender, e) => Paste();
        _contextMenuStrip.Items.Add(_menuItemPaste);

        _menuSeparator = new ToolStripSeparator();
        _contextMenuStrip.Items.Add(_menuSeparator);

        _menuItemClear = new ToolStripMenuItem("Wyczyść");
        _menuItemClear.Click += (sender, e) => Clear();
        _contextMenuStrip.Items.Add(_menuItemClear);

    }

    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            _textBox?.Dispose();
            _pictureBox?.Dispose();
            _clearPictureBox?.Dispose();

            _allToolStripMenuItem?.Dispose();
            _selectContextMenuStrip?.Dispose();

            _toolTip?.Dispose();

            _monthCalendar?.Dispose();

            _contextMenuStrip?.Dispose();
            _menuItemCut?.Dispose();
            _menuItemCopy?.Dispose();
            _menuItemPaste?.Dispose();
            _menuItemClear?.Dispose();

            _shape?.Dispose();
            _fill?.Dispose();

            _options = null;

            Application.RemoveMessageFilter(this);
        }

        base.Dispose(disposing);
    }
    public bool PreFilterMessage(ref Message m)
    {
        const int WM_LBUTTONDOWN = 0x201;

        if (m.Msg == WM_LBUTTONDOWN)
        {
            Point clickLocation = Control.MousePosition;

            // Convert screen coordinates to client coordinates
            Point controlClickLocation = this.PointToClient(clickLocation);
            Point calendarClickLocation = _monthCalendar != null ? _monthCalendar.PointToClient(clickLocation) : Point.Empty;

            // Check if the click occurred outside both the control and the MonthCalendar
            if (!this.ClientRectangle.Contains(controlClickLocation) &&
                (_monthCalendar == null || !_monthCalendar.ClientRectangle.Contains(calendarClickLocation)))
            {
                // Close the MonthCalendar if it's visible
                if (_monthCalendar != null && _monthCalendar.Visible)
                {
                    ToggleMonthCalendar();
                    base.Invalidate();
                }
            }
        }

        return false; // Allow the message to continue to the next filter or control
    }

    #region Control events and methods
    protected override void OnPaint(PaintEventArgs e)
    {
        // Proper position and sizes
        UpdateImage();
        CalculateLocationAndSize();

        e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
        Bitmap bitmap = new Bitmap(base.Width, base.Height);

        using (Graphics g = Graphics.FromImage(bitmap))
        {
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            e.Graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.PixelOffsetMode = PixelOffsetMode.HighSpeed;
            e.Graphics.CompositingQuality = CompositingQuality.HighQuality;

            Color borderColor;
            bool drawBorderShadow = false;

            if (!ReadOnly && (this._textBox.ContainsFocus || _isOpen))
                borderColor = BorderFocusColor;
            else if (!ReadOnly && (this.ClientRectangle.Contains(this.PointToClient(Cursor.Position))))
                borderColor = BorderHoverColor;
            else
                borderColor = BorderColor;


            if (borderColor != BorderDefaultColor)
                drawBorderShadow = true;

            // Shadows
            Matrix _Matrix = new Matrix();
            int borderShadows = BorderShadows ? 2 : 0;

            if (BorderShadows && drawBorderShadow)
            {
                GraphicsPath shadows = new RoundedRectangle((float)base.Width, (float)base.Height, (float)this.BorderRadius + borderShadows, 0.5f, 0.5f).Path;
                FuzzyShadows(shadows, e.Graphics, borderColor, 222, 2, 1);
            }

            // Border shape
            this._shape = new RoundedRectangle((float)base.Width - borderShadows, (float)base.Height - borderShadows, (float)this.BorderRadius, 0f, 0f).Path;
            if (BorderShadows) 
            {
                _Matrix.Translate((float)borderShadows / 2, (float)borderShadows / 2);
                _shape.Transform(_Matrix);
            }

            e.Graphics.DrawPath(new Pen(borderColor), this._shape);

            using (SolidBrush brush = new SolidBrush(this._backgroundColor))
            {
                // Fill area
                float fillMargin = BorderRadius > 0 ? 0.5f : 1f;
                this._fill = new RoundedRectangle((float)(base.Width - fillMargin - borderShadows), (float)(base.Height - fillMargin - borderShadows), (float)this.BorderRadius, 0.5f, 0.5f).Path;
                _fill.Transform(_Matrix);

                e.Graphics.FillPath((Brush)brush, this._fill);
            }

            Transparency.MakeTransparent(this, e.Graphics);
        }

        base.OnPaint(e);
    }
    protected override void OnTextChanged(EventArgs e)
    {
        base.OnTextChanged(e);

        this._textBox.Text = this.Text;
        _borderColor = _borderDefaultColor;

        if (ClearVisibility != ClearVisibility.Never && _clearPictureBox == null)
            _updateImage = true;

        if (this._clearPictureBox != null)
        {
            if (ClearVisibility == ClearVisibility.Always || (ClearVisibility == ClearVisibility.WhenText && !string.IsNullOrEmpty(Text)) && !ReadOnly)
                this._clearPictureBox.Visible = true;
            else
                this._clearPictureBox.Visible = false;
        }

        base.Invalidate();
    }
    protected override void OnFontChanged(EventArgs e)
    {
        base.OnFontChanged(e);

        this._textBox.Font = this.Font;
        _updateImage = true;

        base.Invalidate();
    }
    protected override void OnEnter(EventArgs e)
    {
        base.OnEnter(e);
        base.Invalidate();
    }
    protected override void OnLeave(EventArgs e)
    {
        base.OnLeave(e);
        base.Invalidate();
    }
    protected override void OnMouseEnter(EventArgs e)
    {
        base.OnMouseEnter(e);
        base.Invalidate();
    }
    protected override void OnMouseLeave(EventArgs e)
    {
        base.OnMouseLeave(e);
        base.Invalidate();
    }
    protected override void OnGotFocus(EventArgs e)
    {
        base.OnGotFocus(e);
        this._textBox.Focus();
    }
    protected override void OnClick(EventArgs e)
    {
        base.OnClick(e);
        textBox_MouseDown(this._textBox, null);
    }
    #endregion

    #region TextBox events
    private void textBox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
        //Console.WriteLine($"PreviewKeyDown -> {e.Modifiers}+{e.KeyCode}");

        // Ctrl + Alt reprezentuje sam alt - nie wiem czemu, więc przesyłamy dalej
        if (e.Modifiers == (Keys.Control | Keys.Alt))
        {
            e.IsInputKey = true;
        }
        else if (e.KeyCode == Keys.Tab)
        {
            if (this._monthCalendar != null && this._monthCalendar.Visible)
                ToggleMonthCalendar();
        }
        else if (e.Control && _type != InputType.Password)
        {
            switch (e.KeyCode)
            {
                case Keys.A: // Ctrl+A for Select All
                    _textBox.SelectAll();

                    break;

                case Keys.C: // Ctrl+C for Copy
                    if (!string.IsNullOrEmpty(_textBox.SelectedText))
                        Clipboard.SetText(_textBox.SelectedText);

                    break;

                case Keys.X: // Ctrl+X for Cut
                    if (!ReadOnly && !string.IsNullOrEmpty(_textBox.SelectedText))
                    {
                        Clipboard.SetText(_textBox.SelectedText);
                        _textBox.SelectedText = string.Empty;
                    }

                    break;

                    //case Keys.V: // Ctrl+V for Paste
                    //    if (Type != InputType.Default && !ReadOnly && Clipboard.ContainsText())
                    //        _textBox.SelectedText = Clipboard.GetText();

                    //    break;
            }
            // Set IsInputKey to true to ensure that the control receives these key events
            e.IsInputKey = true;
        }
    }

    public int GetCursorStart() { return this._textBox.SelectionStart; }
    public int GetSelectionLength() { return this._textBox.SelectionLength; }
    private void textBox_KeyDown(object sender, KeyEventArgs e)
    {
        //Console.WriteLine($"KeyDown: {e.KeyCode}, Modifiers: {e.Modifiers}");

        if (ReadOnly || !Editable)
            e.Handled = true;

        if (e.Modifiers == (Keys.Control | Keys.Alt) && e.KeyCode != Keys.ControlKey && e.KeyCode != Keys.Menu) { }
        else if (e.Control && (e.KeyCode == Keys.A))
        {
            // Select all text in the TextBox
            _textBox.SelectAll();
            // Mark the event as handled so it doesn't propagate further
            e.SuppressKeyPress = true;
        }
        else
        {
            switch (_type)
            {
                case InputType.Select:
                case InputType.MultiSelect:
                case InputType.Date:
                    if ((e.Control && e.KeyCode == Keys.V) || (e.Control && e.KeyCode == Keys.X))
                        e.SuppressKeyPress = true;
                    break;
            }
        }
    }
    private void textBox_KeyPress(object sender, KeyPressEventArgs e)
    {
        if (ReadOnly || !Editable)
            e.Handled = true;
        else
        {
            //Console.WriteLine($"KeyPress -> {e.KeyChar}");

            if (e.KeyChar == 22) // ASCII Ctrl+V
            {
                if (Clipboard.ContainsText())
                {
                    switch (_type)
                    {
                        case InputType.Numeric:
                            if (!long.TryParse(Clipboard.GetText().Replace(" ", "").Replace($"{_decimalDelimiter}", "."), out _))
                                e.Handled = true;
                            break;
                        case InputType.Decimal:
                            if (this._textBox.Text.Contains($"{_decimalDelimiter}"))
                            {
                                if (this._textBox.SelectionStart >= this._textBox.Text.Length - this._textBox.Text.Split(_decimalDelimiter)[1].Length)
                                    e.Handled = true;
                            }
                            if (!double.TryParse(Clipboard.GetText().Replace(" ", "").Replace($"{_decimalDelimiter}", ".") , NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out _))
                                e.Handled = true;

                            break;
                    }
                }
                else
                    e.Handled = true;
            }
            else if (e.KeyChar == (char)27) // 27 is the ASCII code for the Escape key
            {
                if (_isOpen)
                {
                    switch (_type)
                    {
                        case InputType.Date:
                            ToggleMonthCalendar();
                            break;
                        case InputType.MultiSelect:
                        case InputType.Select:
                            ToggleSelectContextMenuStrip();
                            break;
                    }
                }
            }
            else if (e.KeyChar != '\b')
            {
                switch (_type)
                {
                    //case InputType.Select:
                    //case InputType.MultiSelect:
                    //case InputType.Date:
                    //    e.Handled = true;
                    //    break;

                    case InputType.Decimal:
                        if (e.KeyChar == '0' && (string.IsNullOrEmpty(this.Text) || (this._textBox.SelectionStart == 0 && this._textBox.SelectionLength > 0)))
                            e.Handled = true;
                        // Cannot start with 0 at the beggining
                        else if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && e.KeyChar != _decimalDelimiter)
                            e.Handled = true;
                        // Allow only digits, control characters, and the decimal point
                        else if (e.KeyChar == _decimalDelimiter && (Text.Contains(_decimalDelimiter) || Text.Length == 0))
                            e.Handled = true;
                        // Allow only one decimal point and it cannot be the first character
                        else if (Text.Contains(_decimalDelimiter) && Text.Split(_decimalDelimiter).Length > 1 && Text.Split(_decimalDelimiter)[1].Length == 2 && this._textBox.SelectionStart <= this._textBox.Text.IndexOf(_decimalDelimiter))
                            e.Handled = false;
                        // If there is a decimal point and it has two decimal places and the selection is before the decimal point
                        // Allow the user to replace the selected portion with new digits
                        // But do not allow additional digits after the decimal point
                        else if (Text.Contains(_decimalDelimiter) && Text.Split(_decimalDelimiter).Length > 1 && Text.Split(_decimalDelimiter)[1].Length == 2 && this._textBox.SelectionStart > this._textBox.Text.IndexOf(_decimalDelimiter))
                            e.Handled = true;
                        // If the selection is after the decimal point, prevent typing more digits
                        else if (e.KeyChar == '0' && (Text.Length == 0 || (this._textBox.SelectionStart == 0 && Text[0] != '0' && Text.Length > 1 && Text[1] == _decimalDelimiter)))
                            e.Handled = false;
                        // If the user attempts to enter '0', ensure it's not the first character or the first character after a decimal point

                        break;

                    case InputType.Numeric:
                        if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
                            e.Handled = true;

                        break;
                }
            }
        }

        base.OnKeyPress(e);
        base.Invalidate();
    }
    private void textBox_TextChanged(object sender, EventArgs e)
    {
        try
        {
            if (Type == InputType.Decimal)
            {
                string number = this._textBox.Text.Replace(" ", "");
                string afterDot = string.Empty;

                int startPos = this._textBox.SelectionStart;
                int length = this._textBox.Text.Length;

                if (this._textBox.Text.Contains(_decimalDelimiter))
                {
                    afterDot = $"{_decimalDelimiter}{this._textBox.Text.Split(_decimalDelimiter)[1]}";
                    number = number.Split(_decimalDelimiter)[0];
                }

                if (long.TryParse(number, out long value))
                {
                    string parts = value.ToString("N0", new NumberFormatInfo()
                    {
                        NumberGroupSizes = new[] { 3 },
                        NumberGroupSeparator = " "
                    });

                    if (!string.IsNullOrEmpty(afterDot))
                        this._textBox.Text = parts + afterDot;
                    else
                        this._textBox.Text = parts;

                    if (this._textBox.Text.Length > length)
                        startPos += this._textBox.Text.Length - length;
                    else if (this._textBox.Text.Length < length)
                        startPos -= length - this._textBox.Text.Length;

                    this._textBox.SelectionStart = startPos;
                }


                //if (this._textBox.Text.Contains("."))
                //{

                //}


                //if (double.TryParse(this._textBox.Text.Trim(), out double d))
                //{
                //    var f = new NumberFormatInfo { NumberGroupSeparator = " " };

                //    this._textBox.Text = d.ToString("n", f); // 2 000 000.00
                //}
            }
        }
        catch (Exception) { }


        this.Text = this._textBox.Text;
        base.Invalidate();
    }
    private void textBox_MouseDoubleClick(object sender, MouseEventArgs e)
    {
        if (e.Button == MouseButtons.Left)
            this._textBox.SelectAll();

    }
    private void textBox_MouseDown(object sender, MouseEventArgs e)
    {
        this._textBox.Focus();

        if (e != null && e.Button == MouseButtons.Right)
            OpenTextBoxContextMenu(e);
        else if (string.IsNullOrEmpty(this.Text) && !Editable)
        {
            switch (_type)
            {
                case InputType.Date:
                case InputType.Select:
                case InputType.MultiSelect:
                case InputType.File:
                    pictureBox_Click(this._pictureBox, e);
                    break;
            }
        }
    }
    private void textBox_GotFocus(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(this._textBox.Text))
            this._textBox.SelectionStart = this._textBox.Text.Length;
    }
    private void textBox_LostFocus(object sender, EventArgs e)
    {
        if (Type == InputType.Decimal)
        {
            if (this._textBox.Text.EndsWith(_decimalDelimiter.ToString()))
                this.Text += "00";
            else if (!this._textBox.Text.Contains(_decimalDelimiter))
            {
                if (this._textBox.Text.Length > 0)
                    this.Text += $"{_decimalDelimiter}00";
                else
                    this.Text = $"0{_decimalDelimiter}00";
            }
                
        }
    }
    #endregion

    #region ContextMenuStrip events and methods
    private void OpenTextBoxContextMenu(MouseEventArgs e)
    {
        if (_type == InputType.Date || _type == InputType.Select || _type == InputType.MultiSelect)
        {
            this._menuItemCut.Visible = false;
            this._menuItemPaste.Visible = false;
            this._menuSeparator.Visible = false;
            this._menuItemClear.Visible = false;
        }
        if (ReadOnly || !Editable)
        {
            this._menuItemCopy.Enabled = this._textBox.SelectedText.Length > 0;
            this._menuItemCut.Enabled = false;
            this._menuItemPaste.Enabled = false;
            this._menuItemClear.Enabled = false;
        }
        else
        {
            this._menuItemCopy.Enabled = this._textBox.SelectedText.Length > 0;
            this._menuItemCut.Enabled = this._textBox.SelectedText.Length > 0;
            this._menuItemPaste.Enabled = Clipboard.GetText() != null;
            this._menuItemClear.Enabled = this._textBox.Text.Length > 0;
        }

        if (this._textBox.Focused && this._contextMenuStrip != null && _type != InputType.Password)
            this._contextMenuStrip.Show(this._textBox, e.Location);
    }
    private void Paste()
    {
        if (Clipboard.ContainsText())
            this._textBox.SelectedText = Clipboard.GetText();
    }
    private void Copy()
    {
        if (!string.IsNullOrEmpty(this._textBox.SelectedText))
            Clipboard.SetText(this._textBox.SelectedText);
    }
    private void Cut()
    {
        if (!string.IsNullOrEmpty(this._textBox.SelectedText))
        {
            Clipboard.SetText(this._textBox.SelectedText);
            this._textBox.SelectedText = string.Empty;
        }
    }
    #endregion

    #region PictureBox events and methods
    private void pictureBox_Click(object sender, EventArgs e)
    {
        if (ReadOnly)
            return;

        MouseEventArgs mouseEvent = e as MouseEventArgs;

        if (mouseEvent != null && mouseEvent.Button == MouseButtons.Left) 
        {
            this.Focus();

            OnImageClick(e);

            if (DisableImageClick)
                return;

            switch (_type)
            {
                case InputType.Password:
                    this._textBox.UseSystemPasswordChar = !this._textBox.UseSystemPasswordChar;
                    _updateImage = true;
                    break;
                case InputType.Select:
                case InputType.MultiSelect:
                    ToggleSelectContextMenuStrip();
                    break;
                case InputType.Date:
                    ToggleMonthCalendar();
                    break;
                case InputType.File:
                    OpenFileDialog fd = new OpenFileDialog();
                    fd.ShowDialog();

                    if (!string.IsNullOrEmpty(fd.FileName))
                    {
                        Console.WriteLine(fd.FileName);
                        Text = fd.SafeFileName;
                        TextExtra = fd.FileName;
                        this._textBox.SelectionStart = Text.Length;
                        this._textBox.SelectionLength = 0;
                        _updateImage = true;
                    }

                    break;
            }

            base.Invalidate();
        }
    }
    private void pictureBox_MouseEnter(object sender, EventArgs e)
    {
        ShowToolTip(ToolTipImageText, this._pictureBox);
    }
    private void pictureBox_MouseLeave(object sender, EventArgs e)
    {
        HideToolTip(this._pictureBox);
    }
    private void clearPictureBox_Click(object sender, EventArgs e)
    {
        if (ReadOnly)
            return;

        MouseEventArgs mouseEvent = e as MouseEventArgs;

        if (mouseEvent != null && mouseEvent.Button == MouseButtons.Left)
        {
            if (Type == InputType.Date && _isOpen)
                ToggleMonthCalendar();
            else if ((Type == InputType.Select || Type == InputType.MultiSelect) && _isOpen)
                ToggleSelectContextMenuStrip();
            else if (Type == InputType.File)
                _updateImage = true;

            if (ClearImageClick != null)
                OnClearImageClick(e);
            else
            {
                Text = string.Empty;
                TextExtra = string.Empty;
            }
        }

        base.Invalidate();
    }
    private void clearPictureBox_MouseEnter(object sender, EventArgs e)
    {
        ShowToolTip(ToolTipClearImageText, this._clearPictureBox);
    }
    private void clearPictureBox_MouseLeave(object sender, EventArgs e)
    {
        HideToolTip(this._clearPictureBox);
    }
    #endregion

    #region ToolTip methods
    private void ShowToolTip(string text, Control control)
    {
        if (this._toolTip == null)
            this._toolTip = new ToolTip();

        if (!string.IsNullOrEmpty(text))
            this._toolTip.Show(text, control);
    }
    private void HideToolTip(Control control)
    {
        if (this._toolTip != null)
            this._toolTip.Hide(control);
    }
    #endregion

    #region SelectContextMenuStrip event handlers and methods
    private void ToggleSelectContextMenuStrip()
    {
        if (this._selectContextMenuStrip == null)
        {
            this._selectContextMenuStrip = new ContextMenuStrip();
            this._selectContextMenuStrip.Font = this.Font;
            this._selectContextMenuStrip.Closing += SelectContextMenuStrip_Closing;
        }
        else if (_options.Count == 0)
            return;

        if (_closeTime != null && (DateTime.Now - _closeTime).TotalMilliseconds <= 200)
        {
            this._selectContextMenuStrip.Hide();
            _isOpen = false;
            _updateImage = true;
            return;
        }
        else
        {
            bool rerender = false;

            if (Type == InputType.Select && _options.Count != this._selectContextMenuStrip.Items.Count)
                rerender = true;
            else if (Type == InputType.MultiSelect && _options.Count != this._selectContextMenuStrip.Items.Count - 2)
                rerender = true;

            if (rerender || _previousText != this._textBox.Text)
            {
                // Aktualne wartości
                Collection<string> values = null;
                string delmiter = string.IsNullOrEmpty(MultiSelectDelimiter) ? _defaultMultiSelectDelmiter : MultiSelectDelimiter;

                if (!string.IsNullOrEmpty(this._textBox.Text))
                {
                    if (_textBox.Text.Contains(delmiter))
                        values = new Collection<string>(this._textBox.Text.Split(new[] { delmiter }, StringSplitOptions.None));
                    else
                        values = new Collection<string> { this._textBox.Text };
                }

                if (rerender || _previousText != this._textBox.Text) // all && separator
                {
                    this._selectContextMenuStrip.Items.Clear();

                    if (_type == InputType.MultiSelect)
                    {
                        _allToolStripMenuItem = new ToolStripMenuItem("Zaznacz wszystko");
                        _allToolStripMenuItem.Font = new Font(_allToolStripMenuItem.Font, FontStyle.Bold);
                        _allToolStripMenuItem.Click += new EventHandler(SelectAllToolStripMenuItem_Click);
                        this._selectContextMenuStrip.Items.Add(_allToolStripMenuItem);

                        ToolStripSeparator toolStripSeparator = new ToolStripSeparator();
                        this._selectContextMenuStrip.Items.Add(toolStripSeparator);
                    }

                    foreach (string option in _options)
                    {
                        ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem(option);
                        toolStripMenuItem.Click += new EventHandler(SelectToolStripMenuItem_Click);
                        this._selectContextMenuStrip.Items.Add(toolStripMenuItem);

                        if (values != null && values.Contains(option))
                            toolStripMenuItem.Checked = true;
                        else
                            toolStripMenuItem.Checked = false;
                    }
                }

                if (values != null && values.Count > 0)
                    _previousText = string.Join(delmiter, values.ToArray());
                else
                    _previousText = this._textBox.Text;
            }

            if (this._selectContextMenuStrip.Items.Count > 0)
            {
                this._selectContextMenuStrip.Show(this, new Point(this.Width - this._selectContextMenuStrip.Width, this.Height));
                _isOpen = true;
                _updateImage = true;
            }
        }
    }
    private void SelectContextMenuStrip_Closing(object sender, ToolStripDropDownClosingEventArgs e)
    {
        if (e.CloseReason == ToolStripDropDownCloseReason.ItemClicked && _type == InputType.MultiSelect)
            e.Cancel = true; // Cancel closing when an item is clicked
        else
        {
            _isOpen = false;
            _updateImage = true;

            _closeTime = DateTime.Now;

            base.Invalidate();
        }
    }
    private void SelectToolStripMenuItem_Click(object sender, EventArgs e)
    {
        SelectOption(sender);
    }
    private void SelectAllToolStripMenuItem_Click(object sender, EventArgs e)
    {
        SelectOption(sender, true);
    }
    private void SelectOption(object sender, bool all = false)
    {
        ToolStripMenuItem toolStripMenuItem = sender as ToolStripMenuItem;

        if (Type == InputType.Select)
        {
            this._textBox.Text = toolStripMenuItem.Text;

            foreach (ToolStripItem toolStripItem in this._selectContextMenuStrip.Items)
            {
                if (toolStripItem is ToolStripMenuItem item && item.Checked && item != toolStripMenuItem)
                    item.Checked = false;
            }
            
            toolStripMenuItem.Checked = true;
        }
        else if (Type == InputType.MultiSelect)
        {
            string output = string.Empty;
            List<string> values = new List<string>();

            string delimiter = string.IsNullOrEmpty(MultiSelectDelimiter) ? _defaultMultiSelectDelmiter : MultiSelectDelimiter;

            // Check for existsing values and if they are valid
            if (this._textBox.Text.Contains(delimiter))
            {
                foreach (string value in this._textBox.Text.Split(new string[] { delimiter }, StringSplitOptions.None))
                {
                    if (Options.Contains(value))
                        values.Add(value);
                }
            }
            else if (Options.Contains(this._textBox.Text))
                values.Add(this._textBox.Text);

            foreach (ToolStripItem toolStripItem in this._selectContextMenuStrip.Items)
            {
                if (toolStripItem is ToolStripMenuItem item && item != _allToolStripMenuItem)
                {
                    if (toolStripMenuItem == _allToolStripMenuItem)
                    {
                        if (Options.Contains(item.Text) && !values.Contains(item.Text))
                        {
                            values.Add(item.Text);
                            item.Checked = true;
                        }
                    }
                    else if (item == toolStripMenuItem)
                    {
                        if (item.Checked)
                        {
                            values.Remove(item.Text);
                            item.Checked = false;
                            _allToolStripMenuItem.Checked = false;
                        }
                        else
                        {
                            if (Options.Contains(item.Text))
                            {
                                values.Add(item.Text);
                                item.Checked = true;
                            }
                        }
                    }
                }
            }

            // Change checked status for select all
            if (toolStripMenuItem == _allToolStripMenuItem)
                _allToolStripMenuItem.Checked = !_allToolStripMenuItem.Checked;

            foreach (string item in values)
            {
                if (string.IsNullOrEmpty(output))
                    output = item;
                else
                    output += delimiter + item;
            }

            this._textBox.Text = output;
        }

        _previousText = this._textBox.Text;
        this._textBox.SelectionStart = this._textBox.Text.Length;
        this._textBox.SelectionLength = 0;

        _updateImage = true;
        base.Invalidate();
    }
    #endregion

    #region MonthCalendar event handlers and methods
    private void ToggleMonthCalendar()
    {
        if (this._monthCalendar == null)
        {
            this._monthCalendar = new MonthCalendar();
            this._monthCalendar.Font = this.Font;
            this._monthCalendar.MaxSelectionCount = 1;
            this._monthCalendar.DateSelected += MonthCalendar_DateSelected;
            this._monthCalendar.PreviewKeyDown += MonthCalendar_PreviewKeyDown;
        }

        if (_isOpen)
        {
            this._monthCalendar.Hide();
            this.FindForm().Controls.Remove(this._monthCalendar);

            _isOpen = false;
            Application.RemoveMessageFilter(this);
        }
        else
        {
            this.FindForm().Controls.Add(this._monthCalendar);
            this._monthCalendar.Location = CalculateMonthCalendarPosition();
            this._monthCalendar.Show();
            this._monthCalendar.BringToFront();
            this._monthCalendar.Focus();

            if (!string.IsNullOrEmpty(Text) && DateTime.TryParse(Text, out DateTime date))
                this._monthCalendar.SetDate(date);

            this._monthCalendar.MinDate = _dateMin;
            this._monthCalendar.MaxDate = _dateMax;

            Application.AddMessageFilter(this);

            _isOpen = true;
        }
    }
    private void MonthCalendar_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
        if (e.KeyCode == Keys.Tab)
        {
            if (this._monthCalendar != null && this._monthCalendar.Visible)
                ToggleMonthCalendar();
        }
    }
    private void MonthCalendar_DateSelected(object sender, DateRangeEventArgs e)
    {
        this.Text = e.End.ToString(string.IsNullOrEmpty(DateFormat) ? _defaultDateFormat : DateFormat);
        ToggleMonthCalendar();
        this._textBox.Focus();
    }
    #endregion

    #region Methods
    private Point CalculateMonthCalendarPosition()
    {
        // Get the position of the control relative to its parent form
        Size formSize = this.FindForm().Size;
        Size calendarSize = this._monthCalendar.Size;
        Point controlPos = this.FindForm().PointToClient(this.PointToScreen(Point.Empty));

        //Console.WriteLine($"form: {formSize}");
        //Console.WriteLine($"calendar: {calendarSize}");
        //Console.WriteLine($"control: {controlPos}");

        // Bellow
        if (formSize.Height - (this.Height + controlPos.Y) > calendarSize.Height)
        {
            if ((controlPos.X + this.Width) > calendarSize.Width)
                return new Point(controlPos.X + this.Width - calendarSize.Width, controlPos.Y + this.Height);
            else
                return new Point(controlPos.X, controlPos.Y + this.Height);
        }

        // Above
        else if (formSize.Height - (formSize.Height - controlPos.Y) > calendarSize.Height)
        {
            return new Point(controlPos.X + this.Width - calendarSize.Width, controlPos.Y - calendarSize.Height);
        }


        // If none of the positions are available, return the original position below the control
        return new Point(0, 0);
    }
    private void CalculateLocationAndSize()
    {
        int fontOffsetY = 1;
        int scrollBar = 0;
        if (_scrollBars == ScrollBars.Vertical || (this._textBox.GetLineFromCharIndex(this._textBox.Text.Length) + 1) * this._textBox.Font.Height > this._textBox.Height)
        {
            this._textBox.ScrollBars = ScrollBars.Vertical;
            scrollBar = SystemInformation.VerticalScrollBarWidth + 2;
        }
        else
        {
            this._textBox.ScrollBars = ScrollBars.None;
            scrollBar = 0;
        }

        int padding = 5;
        if (base.Height >= this._textBox.Height + (BorderRadius * 1.6) && !TextArea)
        {
            this._textBox.Location = new Point(padding, (base.Height / 2) - (this._textBox.Height / 2) + fontOffsetY);
            this._textBox.Width = base.Width - 2 * padding;
        }
        else
        {
            int trueRadius = (int)((BorderRadius - 2) / 3.14);

            if (trueRadius < 0)
                trueRadius = 0;

            if (TextArea)
            {
                trueRadius += BorderRadius >= 3 ? 1 : 0;
                this._textBox.Location = new Point(padding + trueRadius, padding + trueRadius);
                this._textBox.Height = base.Height - 2 * (padding + trueRadius);
            }
            else
                this._textBox.Location = new Point(padding + trueRadius, (base.Height / 2) - (this._textBox.Height / 2) + fontOffsetY);

            this._textBox.Width = this.Width - 2 * (padding + trueRadius);
        }

        int rightMargin = 0;
        int shadows = BorderShadows ? 2 : 0;

        int posX = this._textBox.Location.X + this._textBox.Width - scrollBar - shadows / 2;
        int extraX = 0;

        if (UseImage || ReadOnly)
        {
            if (this._pictureBox != null && this._pictureBox.Image != null)
            {
                extraX += this._pictureBox.Width;

                if (TextArea)
                    this._pictureBox.Location = new Point(posX - extraX, this._textBox.Location.Y);
                else
                    this._pictureBox.Location = new Point(posX - extraX, (base.Height / 2) - (this._pictureBox.Height / 2));

                if (this._pictureBox.Visible)
                {
                    rightMargin += this._pictureBox.Width + 2;
                    this._pictureBox.BringToFront();
                }
            }
        }
        else if (this._pictureBox != null)
            this._pictureBox.Visible = false;


        if (this._clearPictureBox != null && this._clearPictureBox.Image != null)
        {
            if (TextArea)
            {
                if (this._textBox.Height - extraX - this._clearPictureBox.Width >= 0 && extraX != 0)
                {
                    this._clearPictureBox.Location = new Point(posX - extraX - this._clearPictureBox.Width + this._clearPictureBox.Width, this._textBox.Location.Y + this._clearPictureBox.Height);
                }
                else
                {
                    this._clearPictureBox.Location = new Point(posX - extraX - this._clearPictureBox.Width - (this._pictureBox.Visible ? 2 : 0), this._textBox.Location.Y);

                    if (this._clearPictureBox.Visible)
                        rightMargin += this._clearPictureBox.Width + 2;
                }
            }
            else
            {
                this._clearPictureBox.Location = new Point(posX - extraX - this._clearPictureBox.Width - (this._pictureBox.Visible ? 2 : 0), (base.Height / 2) - (this._clearPictureBox.Height / 2));

                if (this._clearPictureBox.Visible)
                    rightMargin += this._clearPictureBox.Width + 2;
            }

            if (this._clearPictureBox.Visible)
                this._clearPictureBox.BringToFront();
        }

        this._textBox.PaddingRight = rightMargin;
    }
    private void InitializePictureBox()
    {
        if (this._pictureBox == null)
        {
            this._pictureBox = new PictureBox();
            this._pictureBox.Click += pictureBox_Click;
            this._pictureBox.MouseEnter += pictureBox_MouseEnter;
            this._pictureBox.MouseLeave += pictureBox_MouseLeave;
            this._pictureBox.BackColor = this.BackgroundColor;

            if (this._pictureBox.Parent != this)
                this._pictureBox.Parent = this;

            if (!this.Controls.Contains(this._pictureBox))
                this.Controls.Add(this._pictureBox);
        }
    }
    private void InitializeClearPictureBox()
    {
        if (this._clearPictureBox == null)
        {
            this._clearPictureBox = new PictureBox();
            this._clearPictureBox.Click += clearPictureBox_Click;
            this._clearPictureBox.MouseEnter += clearPictureBox_MouseEnter;
            this._clearPictureBox.MouseLeave += clearPictureBox_MouseLeave;
            this._clearPictureBox.BackColor = this.BackgroundColor;

            if (this._clearPictureBox.Parent != this)
                this._clearPictureBox.Parent = this;

            if (!this.Controls.Contains(this._clearPictureBox))
                this.Controls.Add(this._clearPictureBox);
        }
    }
    private void UpdateImage()
    {
        if (_updateImage)
        {
            if (ReadOnly)
            {
                if (this._pictureBox != null)
                {
                    this._pictureBox.Visible = SetImage(this._pictureBox, ImageColor, null, InputResources.locked);
                    this._pictureBox.Visible = true;
                }

                if (this._clearPictureBox != null)
                    this._clearPictureBox.Visible = false;
            }
            else
            {
                InitializePictureBox();

                switch (_type)
                {
                    case InputType.Password:
                        if (this._textBox.UseSystemPasswordChar)
                            this._pictureBox.Visible = SetImage(this._pictureBox, ImageColor, ImageFront, InputResources.show);
                        else
                            this._pictureBox.Visible = SetImage(this._pictureBox, ImageColor, ImageBack, InputResources.hide);

                        break;
                    case InputType.Select:
                    case InputType.MultiSelect:
                        if (this._selectContextMenuStrip != null)
                        {
                            if (_isOpen)
                                this._pictureBox.Visible = SetImage(this._pictureBox, ImageColor, ImageBack, InputResources.up);
                            else
                                this._pictureBox.Visible = SetImage(this._pictureBox, ImageColor, ImageFront, InputResources.down);
                        }
                        else
                            this._pictureBox.Visible = SetImage(this._pictureBox, ImageColor, ImageFront, InputResources.down);
                        break;
                    case InputType.File:
                        if (string.IsNullOrEmpty(Text))
                            this._pictureBox.Visible = SetImage(this._pictureBox, ImageColor, ImageFront, InputResources.file);
                        else
                            this._pictureBox.Visible = SetImage(this._pictureBox, ImageColor, ImageFront, InputResources.change);
                        break;
                    case InputType.Decimal:
                        this._pictureBox.Visible = SetImage(this._pictureBox, ImageColor, ImageFront, InputResources.currency_pl);
                        break;
                    case InputType.Numeric:
                        this._pictureBox.Visible = SetImage(this._pictureBox, ImageColor, ImageFront, InputResources.number);
                        break;
                    case InputType.Date:
                        this._pictureBox.Visible = SetImage(this._pictureBox, ImageColor, ImageFront, InputResources.calendar);
                        break;
                    default:
                        this._pictureBox.Visible = SetImage(this._pictureBox, ImageColor, ImageFront);
                        break;
                }

                if (ClearVisibility == ClearVisibility.Always || (ClearVisibility == ClearVisibility.WhenText && !string.IsNullOrEmpty(Text)))
                {
                    InitializeClearPictureBox();
                    this._clearPictureBox.Visible = SetImage(this._clearPictureBox, ClearImageColor, ClearImage, InputResources.clear);
                }
                else if (this._clearPictureBox != null)
                    this._clearPictureBox.Visible = false;
            }
        }

        _updateImage = false;
    }
    public bool SetImage(PictureBox pictureBox, Color color, Image image, Image defaultImage = null)
    {
        Image img = null;

        if (image != null)
            img = image;
        else if (defaultImage != null)
            img = defaultImage;

        if (img == null)
            return false;

        //int shadows = BorderShadows ? 2 : 0;
        int height = this.Font.Height;

        // Get the image current width
        int sourceWidth = img.Width;

        // Get the image current height
        int sourceHeight = img.Height;

        // Calculate width and height with new desired size
        float nPercentW = ((float)height / (float)sourceWidth);
        float nPercentH = ((float)height / (float)sourceHeight);
        float nPercent = Math.Min(nPercentW, nPercentH);

        // New Width and Height
        int destWidth = (int)(sourceWidth * nPercent);
        int destHeight = (int)(sourceHeight * nPercent);

        Bitmap bitmap = new Bitmap(destWidth, destHeight);
        Graphics g = Graphics.FromImage(bitmap);

        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
        g.SmoothingMode = SmoothingMode.HighQuality;
        g.PixelOffsetMode = PixelOffsetMode.HighSpeed;
        g.CompositingQuality = CompositingQuality.HighQuality;

        // Draw image with new width and height
        g.DrawImage(img, 0, 0, destWidth, destHeight);
        g.Dispose();
           
        Bitmap modifiedImage = new Bitmap(bitmap.Width, bitmap.Height);

        for (int y = 0; y < bitmap.Height; y++)
        {
            for (int x = 0; x < bitmap.Width; x++)
            {
                Color pixel = bitmap.GetPixel(x, y);

                int newRed = color.R;
                int newGreen = color.G;
                int newBlue = color.B;

                modifiedImage.SetPixel(x, y, Color.FromArgb(pixel.A, newRed, newGreen, newBlue));
            }
        }

        pictureBox.Size = new Size(height, height);
        pictureBox.Image = modifiedImage;

        return true;
    }
    private void FuzzyShadows(GraphicsPath path, Graphics gr, Color base_color, int max_opacity, int width, int opaque_width)
    {
        int num_steps = width - opaque_width + 1;
        float delta = (float)max_opacity / num_steps / num_steps;

        float alpha = delta;

        for (int thickness = width; thickness >= opaque_width; thickness--)
        {
            Color color = Color.FromArgb((int)alpha, base_color.R, base_color.G, base_color.B);
            using (Pen pen = new Pen(color, thickness))
            {
                pen.EndCap = LineCap.Round;
                pen.StartCap = LineCap.Round;
                gr.DrawPath(pen, path);
            }
            alpha += delta;
        }
    }
    public void Clear()
    {
        this.Text = string.Empty;
        this._textExtra = string.Empty;
    }
    public void SetCursorPosition(int index, int length = 0)
    {
        if (index <= this._textBox.Text.Length)
        {
            this._textBox.SelectionStart = index;
            this._textBox.SelectionLength = length;
        }
    }
    #endregion
}
